<?php

// Bible SuperSearch Configuration
$default_language="en";
$default_bible_version="kjv";
$default_interface="user_friendly1";
$max_verses="1000";
$verses_per_page="20";
$formatting_buttons="standard";
$browsing_buttons="square";
$enable_remote_install="off";
$enable_booklinks="off";
// Do not change this unless you know what you are doing!
$installed_version="2.0.00.";
?>
