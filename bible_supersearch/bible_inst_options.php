<?php 
//session_save_path("modules");
//session_start(); 
require_once("bible_inst_functions.php");

//error_reporting(E_ALL);

//if(!check_login_file()){return;}

require_once("bible_inst_auth.php");
if ($auth==false){return;}
?>

<div style="text-align:center;color:darkred;font-size:140%; font-weight:bold;">Bible SuperSearch - Options</div><br><br><center>


<?php

//crase

menu("bible_inst_options.php");


//require_once("bible_mysql.php");
//connect();

require_once("bible_login.php");
require_once("bible_misc.php");


GlorifytheLord("yes");

if(!is_writable("bible_config.php")){echo("Set the file \"bible_config.php\" to writable to continue.");}

$submit=$_POST['submit'];

if($submit=="true"){
$default_language=$_POST['default_language'];
$default_bible_version=$_POST['version'];
$default_interface=$_POST['default_interface'];
$max_verses=$_POST['max_verses'];
$verses_per_page=$_POST['verses_per_page'];
$formatting_buttons=$_POST['formatting_buttons'];
$browsing_buttons=$_POST['browsing_buttons'];
$installed_version=$_POST['installed_version'];
$enable_remote_install=$_POST['enable_remote_install'];
$enable_booklinks=$_POST['enable_booklinks'];

$content="<?php

// Bible SuperSearch Configuration
\$default_language=\"$default_language\";
\$default_bible_version=\"$default_bible_version\";
\$default_interface=\"$default_interface\";
\$max_verses=\"$max_verses\";
\$verses_per_page=\"$verses_per_page\";
\$formatting_buttons=\"$formatting_buttons\";
\$browsing_buttons=\"$browsing_buttons\";
\$enable_remote_install=\"$enable_remote_install\";
\$enable_booklinks=\"$enable_booklinks\";
// Do not change this unless you know what you are doing!
\$installed_version=\"$installed_version\";
?>";

write_file("bible_config.php",$content);

echo('Changes saved.<br><BR>
<form action=bible_inst_manager.php><input type=submit value="Continue"></form>');
}// end if

else{

require_once("bible_config.php");
require_once("bible_system.php");

//echo("I am here");

echo("<form method='post' name='me'><input type=hidden name=submit value=true>
<input type=hidden name='installed_version' value='$installed_version'>
<table border=1><tr><td>
Default Bible</td><td> ".bibleversions($default_bible_version)."</td><td>Choose the default Bible version to be used.</td></tr>
<tr><td>Default Language </td><td> <select name='default_language'>".languageList($default_language)."</select></td><td>Default language should correspond to the language of the default Bible</td></tr>
<tr><td>Interface </td><td> <select name='default_interface'><option value='standard'> Standard ($standard_interface)</option>".interfaceList($default_interface)."</select>
</td><td>Choose the interface that you want to use.</td></tr>
<tr><td>Browsing Buttons</td><td><select name='browsing_buttons'>".getBrowsingButtons($browsing_buttons)."</select></td><td>Choose the style of the browsing buttons</td></tr>
<tr><td>Formatting Icons</td><td><select name='formatting_buttons'>".getFormattingButtons($formatting_buttons)."</select></td><td>Choose the style of formatting icons.</td></tr>
<!--<input type=text name='default_interface' value='$default_interface'>--></td></tr>
<tr><td>Maximum verses</td><td> <input type=text name='max_verses' value='$max_verses'></td><td>Total maximum number of verses returned by any search. Users are advised to narrow their searches if this value is exceeded. Helps prevent overload of your server. Default=2000</td></tr>
<tr><td>Verses per Page</td><td> <input type=text name='verses_per_page' value='$verses_per_page'></td><td>The maximum number of verses displayed per page for paginated search results. Default=20</td></tr>
<tr><td>Enable Remote Install</td><td><select name='enable_remote_install'>".onoff($enable_remote_install)."</select></td><td>Remote install allows you to install Bibles directly from various servers, without having to download, then upload the modules yourself. PLEASE NOTE, THIS IS AN EXPERIMENTAL FEATURE. ENABELING IT MAY CAUSE BANNER ADS TO APPEAR ON THE INSTALLATION MANAGER PAGES. Disable it when you are not using it.</td></tr>
<tr><td>Enable Booklinks</td><td><select name='enable_booklinks'>".onoff($enable_booklinks)."</select></td><td>Displays links to all books of the Bible at the bottom of your interface.</td></tr>
</table>");


if(is_writable("bible_config.php")){echo"<input type=submit value='Go'>";}
else{echo"<br><BR>The file \"bible_config.php\" must be set WRITABLE to continue.";}

echo("</form><br><BR><BR>");

previewInterfaces();

}

function onoff($on){

$text="<option value='on'";
if ($on=="on"){$text.=" selected";}
$text.="> Yes </option><option value='off'";
if ($on!="on"){$text.=" selected";}

$text.="> No </option>";

return $text;

}
