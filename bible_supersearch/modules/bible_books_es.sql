
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the Spanish language.

DROP TABLE IF EXISTS `bible_books_es`;
CREATE TABLE `bible_books_es` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('1','Génesis','','50');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('2','Éxodo','','40');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('3','Levítico','','27');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('4','Números','','36');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('5','Deuteronomio','','34');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('6','Josué','','24');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('7','Jueces','','21');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('8','Ruth','','4');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('9','1Samuel','','31');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('10','2Samuel','','24');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('11','1Reyes','','22');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('12','2Reyes','','25');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('13','1Crónicas','','29');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('14','2Crónicas','','36');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('15','Esdras','','10');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('16','Nehemías','','13');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('17','Esther','','10');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('18','Job','','42');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('19','Salmos','','150');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('20','Proverbios','','31');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('21','Ecclesiastés','','12');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('22','Canción de canciones','','8');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('23','Isaías','','66');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('24','Jeremías','','52');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('25','Lamentaciones','','5');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('26','Ezequiel','','48');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('27','Daniel','','12');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('28','Oseas','','14');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('29','Joel','','3');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('30','Amós','','9');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('31','Abdías','','1');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('32','Jonás','','4');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('33','Miqueas','','7');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('34','Nahum','','3');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('35','Habacuc','','3');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('36','Sofonías','','3');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('37','Haggeo','','2');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('38','Zacarías','','14');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('39','Malaquías','','4');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('40','Mateo','','28');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('41','Marcos','','16');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('42','Lucas','','24');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('43','Juan','','21');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('44','Hechos','','28');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('45','Romanos','','16');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('46','1Corintios','','16');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('47','2Corintios','','13');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('48','Gálatas','','6');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('49','Efesios','','6');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('50','Filipenses','','4');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('51','Colosenses','','4');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('52','1Tesalonicenses','','5');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('53','2Tesalonicenses','','3');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('54','1Timoteo','','6');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('55','2Timoteo','','4');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('56','Tito','','3');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('57','Filemón','','1');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('58','Hebreos','','13');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('59','Santiago','','5');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('60','1Pedro','','5');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('61','2Pedro','','3');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('62','1Juan','','5');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('63','2Juan','','1');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('64','3Juan','','1');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('65','Judas','','1');
INSERT INTO `bible_books_es` (`number`,`fullname`,`short`,`chapters`) VALUES ('66','Revelación','','22');
