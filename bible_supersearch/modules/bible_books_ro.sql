
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the Romanian language.

DROP TABLE IF EXISTS `bible_books_ro`;
CREATE TABLE `bible_books_ro` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('1','Geneza ','','50');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('2','Exod ','','40');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('3','Levitic ','','27');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('4','Numeri ','','36');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('5','Deuteronom ','','34');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('6','Iosua ','','24');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('7','Judecatori ','','21');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('8','Rut ','','4');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('9','1 Samuel ','','31');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('10','2 Samuel ','','24');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('11','1 Imparati ','','22');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('12','2 Imparati ','','25');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('13','1 Cronici ','','29');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('14','2 Cronici ','','36');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('15','Ezra ','','10');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('16','Neemia ','','13');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('17','Estera ','','10');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('18','Iov ','','42');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('19','Psalmi ','','150');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('20','Proverbe ','','31');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('21','Ecclesiast ','','12');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('22','Cantarea Cantarilor ','','8');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('23','Isaia ','','66');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('24','Ieremia ','','52');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('25','Plângeri ','','48');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('26','Ezechiel ','','12');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('27','Daniel ','','5');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('28','Osea ','','14');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('29','Ioel ','','3');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('30','Amos ','','9');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('31','Obadia ','','1');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('32','Iona ','','4');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('33','Mica ','','7');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('34','Naum ','','3');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('35','Habacuc ','','3');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('36','Tefania ','','3');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('37','Hagai ','','2');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('38','Zaharia ','','14');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('39','Maleahi ','','4');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('40','Matei ','','28');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('41','Marcu ','','16');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('42','Luca ','','24');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('43','Ioan ','','21');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('44','Faptele Apostolilor ','','28');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('45','Romani ','','16');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('46','1 Corinteni ','','16');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('47','2 Corinteni ','','13');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('48','Galateni ','','6');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('49','Efeseni ','','6');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('50','Filipeni ','','4');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('51','Coloseni ','','4');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('52','1 Tesaloniceni ','','5');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('53','2 Tesaloniceni ','','3');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('54','1 Timotei ','','6');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('55','2 Timotei ','','4');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('56','Tit ','','3');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('57','Filimon ','','1');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('58','Evrei ','','13');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('59','Iacob ','','5');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('60','1 Petru ','','5');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('61','2 Petru ','','3');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('62','1 Ioan ','','5');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('63','2 Ioan ','','1');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('64','3 Ioan ','','1');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('65','Iuda ','','1');
INSERT INTO `bible_books_ro` (`number`,`fullname`,`short`,`chapters`) VALUES ('66','Apocalipsa ','','22');
