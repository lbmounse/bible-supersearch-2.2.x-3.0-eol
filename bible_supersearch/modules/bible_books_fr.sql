
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the French language.

DROP TABLE IF EXISTS `bible_books_fr`;
CREATE TABLE `bible_books_fr` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('1','Genèse ','','50');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('2','Exode ','','40');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('3','Lévitique ','','27');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('4','Nombres ','','36');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('5','Deutéronome ','','34');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('6','Josué ','','24');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('7','Juges ','','21');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('8','Ruth ','','4');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('9','1 Samuel ','','31');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('10','2 Samuel ','','24');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('11','1 Rois ','','22');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('12','2 Rois ','','25');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('13','1 Chroniques ','','29');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('14','2 Chroniques ','','36');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('15','Esdras ','','10');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('16','Néhémie ','','13');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('17','Esther ','','10');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('18','Job ','','42');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('19','Psaume ','','150');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('20','Proverbes ','','31');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('21','Ecclésiaste ','','12');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('22','Cantique Des Cantiqu ','','8');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('23','Ésaïe ','','66');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('24','Jérémie ','','52');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('25','Lamentations ','','48');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('26','Ézéchiel ','','12');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('27','Daniel ','','5');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('28','Osée ','','14');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('29','Joël ','','3');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('30','Amos ','','9');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('31','Abdias ','','1');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('32','Jonas ','','4');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('33','Michée ','','7');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('34','Nahum ','','3');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('35','Habacuc ','','3');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('36','Sophonie ','','3');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('37','Aggée ','','2');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('38','Zacharie ','','14');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('39','Malachie ','','4');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('40','Matthieu ','','28');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('41','Marc ','','16');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('42','Luc ','','24');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('43','Jean ','','21');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('44','Actes ','','28');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('45','Romains ','','16');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('46','1 Corinthiens ','','16');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('47','2 Corinthiens ','','13');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('48','Galates ','','6');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('49','Éphésiens ','','6');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('50','Philippiens ','','4');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('51','Colossiens ','','4');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('52','1 Thessaloniciens ','','5');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('53','2 Thessaloniciens ','','3');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('54','1 Timothée ','','6');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('55','2 Timothée ','','4');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('56','Tite ','','3');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('57','Philémon ','','1');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('58','Hébreux ','','13');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('59','Jacques ','','5');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('60','1 Pierre ','','5');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('61','2 Pierre ','','3');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('62','1 Jean ','','5');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('63','2 Jean ','','1');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('64','3 Jean ','','1');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('65','Jude ','','1');
INSERT INTO `bible_books_fr` (`number`,`fullname`,`short`,`chapters`) VALUES ('66','Apocalypse ','','22');
