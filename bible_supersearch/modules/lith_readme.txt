
Bible SuperSearch module for
LIthuanian Bible (lith)

For more Bibles, please see www.BibleSuperSearch.com

This is an unofficial module, and is NOT supported by BibleSuperSearch.com
Requirements: Bible SuperSearch >= 1.6.30

Installation Instructions
* Extract all files
* Upload all these files to your Bible SuperSearch directory
* Open the Bible Manager ("bible_install.php")
* Install "lith"
* Once installed, test it
* If all is well, you are done!

