
Bible SuperSearch module for
Reina Valera 1858 NT (1858rv)

For more Bibles, please see www.BibleSuperSearch.com

This is an official module, and is supported by BibleSuperSearch.com
Requirements: Bible SuperSearch >= 1.6.30

Installation Instructions
* Extract all files
* Upload all these files to your Bible SuperSearch directory
* Open the Bible Manager ("bible_install.php")
* Install "1858rv"
* Once installed, test it
* If all is well, you are done!

