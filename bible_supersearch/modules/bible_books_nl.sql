
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the Dutch language.

DROP TABLE IF EXISTS `bible_books_nl`;
CREATE TABLE `bible_books_nl` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('1','Genesis ','','50');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('2','Exodus ','','40');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('3','Leviticus ','','27');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('4','Numberi ','','36');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('5','Deuteronomium ','','34');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('6','Jozua ','','24');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('7','Richtere ','','21');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('8','Ruth ','','4');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('9','1 Samuël ','','31');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('10','2 Samuël ','','24');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('11','1 Koningen ','','22');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('12','2 Koningen ','','25');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('13','1 Kronieken ','','29');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('14','2 Kronieken ','','36');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('15','Ezra ','','10');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('16','Nehemia ','','13');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('17','Esther ','','10');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('18','Job ','','42');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('19','Psalmen ','','150');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('20','Spreuken ','','31');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('21','Prediker ','','12');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('22','Hooglied ','','8');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('23','Jesaja ','','66');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('24','Jeremia ','','52');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('25','Klaagliederen ','','48');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('26','Ezechiël ','','12');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('27','Daniël ','','5');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('28','Hosea ','','14');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('29','Joël ','','3');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('30','Amos ','','9');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('31','Obadja ','','1');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('32','Jona ','','4');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('33','Micha ','','7');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('34','Nahum ','','3');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('35','Habakuk ','','3');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('36','Zefanja ','','3');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('37','Haggaï ','','2');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('38','Zacharia ','','14');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('39','Maleachi ','','4');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('40','Mattheüs ','','28');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('41','Markus ','','16');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('42','Lukas ','','24');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('43','Johannes ','','21');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('44','Handelingen ','','28');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('45','Romeinen ','','16');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('46','1 Corinthiërs ','','16');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('47','2 Corinthiër ','','13');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('48','Galaten ','','6');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('49','Efeziërs ','','6');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('50','Filippenzen ','','4');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('51','Colossenzen ','','4');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('52','1 Thessalonicenzen ','','5');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('53','2 Thessalonicenzen ','','3');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('54','1 Timotheüs ','','6');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('55','2 Timotheüs ','','4');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('56','Titus ','','3');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('57','Filémon ','','1');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('58','Hebreeën ','','13');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('59','Jakobus ','','5');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('60','1 Petrus ','','5');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('61','2 Petrus ','','3');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('62','1 Johannes ','','5');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('63','2 Johannes ','','1');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('64','3 Johannes ','','1');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('65','Judas ','','1');
INSERT INTO `bible_books_nl` (`number`,`fullname`,`short`,`chapters`) VALUES ('66','Openbaring ','','22');
