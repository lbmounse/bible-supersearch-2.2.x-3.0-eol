
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the Arabic language.

DROP TABLE IF EXISTS `bible_books_ar`;
CREATE TABLE `bible_books_ar` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('1','﻿ﻦﻳﻮﻜﺗ ','','50');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('2','ﺝﻭﺮﺨﻟﺍ ','','40');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('3','ﻲﻳﻭﻼﻟﺍ ','','27');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('4','ﺩﺪﻌﻟﺍ ','','36');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('5','ﺔﻴﻨﺜﺘﻟﺍ ','','34');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('6','ﻉﻮﺸﻳ ','','24');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('7','ﺓﺎﻀﻘﻟﺍ ','','21');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('8','ﺙﻮﻋﺍﺭ ','','4');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('9','ﻝﻭﻷﺍ ﻞﻴﺋﻮﻤﺻ ','','31');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('10','ﻲﻧﺎﺜﻟﺍ ﻞﻴﺋﻮﻤﺻ ','','24');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('11','ﻭﻷﺍ ﻙﻮﻠﻤﻟﺍ ','','22');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('12','ﻲﻧﺎﺜﻟﺍ ﻙﻮﻠﻤﻟﺍ ','','25');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('13','ﻷﺍ ﻡﺎﻳﻷﺍ ﺭﺎﺒﺧﺃ ','','29');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('14','ﻥﺎﺜﻟﺍ ﻡﺎﻳﻷﺍ ﺭﺎﺒﺧﺃ ','','36');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('15','ﺍﺭﺰﻋ ','','10');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('16','ﺎﻴﻤﺤﻧ ','','13');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('17','ﺮﻴﺘﺳﺃ ','','10');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('18','ﺏﻮﻳﺃ ','','42');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('19','ﺮﻴﻣﺍﺰﻤﻟﺍ ','','150');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('20','ﻝﺎﺜﻣﺃ ','','31');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('21','ﺔﻌﻣﺎﺠﻟﺍ ','','12');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('22','ﺎﺸﻧﻷﺍ ﺪﻴﺸﻧ ','','8');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('23','ءﺎﻴﻌﺷﺃ ','','66');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('24','ءﺎﻴﻣﺭﺃ ','','52');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('25','ﺎﻴﻣﺭﺇ ﻲﺛﺍﺮﻣ ','','48');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('26','ﻝﺎﻴﻗﺰﺣ ','','12');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('27','ﻝﺎﻴﻧﺍﺩ ','','5');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('28','ﻊﺷﻮﻫ ','','14');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('29','ﻞﻴﺋﻮﻳ ','','3');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('30','ﺱﻮﻣﺎﻋ ','','9');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('31','ﺎﻳﺪﺑﻮﻋ ','','1');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('32','ﻥﺎﻧﻮﻳ ','','4');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('33','ﺎﺨﻴﻣ ','','7');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('34','ﻡﻮﺣﺎﻧ ','','3');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('35','ﻕﻮﻘﺒﺣ ','','3');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('36','ﺎﻴﻨﻔﺻ ','','3');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('37','ﻲﺠﺣ ','','2');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('38','ﺎﻳﺮﻛﺯ ','','14');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('39','ﺥﻼﻣ ','','4');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('40','ﻰﺘﻣ ','','28');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('41','ﺲﻗﺮﻣ ','','16');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('42','ﺎﻗﻮﻟ ','','24');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('43','ﺎﻨﺣﻮﻳ ','','21');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('44','ﻞﺳﺮﻟﺍ ﻝﺎﻤﻋﺍ ','','28');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('45','ﺔﻴﻣﻭﺭ ','','16');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('46','ﻝﻭﻻﺍ ﺱﻮﺜﻧﺭﻮﻛ ','','16');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('47','ﺔﻴﻧﺎﺜﻟﺍ ﺱﻮﺜﻧﺭﻮﻛ ','','13');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('48','ﻲﻃﻼﻏ ','','6');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('49','ﺲﺴﻓﺃ ','','6');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('50','ﻲﺒﻴﻠﻴﻓ ','','4');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('51','ﻲﺳﻮﻟﻮﻛ ','','4');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('52','ﻲﻜﻴﻧﻮﻟﺎﺴﺗ ﻝﻭﻻﺍ ','','5');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('53','ﻲﻜﻴﻧﻮﻟﺎﺴﺗ ﺔﻴﻧﺎﺜﻟﺍ ','','3');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('54','ﺱﻭﺎﺛﻮﻤﻴﺗ ﻝﻭﻻﺍ ','','6');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('55','ﺱﻭﺎﺛﻮﻤﻴﺗ ﺔﻴﻧﺎﺜﻟﺍ ','','4');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('56','ﺲﻄﻴﺗ ','','3');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('57','ﻥﻮﻤﻴﻠﻓ ','','1');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('58','ﻦﻴﻴﻧﺍﺮﺒﻌﻟﺍ ','','13');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('59','ﺏﻮﻘﻌﻳ ','','5');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('60','ﻝﻭﻻﺍ ﺱﺮﻄﺑ ','','5');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('61','ﺔﻴﻧﺎﺜﻟﺍ ﺱﺮﻄﺑ ','','3');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('62','ﻝﻭﻻﺍ ﺎﻨﺣﻮﻳ ','','5');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('63','ﺔﻴﻧﺎﺜﻟﺍ ﺎﻨﺣﻮﻳ ','','1');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('64','ﺔﺜﻟﺎﺜﻟﺍ ﺎﻨﺣﻮﻳ ','','1');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('65','ﺍﺫﻮﻬﻳ ','','1');
INSERT INTO `bible_books_ar` (`number`,`fullname`,`short`,`chapters`) VALUES ('66','ﺎﻳﺅﺭ ﺎﻨﺣﻮﻳ ','','22');
