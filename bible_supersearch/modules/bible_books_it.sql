
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the Italian language.

DROP TABLE IF EXISTS `bible_books_it`;
CREATE TABLE `bible_books_it` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('1','Genesi ','','50');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('2','Esodo ','','40');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('3','Levitico ','','27');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('4','Numeri ','','36');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('5','Deuteronomio ','','34');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('6','Giosué ','','24');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('7','Giudici ','','21');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('8','Rut ','','4');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('9','1 Samuele ','','31');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('10','2 Samuele ','','24');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('11','1 Re ','','22');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('12','2 Re ','','25');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('13','1 Cronache ','','29');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('14','2 Cronache ','','36');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('15','Esdra ','','10');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('16','Neemia ','','13');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('17','Ester ','','10');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('18','Giobbe ','','42');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('19','Salmi ','','150');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('20','Proverbi ','','31');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('21','Ecclesiaste ','','12');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('22','Cantico Dei Cantici ','','8');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('23','Isaia ','','66');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('24','Geremia ','','52');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('25','Lamentazioni ','','48');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('26','Ezechiele ','','12');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('27','Daniele ','','5');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('28','Osea ','','14');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('29','Gioele ','','3');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('30','Amos ','','9');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('31','Abdia ','','1');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('32','Giona ','','4');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('33','Michea ','','7');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('34','Nahum ','','3');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('35','Abacuc ','','3');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('36','Sofonia ','','3');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('37','Aggeo ','','2');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('38','Zaccaria ','','14');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('39','Malachia ','','4');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('40','Matteo ','','28');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('41','Marco ','','16');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('42','Luca ','','24');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('43','Giovanni ','','21');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('44','Atti ','','28');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('45','Romani ','','16');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('46','1 Corinzi ','','16');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('47','2 Corinzi ','','13');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('48','Galati ','','6');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('49','Efesini ','','6');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('50','Filippesi ','','4');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('51','Colossesi ','','4');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('52','1 Tessalonicesi ','','5');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('53','2 Tessalonicesi ','','3');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('54','1 Timoteo ','','6');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('55','2 Timoteo ','','4');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('56','Tito ','','3');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('57','Filemone ','','1');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('58','Ebrei ','','13');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('59','Giacomo ','','5');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('60','1 Pietro ','','5');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('61','2 Pietro ','','3');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('62','1 Giovanni ','','5');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('63','2 Giovanni ','','1');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('64','3 Giovanni ','','1');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('65','Giuda ','','1');
INSERT INTO `bible_books_it` (`number`,`fullname`,`short`,`chapters`) VALUES ('66','Apocalisse ','','22');
