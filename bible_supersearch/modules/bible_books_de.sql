-- phpMyAdmin SQL Dump
-- version 2.8.1-Debian-1~dapper1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Sep 30, 2006 at 03:23 PM
-- Server version: 4.1.15
-- PHP Version: 5.1.2
-- 
-- Database: `aic`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `bible_books_de`
-- 

CREATE TABLE `bible_books_de` (  `number` int(11) NOT NULL auto_increment,  `fullname` tinytext NOT NULL,  `short` tinytext,  `chapters` int(11) NOT NULL default '0',  PRIMARY KEY  (`number`)) ENGINE=MyISAM ;

-- 
-- Dumping data for table `bible_books_de`
-- 

INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (1, '1Mose', '', 50);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (2, '2Mose', '', 40);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (3, '3Mose', '', 27);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (4, '4Mose', '', 36);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (5, '5Mose', '', 34);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (6, 'Josua', '', 24);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (7, 'Richter', '', 21);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (8, 'Rut', '', 4);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (9, '1Samuel', '', 31);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (10, '2Samuel', '', 24);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (11, '1Koenige', '', 22);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (12, '2Koenige', '', 25);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (13, '1Chronik', '', 29);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (14, '2Chronik', '', 36);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (15, 'Esra', '', 10);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (16, 'Nehemia', '', 13);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (17, 'Ester', '', 10);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (18, 'Job', '', 42);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (19, 'Psalm', '', 150);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (20, 'Sprueche', '', 31);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (21, 'Prediger', '', 12);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (22, 'Hohelied', '', 8);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (23, 'Jesaja', '', 66);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (24, 'Jeremia', '', 52);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (25, 'Klagelieder', '', 5);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (26, 'Hesekiel', '', 48);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (27, 'Daniel', '', 12);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (28, 'Hosea', '', 14);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (29, 'Joel', '', 3);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (30, 'Amos', '', 9);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (31, 'Obadja', '', 1);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (32, 'Jona', '', 4);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (33, 'Mica', '', 7);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (34, 'Nahum', '', 3);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (35, 'Habakuk', '', 3);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (36, 'Zephanja', '', 3);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (37, 'Haggai', '', 2);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (38, 'Sacharja', '', 14);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (39, 'Maleachi', '', 4);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (40, 'Matthaeus', '', 28);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (41, 'Markus', '', 16);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (42, 'Lukas', '', 24);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (43, 'Johannes', '', 21);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (44, 'Apostelgeschichte', '', 28);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (45, 'Roemers', '', 16);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (46, '1Korinther', '', 16);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (47, '2Korinther', '', 13);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (48, 'Galater', '', 6);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (49, 'Epheser', '', 6);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (50, 'Philipper', '', 4);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (51, 'Kolosser', '', 4);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (52, '1Thessalonicher', '', 5);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (53, '2Thessalonicher', '', 3);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (54, '1Timotheus', '', 6);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (55, '2Timotheus', '', 4);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (56, 'Titus', '', 3);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (57, 'Philemon', '', 1);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (58, 'Hebraeer', '', 13);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (59, 'Jakobus', '', 5);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (60, '1Petrus', '', 5);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (61, '2Petrus', '', 3);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (62, '1Johannes', '', 5);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (63, '2Johannes', '', 1);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (64, '3Johannes', '', 1);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (65, 'Judas', '', 1);
INSERT INTO `bible_books_de` (`number`, `fullname`, `short`, `chapters`) VALUES (66, 'Offenbarung', '', 22);
