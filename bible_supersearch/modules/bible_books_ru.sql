
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the Russian language.

DROP TABLE IF EXISTS `bible_books_ru`;
CREATE TABLE `bible_books_ru` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('1','бытие ','','50');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('2','Исход ','','40');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('3','Левит ','','27');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('4','Числа ','','36');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('5','Второзаконие ','','34');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('6','Иисус Навин ','','24');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('7','Книга Судей ','','21');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('8','Руфь ','','4');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('9','1-Я Царств ','','31');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('10','2-Я Царств ','','24');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('11','3-Я Царств ','','22');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('12','4-Я Царств ','','25');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('13','1-Я Паралипоменон ','','29');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('14','2-Я Паралипоменон ','','36');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('15','Ездра ','','10');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('16','Неемия ','','13');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('17','Есфирь ','','10');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('18','Иов ','','42');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('19','Псалтирь ','','150');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('20','Притчи ','','31');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('21','Екклесиаст ','','12');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('22','Песни Песней ','','8');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('23','Исаия ','','66');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('24','Иеремия ','','52');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('25','Плач Иеремии ','','48');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('26','Иезекииль ','','12');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('27','Даниил ','','5');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('28','Осия ','','14');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('29','Иоиль ','','3');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('30','Амос ','','9');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('31','Авдия ','','1');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('32','Иона ','','4');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('33','Михей ','','7');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('34','Наум ','','3');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('35','Аввакум ','','3');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('36','Софония ','','3');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('37','Аггей ','','2');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('38','Захария ','','14');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('39','Малахия ','','4');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('40','От Матфея ','','28');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('41','От Марка ','','16');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('42','От Луки ','','24');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('43','От Иоанна ','','21');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('44','Деяния ','','28');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('45','К Римлянам ','','16');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('46','1-Е Коринфянам ','','16');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('47','2-Е Коринфянам ','','13');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('48','К Галатам ','','6');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('49','К Ефесянам ','','6');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('50','К Филиппийцам ','','4');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('51','К Колоссянам ','','4');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('52','1-Е Фессалоникийцам ','','5');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('53','2-Е Фессалоникийцам ','','3');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('54','1-Е Тимофею ','','6');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('55','2-Е Тимофею ','','4');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('56','К Титу ','','3');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('57','К Филимону ','','1');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('58','К Евреям ','','13');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('59','Иакова ','','5');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('60','1-E Петра ','','5');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('61','2-E Петра ','','3');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('62','1-E Иоанна ','','5');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('63','2-E Иоанна ','','1');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('64','3-E Иоанна ','','1');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('65','Иуда ','','1');
INSERT INTO `bible_books_ru` (`number`,`fullname`,`short`,`chapters`) VALUES ('66','Откровение ','','22');
