
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the Hungarian language.

DROP TABLE IF EXISTS `bible_books_hu`;
CREATE TABLE `bible_books_hu` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('1','1 Mózes ','','50');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('2','2 Mózes ','','40');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('3','3 Mózes ','','27');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('4','4 Mózes ','','36');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('5','5 Mózes ','','34');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('6','Józsué ','','24');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('7','Birák ','','21');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('8','Ruth ','','4');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('9','1 Sámuel ','','31');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('10','2 Sámuel ','','24');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('11','1 Királyok ','','22');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('12','2 Királyok ','','25');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('13','1 Krónika ','','29');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('14','2 Krónika ','','36');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('15','Ezsdrás ','','10');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('16','Nehemiás ','','13');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('17','Eszter ','','10');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('18','Jób ','','42');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('19','Zsoltárok ','','150');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('20','Példabeszédek ','','31');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('21','Prédikátor ','','12');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('22','Énekek Éneke ','','8');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('23','Ézsaiás ','','66');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('24','Jeremiás ','','52');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('25','Jeremiás Sir ','','48');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('26','Ezékiel ','','12');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('27','Dániel ','','5');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('28','Hóseás ','','14');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('29','Jóel ','','3');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('30','Ámos ','','9');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('31','Abdiás ','','1');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('32','Jónás ','','4');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('33','Mikeás ','','7');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('34','Náhum ','','3');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('35','Habakuk ','','3');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('36','Sofoniás ','','3');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('37','Aggeus ','','2');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('38','Zakariás ','','14');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('39','Malakiás ','','4');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('40','Máté ','','28');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('41','Márk ','','16');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('42','Lukács ','','24');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('43','János ','','21');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('44','Apostolok ','','28');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('45','Rómaiakhoz ','','16');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('46','1 Korintusi ','','16');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('47','2 Korintusi ','','13');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('48','Galatákhoz ','','6');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('49','Efézusiakhoz ','','6');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('50','Filippiekhez ','','4');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('51','Kolosséiakhoz ','','4');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('52','1 Tesszalonika ','','5');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('53','2 Tesszalonika ','','3');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('54','1 Timóteushoz ','','6');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('55','2 Timóteushoz ','','4');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('56','Titushoz ','','3');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('57','Filemonhoz ','','1');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('58','Zsidókhoz ','','13');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('59','Jakab ','','5');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('60','1 Péter ','','5');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('61','2 Péter ','','3');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('62','1 János ','','5');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('63','2 János ','','1');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('64','3 János ','','1');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('65','Júdás ','','1');
INSERT INTO `bible_books_hu` (`number`,`fullname`,`short`,`chapters`) VALUES ('66','Jelenések ','','22');
