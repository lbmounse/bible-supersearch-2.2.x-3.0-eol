
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the Chinese language.

DROP TABLE IF EXISTS `bible_books_zh`;
CREATE TABLE `bible_books_zh` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('1','﻿創 世 記 ','','50');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('2','出 埃 及 記 ','','40');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('3','利 未 記 ','','27');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('4','民 數 記 ','','36');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('5','申 命 記 ','','34');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('6','約 書 亞 記 ','','24');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('7','士 師 記 ','','21');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('8','路 得 記 ','','4');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('9','撒 母 耳 記 上 ','','31');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('10','撒 母 耳 記 下 ','','24');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('11','列 王 紀 上 ','','22');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('12','列 王 紀 下 ','','25');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('13','歷 代 志 上 ','','29');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('14','歷 代 志 下 ','','36');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('15','以 斯 拉 記 ','','10');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('16','尼 希 米 記 ','','13');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('17','以 斯 帖 記 ','','10');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('18','約 伯 記 ','','42');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('19','詩 篇 ','','150');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('20','箴 言 ','','31');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('21','傳 道 書 ','','12');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('22','雅 歌 ','','8');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('23','以 賽 亞 書 ','','66');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('24','耶 利 米 書 ','','52');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('25','耶 利 米 哀 歌 ','','12');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('26','以 西 結 書 ','','5');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('27','但 以 理 書 ','','48');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('28','何 西 阿 書 ','','14');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('29','約 珥 書 ','','3');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('30','阿 摩 司 書 ','','9');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('31','俄 巴 底 亞 書 ','','1');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('32','約 拿 書 ','','4');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('33','彌 迦 書 ','','7');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('34','那 鴻 書 ','','3');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('35','哈 巴 谷 書 ','','3');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('36','西 番 雅 書 ','','3');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('37','哈 該 書 ','','2');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('38','撒 迦 利 亞 ','','14');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('39','瑪 拉 基 書 ','','4');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('40','馬 太 福 音 ','','28');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('41','馬 可 福 音 ','','16');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('42','路 加 福 音 ','','24');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('43','約 翰 福 音 ','','21');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('44','使 徒 行 傳 ','','28');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('45','羅 馬 書 ','','16');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('46','歌 林 多 前 書 ','','16');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('47','歌 林 多 後 書 ','','13');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('48','加 拉 太 書 ','','6');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('49','以 弗 所 書 ','','6');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('50','腓 立 比 書 ','','4');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('51','歌 羅 西 書 ','','4');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('52','帖 撒 羅 尼 迦 前 書 ','','5');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('53','帖 撒 羅 尼 迦 後 書 ','','3');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('54','提 摩 太 前 書 ','','6');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('55','提 摩 太 後 書 ','','4');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('56','提 多 書 ','','3');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('57','腓 利 門 書 ','','1');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('58','希 伯 來 書 ','','13');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('59','雅 各 書 ','','5');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('60','彼 得 前 書 ','','5');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('61','彼 得 後 書 ','','3');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('62','約 翰 一 書 ','','5');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('63','約 翰 二 書 ','','1');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('64','約 翰 三 書 ','','1');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('65','猶 大 書 ','','1');
INSERT INTO `bible_books_zh` (`number`,`fullname`,`short`,`chapters`) VALUES ('66','启 示 录 ','','22');
