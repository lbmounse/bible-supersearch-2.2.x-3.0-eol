
Bible SuperSearch module for
La Bible de l'Épée (epee)

For more Bibles, please see www.BibleSuperSearch.com

This module is officially supported by BibleSuperSearch.com
Requirements: Bible SuperSearch >= 1.5.07 (>= 1.6.30 highly reccomended)

Installation Instructions
* Extract all files
* Upload all these files to your Bible SuperSearch directory
* Open the Bible Manager ("bible_install.php")
* Install "epee"
* Once installed, test it
* If all is well, you are done!

