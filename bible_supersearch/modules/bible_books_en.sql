
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the English language.

DROP TABLE IF EXISTS `bible_books_en`;
CREATE TABLE `bible_books_en` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('1','Genesis','Gn','50');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('2','Exodus','Exo','40');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('3','Leviticus','Lv','27');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('4','Numbers','Nm','36');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('5','Deuteronomy','Dt','34');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('6','Joshua','Josh','24');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('7','Judges','Jdg','21');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('8','Ruth','Rth','4');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('9','1 Samuel','1Samuel 1Sm 1 Sm','31');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('10','2 Samuel','2Samuel 2Sm 2 Sm','24');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('11','1 Kings','1Kings 1Kgs 1 Kgs','22');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('12','2 Kings','2Kings 2Kgs 2 Kgs','25');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('13','1 Chronicles','1Chronicles','29');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('14','2 Chronicles','2Chronicles','36');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('15','Ezra','Era','10');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('16','Nehemiah','Neh','13');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('17','Esther','Est','10');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('18','Job','Jb','42');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('19','Psalms','Ps','150');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('20','Proverbs','Prov','31');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('21','Ecclesiastes','Ecc','12');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('22','Song of Solomon','SOS Song of Songs','8');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('23','Isaiah','Isa','66');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('24','Jeremiah','Jer','52');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('26','Ezekiel','Eze','48');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('27','Daniel','Dn','12');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('25','Lamentations','Lam','5');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('28','Hosea','Hos','14');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('29','Joel','Jol Jl','3');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('30','Amos','Ams','9');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('31','Obadiah','Oba','1');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('32','Jonah','Jon','4');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('33','Micah','Mch','7');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('34','Nahum','Nhm','3');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('35','Habakkuk','Hb','3');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('36','Zephaniah','Zep','3');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('37','Haggai','Hg','2');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('38','Zechariah','Zec','14');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('39','Malachi','Mal','4');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('40','Matthew','Mt','28');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('41','Mark','Mk','16');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('42','Luke','Lk','24');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('43','John','Jn','21');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('44','Acts','Act','28');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('45','Romans','Rm','16');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('46','1 Corinthians','1Corinthians','16');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('47','2 Corinthians','2Corinthians','13');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('48','Galatians','Gal','6');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('49','Ephesians','Eph','6');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('50','Philippians','Phil','4');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('51','Colossians','Col','4');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('52','1 Thessalonians','1Thessalonians','5');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('53','2 Thessalonians','2Thessalonians','3');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('54','1 Timothy','1Timothy','6');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('55','2 Timothy','2Timothy','4');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('56','Titus','Tts','3');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('57','Philemon','Phlm Phm','1');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('58','Hebrews','Heb','13');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('59','James','Jas','5');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('60','1 Peter','1Peter 1Pt 1 Pt','5');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('61','2 Peter','2Peter 2Pt 2 Pt','3');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('62','1 John','1Jn 1 Jn 1John','5');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('63','2 John','2Jn 2 Jn 2John','1');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('64','3 John','3Jn 3 Jn 3John','1');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('65','Jude','Jde','1');
INSERT INTO `bible_books_en` (`number`,`fullname`,`short`,`chapters`) VALUES ('66','Revelation','Rv Apocalypse','22');
