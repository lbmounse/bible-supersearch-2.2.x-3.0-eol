-- Bible SuperSearch
-- This sets up the Bible book data table
-- And the search limiter (shortcut) tables
-- (And any other MySQL tables that may be needed in the future)
-- 
-- www.BibleSuperSearch.com
-- Copyright (C) 2006 Luke Mounsey
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License
-- as published by the Free Software Foundation; either version 2
-- of the License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License included in licence.txt for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
--
-- --------------------------------------------------------
-- 
-- Table structure for table `bible_books_en`
-- 

DROP TABLE IF EXISTS `bible_books_en`;
CREATE TABLE `bible_books_en` (  `number` int(11) NOT NULL auto_increment,  `fullname` tinytext NOT NULL,  `short` tinytext NOT NULL,  `chapters` int(11) NOT NULL default '0',  PRIMARY KEY  (`number`)) ;

-- 
-- Data for table `bible_books_en`
-- 

INSERT INTO `bible_books_en` VALUES (1, 'Genesis', 'Gn', 50);
INSERT INTO `bible_books_en` VALUES (2, 'Exodus', 'Exo', 40);
INSERT INTO `bible_books_en` VALUES (3, 'Leviticus', 'Lv', 27);
INSERT INTO `bible_books_en` VALUES (4, 'Numbers', 'Nm', 36);
INSERT INTO `bible_books_en` VALUES (5, 'Deuteronomy', 'Dt', 34);
INSERT INTO `bible_books_en` VALUES (6, 'Joshua', 'Josh', 24);
INSERT INTO `bible_books_en` VALUES (7, 'Judges', 'Jdg', 21);
INSERT INTO `bible_books_en` VALUES (8, 'Ruth', 'Rth', 4);
INSERT INTO `bible_books_en` VALUES (9, '1 Samuel', '1Samuel 1Sm 1 Sm', 31);
INSERT INTO `bible_books_en` VALUES (10, '2 Samuel', '2Samuel 2Sm 2 Sm', 24);
INSERT INTO `bible_books_en` VALUES (11, '1 Kings', '1Kings 1Kgs 1 Kgs', 22);
INSERT INTO `bible_books_en` VALUES (12, '2 Kings', '2Kings 2Kgs 2 Kgs', 25);
INSERT INTO `bible_books_en` VALUES (13, '1 Chronicles', '1Chronicles', 29);
INSERT INTO `bible_books_en` VALUES (14, '2 Chronicles', '2Chronicles', 36);
INSERT INTO `bible_books_en` VALUES (15, 'Ezra', 'Era', 10);
INSERT INTO `bible_books_en` VALUES (16, 'Nehemiah', 'Neh', 13);
INSERT INTO `bible_books_en` VALUES (17, 'Esther', 'Est', 10);
INSERT INTO `bible_books_en` VALUES (18, 'Job', 'Jb', 42);
INSERT INTO `bible_books_en` VALUES (19, 'Psalms', 'Ps', 150);
INSERT INTO `bible_books_en` VALUES (20, 'Proverbs', 'Prov', 31);
INSERT INTO `bible_books_en` VALUES (21, 'Ecclesiastes', 'Ecc', 12);
INSERT INTO `bible_books_en` VALUES (22, 'Song of Solomon', 'SOS Song of Songs', 8);
INSERT INTO `bible_books_en` VALUES (23, 'Isaiah', 'Isa', 66);
INSERT INTO `bible_books_en` VALUES (24, 'Jeremiah', 'Jer', 52);
INSERT INTO `bible_books_en` VALUES (26, 'Ezekiel', 'Eze', 48);
INSERT INTO `bible_books_en` VALUES (27, 'Daniel', 'Dn', 12);
INSERT INTO `bible_books_en` VALUES (25, 'Lamentations', 'Lam', 5);
INSERT INTO `bible_books_en` VALUES (28, 'Hosea', 'Hos', 14);
INSERT INTO `bible_books_en` VALUES (29, 'Joel', 'Jol Jl', 3);
INSERT INTO `bible_books_en` VALUES (30, 'Amos', 'Ams', 9);
INSERT INTO `bible_books_en` VALUES (31, 'Obadiah', 'Oba', 1);
INSERT INTO `bible_books_en` VALUES (32, 'Jonah', 'Jon', 4);
INSERT INTO `bible_books_en` VALUES (33, 'Micah', 'Mch', 7);
INSERT INTO `bible_books_en` VALUES (34, 'Nahum', 'Nhm', 3);
INSERT INTO `bible_books_en` VALUES (35, 'Habakkuk', 'Hb', 3);
INSERT INTO `bible_books_en` VALUES (36, 'Zephaniah', 'Zep', 3);
INSERT INTO `bible_books_en` VALUES (37, 'Haggai', 'Hg', 2);
INSERT INTO `bible_books_en` VALUES (38, 'Zechariah', 'Zec', 14);
INSERT INTO `bible_books_en` VALUES (39, 'Malachi', 'Mal', 4);
INSERT INTO `bible_books_en` VALUES (40, 'Matthew', 'Mt', 28);
INSERT INTO `bible_books_en` VALUES (41, 'Mark', 'Mk', 16);
INSERT INTO `bible_books_en` VALUES (42, 'Luke', 'Lk', 24);
INSERT INTO `bible_books_en` VALUES (43, 'John', 'Jn', 21);
INSERT INTO `bible_books_en` VALUES (44, 'Acts', 'Act', 28);
INSERT INTO `bible_books_en` VALUES (45, 'Romans', 'Rm', 16);
INSERT INTO `bible_books_en` VALUES (46, '1 Corinthians', '1Corinthians', 16);
INSERT INTO `bible_books_en` VALUES (47, '2 Corinthians', '2Corinthians', 13);
INSERT INTO `bible_books_en` VALUES (48, 'Galatians', 'Gal', 6);
INSERT INTO `bible_books_en` VALUES (49, 'Ephesians', 'Eph', 6);
INSERT INTO `bible_books_en` VALUES (50, 'Philippians', 'Phil', 4);
INSERT INTO `bible_books_en` VALUES (51, 'Colossians', 'Col', 4);
INSERT INTO `bible_books_en` VALUES (52, '1 Thessalonians', '1Thessalonians', 5);
INSERT INTO `bible_books_en` VALUES (53, '2 Thessalonians', '2Thessalonians', 3);
INSERT INTO `bible_books_en` VALUES (54, '1 Timothy', '1Timothy', 6);
INSERT INTO `bible_books_en` VALUES (55, '2 Timothy', '2Timothy', 4);
INSERT INTO `bible_books_en` VALUES (56, 'Titus', 'Tts', 3);
INSERT INTO `bible_books_en` VALUES (57, 'Philemon', 'Phlm Phm', 1);
INSERT INTO `bible_books_en` VALUES (58, 'Hebrews', 'Heb', 13);
INSERT INTO `bible_books_en` VALUES (59, 'James', 'Jas', 5);
INSERT INTO `bible_books_en` VALUES (60, '1 Peter', '1Peter 1Pt 1 Pt', 5);
INSERT INTO `bible_books_en` VALUES (61, '2 Peter', '2Peter 2Pt 2 Pt', 3);
INSERT INTO `bible_books_en` VALUES (62, '1 John', '1Jn 1 Jn 1John', 5);
INSERT INTO `bible_books_en` VALUES (63, '2 John', '2Jn 2 Jn 2John', 1);
INSERT INTO `bible_books_en` VALUES (64, '3 John', '3Jn 3 Jn 3John', 1);
INSERT INTO `bible_books_en` VALUES (65, 'Jude', 'Jde', 1);
INSERT INTO `bible_books_en` VALUES (66, 'Revelation', 'Rv Apocalypse', 22);

-- --------------------------------------------------------

-- 
-- Table structure for table `bible_shortcuts_en`
-- 

DROP TABLE IF EXISTS `bible_shortcuts_en`;
CREATE TABLE `bible_shortcuts_en` (  `index` int(11) NOT NULL auto_increment,  `name` text NOT NULL,  `short1` tinytext NOT NULL,  `short2` tinytext NOT NULL,  `short3` tinytext NOT NULL,  `reference` tinytext NOT NULL,  `display` tinytext NOT NULL,  UNIQUE KEY `index` (`index`)) AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `bible_shortcuts_en`
-- 
INSERT INTO `bible_shortcuts_en` VALUES ('', 'Old Testament', 'OT', 'Tenakh', 'Old', 'Genesis - Malachi', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'New Testament', 'New', 'NT', 'New_Testament', 'Matthew - Revelation', 'yes');

INSERT INTO `bible_shortcuts_en` VALUES ('', 'Law', 'OT Law', 'Torah', 'Moses', 'Genesis - Deuteronomy', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'History', 'History', '', '', 'Joshua - Esther', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'Wisdom & Poetry', 'Wisdom', 'Poetry', 'Wisdom and Poetry', 'Job - Song of Solomon', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'Prophets', 'Prophets', 'All Prophets', '', 'Isaiah - Malachi', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'Major Prophets', 'Major_Prophets', 'Major', 'MajorProphets', 'Isaiah - Daniel', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'Minor Prophets', 'Minor_Prophets', 'Minor', 'MinorProphets', 'Hosea - Malachi', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'Gospels', 'Gosples', 'Gospel', 'Passion', 'Matthew - John', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'Epistles', 'NT Epistles', 'Doctrine', 'All Epistles', 'Romans - Jude', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'Pauline Epistles', 'Paul', 'Pauline', 'Pauline Epistles', 'Romans - Hebrews', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'General Epistles', 'General', 'General_Epistles', 'Epistles General', 'James - Jude', 'yes');
INSERT INTO `bible_shortcuts_en` VALUES ('', 'End Times Prophecy', 'Last Days', 'End', 'End Times', 'Revelation; Daniel; Matthew 24', 'no');
