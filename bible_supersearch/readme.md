# Bible SuperSearch 2.2.x

## [BibleSuperSearch.com](https://www.biblesupersearch.com)

### NOTICE:  AS OF MAY 2, 2020, THIS LEGACY SOFTWARE HAS GONE END-OF-LIFE, AND WILL NO LONGER BE SUPPORTED OR MAINTAINED.

#### NOTICE:  THIS LEGACY SOFTWARE IS NOT SUPPORTED IN PHP 7 +

## PLEASE DOWNLOAD AND INSTALL THE LATEST VERSION
### https://www.biblesupersearch.com/downloads/
### https://www.biblesupersearch.com/migration-legacy/