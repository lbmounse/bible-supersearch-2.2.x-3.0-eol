<?php
// SECURE

// Bible SuperSearch Version 2.1.00
// The free, open source, PHP and MySQL web-based   
// Bible Reference retrevial and search utility
//
// Copyright (C) 2006, 2007, 2008 Luke Mounsey
// www.BibleSuperSearch.com
// www.PreservedWord.com
// www.Alive-in-Christ.com
//
// bible_lookup.php
// Contains all functions needed to look up references and search the Bible.
// This does NOT contain functions for DISPLAYING the verses.
// Functions for displaying verses are in the file "bible_display.php"
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License included in the file 
// "license.txt" for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Program Name
$bss_name="Bible SuperSearch \"Exodus\" RELEASE";
// Program Version
$bss_version="2.1.00";
// Program Version Short
$bss_version_short="2.1";
// File Version
$bible_lookup_version=2.1;

ini_set("mysql.connect_timeout",300);

//error_reporting(E_ALL);

// Default Bible Version
//$bss_default_version="kjv";
$bible_version=$version;//$bss_default_version;
//$default_language="en";
//$language=$default_language;

$table_prefix="bible_";
// diagnostic mode
$diag=false;
global $language;

require_once("bible_misc.php"); 
require_once("bible_config.php");
require_once("bible_system.php");
$bss_default_version=$default_bible_version;

// getSearch
// Executes a lookup/search using the same paramaters
// @ param look - passage reference
// @ param sear - search query
// @ param vers - Bible version
// @ return - an (2 dimensional?) array of verse arrays

function getSearch($look="",$sear="",$vers="", $st=null){

//echo("getSearch<br>");



global $lookup, $search, $version,$proxrange, $version_array;

//print_out($version_array);

if(($look=="")&&($sear=="")){
$look=$lookup;
$sear=$search;
}
if($vers==""){$vers=$version;}
if($st==null){global $st;}

//echo("<br><BR>GetSearch: look $look sear $sear vers $vers");

// if parallel search, make sure all Bibles are the same language
if(($search!=null)&&(count($version_array)>1)){

$parallel_search = true;
$lang=getLanguage($version_array[0]);

$test=true;
foreach ($version_array as $bib)

$test=($lang==getLanguage($bib))&($test);
//echo(getLanguage($bib));
//echo("<br>|$bib|");

if(!$test){

echo("<center>Please note, you may only parallel search Bibles of the same language.</center><br>");
return null;

}// end if

}// end check parallel search
else {
	$parallel_search = false;
}

/*
$st=array("$searchtype");
if ($wholeword){$st[]="wholeword";}
else{$st[]="partial";}
*/

//echo("$look|$sear|$vers|".$st[0]."||".$st[1]."<br>");

if(($st[0]=="proximity")&&($search=="")){$st[0]="All Words";}


if(strpos($search,"PROX")!==false){


//$res=proxboolean($lookup,$search);

$st[0]="Boolean";
}

// basic proximity search
if ($st[0]=="proximity"){
$terms=explode(" ",$sear);
return proxand($look,$terms,$proxrange);
}

// look up references and reference/search combos

// Convert search reference shortcuts to the actual reference 
$look=rtrim($look,";");
$look=shortcut($look);
//echo("lookup |$look|");

$set_of_verses=array();
$ser_ver=array();

// separate the references
//echo("look $look<br>");
$lookupquery=explode(";",$look);

//print_out($lookupquery);

// PROCESS LOOKUP/SEARCH COMBINATIONS
$in=0;
while($lookupquery[$in]!=null){

//echo("|".$lookupquery[$in]."|");

$ref=explodeRef(trim($lookupquery[$in]));

//print_out($ref);

// if request for whole book and whole book = false and no search
// display only first chapter.
if (($wholebook!="true")&&($ref["chapter"]=="")&&($ref["verse"]=="")&&($sear=="")){
$ref["chapter"]=1;
}// end if

$verses=getVerses($ref["book"], $ref["chapter"], $ref["verse"], $sear, $st, $vers);
//printout($verses);

if(((is_array($verses))&(count($verses[0])!=0))||($sear=="")){
$set_of_verses[]=$verses;
}
//printout($set_of_verses);

if(strpos($search,"PROX")!==false){
//$set_of_verses=array(proxboolean($lookup,$search));
//$st[0]="Boolean";
}

//if(($sear!="")&&($verses!=null)){
//$ser_ver=array_merge($ser_ver,$verses);
//}

/*
// display error message if no verses are found

if (($verses[0]==null) || ($verses==null)){

// This displays a message when a search with a verse range limitation produces no 
// results.  For some reason, these if statments do not detect a case where an 
// unlimited search produces no results.   A similar if statement is able to 
// detect this case in displayVerses

if ($sear!=null){error("Your request for <span class='bss_errorsearch'>$lookupquery[$in]</span> searched for \"<span class='bss_errorsearch'>$search</span>\" returned no results.");}
else{error("Your request for \"<span class='bss_errorsearch'>$lookupquery[$in]</span>\" returned no results. &nbsp; Please edit your request and try again.");}
}// end if
*/

$in+=1;	
}// end while

// END LOOKUP/SEARCH COMBINATIONS

/*
echo("/".$ser_ver[1][0]["text"]."/ {g}  ".$verses[0][0]["text"]."<br><BR>");

echo("<pre>");
print_r($ser_ver);
echo("</pre>");

echo("<pre>");
print_r($verses);
echo("</pre>");
*/

// PROCESS SEARCHES

if (($sear!=null)&&(empty($look))){
// searches only, with no passage reference

$set_of_verses[]=getVerses("","","",$sear, $st, $vers);

// if the search returns no results, display a message
// (this doesn't actually detect search errors, simular to as mentioned above)
if ((empty($set_of_verses[0]))||(count($set_of_verses[0])==0)){
//error("Your search for \"<span class='bss_errorsearch'>$search</span>\" returned no results. &nbsp; Please edit your search and try again.");
}

else{$ser_ver=array_merge($ser_ver,$set_of_verses[0]);}// end else

}// end if (search only)

if($sear!=""){

$server=array();

$o=0;
while($ar=$set_of_verses[0][$o]){
$bibver=array();

$i=0;
while($arr=$set_of_verses[$i][$o]){

$bibver=array_merge($bibver,$arr);

//foreach($arr as $ar){$server[]=$ar;}

//$server=array_merge($server,$arr);

$i++;
}
$server[]=$bibver;
$o++;
}

/*
echo("<pre>");
print_r($set_of_verses);
echo("</pre>");
*/

return $server;

}
else{return $set_of_verses;}

}// end getSearch


// quickLookup
// Allows lookup of passages without specifiying search parameters
// @ param book - name or number of the book
// @ param chapter - chapter number
// @ param verse - verse number
// @ param version - Bible version to use - defaults to system default (kjv)
// @ returns - an array of verse arrays

function quickLookup($book, $chapter, $verse, $version=""){return getVerses($book, $chapter, $verse, "", "", $version);}

// quickSearch
// allows simple searching without having to specify lookup parameters
// @ param search - search query
// @ param searchtype - type of search to be performed - default system default
// @ param version - Bible version to use - defaults to system default (kjv)
// @ return array of verses
function quickSearch($search, $searchtype=""){return getVerses("","","",$search, $searchtype, $version);}

// getVerses - main Bible lookup and search function
// @ param book - name or number of the book
// @ param chapter - chapter number
// @ param verse - verse number
// @ param search - search query
// @ param searchtype - specifies all words, any word, exact phrase, or advanced search
// @ param version - short name of Texus Receptus Bible version to use
// @ returns - an array of verse arrays

function getVerses($book, $chapter, $verse, $search, $searchtype, $version="", $start=false,$end=false) {
	global $default_bible_version, $wholebook, $table_prefix, $language, $show_query, $max_verses;
	//echo("getVerses<br>");

	//echo("book $book chapter $chapter verse $verse search $search version $version<br>");

	splitSearch($search);

	if($version!=""){
	$ver=str_replace(array(",,,",",,"),",",$version);
	$version_array=explode(",",$ver);
	}
	else{global $version_array;}

	//printout($version_array);
	//if($version_array==null){$version_array=array($version);}
	//echo("|".$searchtype[0]."|");

	if($searchtype[0]=="regexp"){$searchtype[0]="Exact Phrase";}
	if ($version==""){$version=$bss_default_version;}
	if ($language==""){$language=getLanguage($version);}
	$num_ver=count($version_array);

	//$version=str_replace(" ","",$version);
	//$version_array=explode(",",$version);

	// semi-default language
	$lang=getLanguage($version_array[0]);
	$parallel_search=false;

	// if parallel search, make sure all Bibles are the same language
	if(($search!=null)&&(count($version_array)>1)) {
		$parallel_search=true;
		$test=true;
		foreach ($version_array as $bib)

		$test=($lang==getLanguage($bib))&($test);
		//echo(getLanguage($bib));
		if(!$test) {
			//echo("<center>Please note, you may only parallel search Bibles of the same language.</center><br>");
			return null;
		}// end if

		// also make sure it isn't a proximity search


	}// end check parallel search
	else{ $parallel_search = false; }

	//echo($parallel_search);

	// random passage
	if(strtolower($book)=="random") {
		$rand=random_passage($chapter,$version_array[0]);
		$book=$rand['book'];
		$chapter=$rand['chapter'];
		$verse=$rand['verse'];
	}

		//echo("book $book<br>");
		//echo("$version|".count($version_array)."<br>");
		/*
		$i=0;
		while($bb=$version_array[$i]){
		echo("$bb<br>");
		$i++;
		}
		*/

	//echo($language);

	// check for content
	// if book and search are empty, the form is blank
	if (empty($book)&&(empty($search))){
	echo"You didn't enter any thing. &nbsp;  Please enter your lookup and/or search before submitting the form.<br><br>";
	return null;
	}// end if

	// if search is just a common word, return null
	if (commonWord($search)){
	error("<center>You can't search for the common word \"$search,\" as it will produce too many results</center>");
	return null;
	}// end if
	//echo("book |$book| chapter |$chapter| verse |$verse|<br>");

	// book range with no search.   This will overwelm your server, and therefore BSS returns null.
	if ((strpos($book,"-")!==false)&&(empty($search))&&($lang!="ru")){
		//error("Please note that while you can search ranges of books (ex. Matt-John) you cannot look up ranges of books at a time. &nbsp; This helps prevent overloading my server. &nbsp;  If you need to look up multiple books, enter them into the look up box individually (ex. Matt; Mark; Luke; John).");
		return null;
	}// end if

	//strip initial spaces
	$book=stripextraspaces($book);
	$book=trim($book);
	$book=str_replace(array(" - ","- "," -"),"-",$book);
	$chapter=trim($chapter);
	$verse=trim($verse);
	$search=trim($search);

	// if book starts with a hyphen, add "01" (Genesis) to beginning
	if($book{0}=="-"){$book="01".$book;}

	//if $verse starts with a hyphen, put a "1" as the first character
	if (substr($verse,0,1)=="-"){$verse="1".$verse;}

	// SPECIAL CASES

	// #1 Multiple chapter, verse references
	// Example: Rom 2:8, 4:10, 5:8-10, 6:23, 8:2,5-7,9:5,10-11:5, 12:2-15:8,9,13-15
	// Is there a colon between comma n and comma n+1?
	$com1=strposneg($verse,',');
	if ($com1!=-1){$col=strposneg($verse,':',$com1+1);}
	else{$col=-1;}
	//echo("com1: |$com1| col: |$col|<br>");
	// If there sre multiple chapter and verse references for the same book
	if (($com1!=-1)&&($col!=-1)){

	$reff=$chapter.":".$verse;
	//echo("reff init:|$reff|<br>");

	//echo("numver".$num_ver);

	// set all "distinguished commas" to "~"
	// so the reference can be easily splitten into separate parts

	$max=strlen($reff)-1;
	$com2=-2;
	$col=0;
	$com1=strposneg($reff,',');

	//echo("reff:|$reff|<br>");
	// search for "distinguished commas"/
	while($col<$max) {
		if ($com2!=-2){$com1=$com2;}
		$col=strposneg($reff,':',$com1+1);
		$com2=strposneg($reff,',',$com1+1);
		//echo("com1|$com1| col|$col| com2|$com2|<br>");// diagnostic

		// replace comma at pos $com1
		if (($col!=-1)&&(($col<$com2)||($com2==-1))){
		//echo("dist at $col<br>");//diagnosticS
		// this allows for functionality in php3, which does not have substr_replace
		$reff=substr($reff, 0,$com1)."~".substr($reff,$com1+1);
		}
		//echo("<br>reff modified:|$reff|<br>");// diagnostic

		if(($com2>$max)||($col==-1)||($com2==-1)){$col = $max+5;}
	}// end while

	//echo("reff:|$reff|<br>");
	// separate the references and look them up recursively
	$reflist=explode("~",$reff);
	$in=0;

	$ver=array();

	while($chapver=$reflist[$in]) {
		$ref=$book." ".$chapver;
		//echo("ref|$ref|<br>");//diagnostic
		$ref=explodeRef($ref);
		$ver[]=getVerses($ref['book'], $ref['chapter'], $ref['verse'], $search, $searchtype, $version);
		$in++;
	}// end while

//print_out($ver);
 
$count=count($ver);

//echo($count);

$verses=array();

$v=0;
while($v<$num_ver){

$i=0;
$verss=array();
while($i<$count){
$vers=$ver[$i][$v];

//echo("vers");
//print_out($vers);
//print_out($verss);
error_reporting(E_ERROR);
$verss=array_merge($verss, $vers);
$i+=1;
}
$verses[]=$verss;
$v++;
}
error_reporting(E_ERROR|E_PARSE|E_WARNING);

//print_out($verses);
return $verses;
}// end if

// #2: cross-chapter references
$vhyp=strpos($verse,"-");
$vcol=strpos($verse,":");
$chyp=strpos($chapter,"-");


// special case cross chapter ex: Gen 2-3:10 (Genesis ch 2 through ch 3, verse 10)
// Requests such as Gen 2:3-10 will be interpreted as Geneses 2, verses 3-10
// Gen 2:10-3 will generate an error message, for now
$crossspecial=(($chyp!=0)&&(!empty($verse)));
// implement cross chapter references
if((($vhyp!=0)&&($vcol!=0))||($crossspecial)){

if ($crossspecial){
$c1=substr($chapter,0,$chyp);
$v1=1;
$c2=substr($chapter,$chyp+1);
$v2=$verse;
}// end if
else{
$c1=$chapter;
$v1=substr($verse,0,$vhyp);
$c2=substr($verse,$vhyp+1,$vcol-$vhyp-1);
$v2=substr($verse,$vcol+1);
}// end else

//echo("c1: |$c1| v1: |$v1| c2: |$c2| v2: |$v2| <br>");// diagnostic

return crossChapterLookup($book, $c1, $v1, $c2, $v2, $search, $searchtype, $version);
}// end if (cross chapter lookup)



// get booknumber

$bookfile=getBook($book,$lang);
$booknum=$bookfile["number"];

// Start building mySQL query that will be used to lookup the verses
$query=" WHERE";//SELECT * FROM `$table_prefix$version` WHERE";

// add book, chapter, and verse information to the query

// book
if (!empty($book)){$query.=makeQuery($book, "book");
// chapter
if (!empty($chapter)){
$query.=makeQuery($chapter, "chapter");
// verse
if(!empty($verse)){
$qu=makeQuery($verse, "verse");
if (strposneg($qu,"error")!="-1"){
//$qu=explode(" ",$qu);
echo ("verse error, lookup line 379");
return getVerses($book,$chapter,"$verse:");
}//
else {$query.=$qu;}

}//end verse
}// end chapter
if (!empty($search)){
$query.=" AND ";}
}// end book

// SEARCHING 
// build query for searches
// MAKE THESE INTO SEPARATE FUNCTIONS SO ADVANCED SEARCH IMPLEMENTATION WILL BE EASIER!!!
// functions: and, or, phrase, advanced

if (!empty($search)){

// ANY WORD OR ALL WORDS
if (($searchtype[0]=="Any Word")||($searchtype[0]=="All Words")||($searchtype[0]=="A")){

// AND or OR
if ($searchtype[0]=="Any Word"){$delm=" OR ";}
else{$delm=" AND ";}

// break the search string into individual words
$list=explode(" ",$search);

// initiate this part of the query
// whole or partial words?
if (($searchtype[1]=="wholeword")||($searchtype[1]==1)){

$query.="(".wholeWordQuery($list[0]);

}// end if
else{
$query.="(`text` LIKE '%".sanitize($list[0],true)."%'";
}// end else
// end init

$in=1;

// build the rest of this part of the query
while($list[$in]!=null){

if ($searchtype[1]=="wholeword"){$query.="$delm".wholeWordQuery($list[$in]);}
else{
$query.="$delm `text` LIKE '%".mysan($list[$in])."%'";
}// end if

$in+=1;
}// end while


// finalize this part of the query
$query.=")";


}// end if (any word or all words)

// EXACT PHRASE
else if($searchtype[0]=="Exact Phrase"){

$search = mysan($search);

//$query.=advancedQuery($search);
$query.=" `text` REGEXP '$search'";

}// end EXACT PHRASE
else{
// ADVANCED SEARCH

if (strpos($search,"PROX")!==false){
//global $lookup;

$lookup=referenceimplode(array($book,$chapter,$verse));

//$lk="$book $chapter:$verse";
//if($lk!=" :"){$lookup=$lk;}
//else{$lookup="";}
return proxboolean($lookup,$search);
}

$as=advancedSearch($search);
if ($as===null){return null;}
$query.=$as;
//
}// end advanced search

}//end if

// finalize the query
$query.=" ORDER BY `book`,`chapter`,`verse`;";

// display the query text if diagnostic mode is enabled.
if($show_query){echo("<br>$query<br><br>");}
// run the queries
$res=array();

if($parallel_search){$collumns="`book`,`chapter`,`verse`";}
else{$collumns="*";}

$mo=0;
while($ver=$version_array[$mo]){

$q2="select $collumns from `".mysan($table_prefix.$ver,true)."` $query";
//echo("Query: $q2<br>");

if($ver!=null){$res[]=mysql_query($q2);}

if($show_query){
echo(mysql_error());
}

$mo++;
}// end while

if ($res==null){return null;}

// create an array in which to put the verses

$verses_parallel=array();

// 
$mo=0;
while($ver=$version_array[$mo]){
$cn=0;
$verses=array();// temporary array

// pick each verse out of the query result and store in verses
while ($text = mysql_fetch_array($res[$mo])) { 
$verses[]=$text;
$cn+=1;
//echo("<span style='font-family:sans serif'>".$text["text"]."<br>");
//$vhigh=text;
if ($cn==$max_verses){break;}
}//end while

//if (count($verses)==0){$verses=null;}

//printout($verses);

//error_reporting(E_ALL);

if($parallel_search){$verses_parallel=array_merge($verses_parallel, $verses);}
else{$verses_parallel[]=$verses;}

$mo++;
}// end outer while


// check to make sure all verses requested are listed
// if not, set the error message, because some verses
// requested do not exist
// NOT IMPLEMENTED

//$vlow=$verses[0];
//if ($vlow["verse"]!=$verse){$lowerr=true;}

// return the array verses

//if ((count($version_array))==1){return $verses;}
//else{return $verses_parallel;}

//print_out($verses_parallel);

// code for parallel searches
// (searching more than one Bible at a time)

if($parallel_search){

//$verses_parallel=array_unique($verses_parallel);

$ref=array();

sort($verses_parallel);

foreach($verses_parallel as $item){

$ref[]=implodeRef($item);
//printout($item);

//getVerses($item["book"],$item["chapter"],$item["verse"],"","");

}

$ref=array_unique($ref);
//sort($ref);
$ref=implode(";",$ref);

//echo("$ref<br>");

return getSearch($ref);

}// end parallel search

//printout($verses_parallel);

return $verses_parallel;

}//end getVerses



// explodeRef
// seperates a Bible passage reference into book, chapter, and verse.
// @ param ref - the passage
// @ return - exRef - associative array containing the book, chapter, verse

function explodeRef($ref){

global $lang, $default_language;

//$lang=$language;

//echo("Reference: $ref<br>");// diagnostic code

$ref=stripextraspaces($ref);
$ref=str_replace(array(" - ","- "," -"),"-",$ref);



$startpos=0;
while(substr($ref, $startpos, 1)==" "){$startpos+=1;}
$ref=substr($ref, $startpos);
// find the break between book and chapter
// Since the shortest a reference will be is 2 letters (Ps) and some have a space (1 Tim)
// we start looking for the space at index =2
$break1=strpos($ref," ",2);

if(($break1!==false)){

$subbr=$break1;//strpos($ref," ",$break1+1);
$char=substr($ref,$subbr+1,1);
$sub=true;
while(((!ctype_digit($char)))&&($sub!==false)&&($char!="")){

//$br=$subbr;

$sub=strpos($ref," ",$subbr+1);
$subbr=$sub;
$char=substr($ref,$subbr+1,1);

$br=$sub;
//echo((substr($ref,$subbr-1,1)." "));

}// end while

if ($br>$break1){$break1=$br;}
if ($br===false){$break1=0;}
//echo("char $char ");

}// end if

if(strtolower(substr($ref,0,6))=="random"){$break1=6;}

//echo("break1 $break1 ");

// Special case: allow for any abbreviation of "Song of Solomon"
// This is done because SoS fullname references do not work with above command
// and because the code allows for any abbreviation of all other books
if (substr($ref, 0, 6)=="Song o"){$break1=strpos($ref," ",6);}
if (substr($ref, 0, 9)=="Song of S"){$break1=strpos($ref," ",9);}

//$break1=26;

// find the value for "book"
$book=substr($ref,0,$break1);
if ($break1==0){$book=$ref;}

// Special case #2: Book ranges with spaces.
$notbook=trim(substr($ref,$break1));
$blast=substr($b1,strlen($b1)-1,1);
//echo("book |$book| notbook |$notbook|");

$book=trim(stripextraspaces($book));




/*

// this is old code for book ranges with spaces

if ((substr($notbook,0,1)=="-")||($blast=="-")){

$reflen=strlen($ref);
$hyph=strpos($ref,"-");
// find the next text
$in=$hyph+1;
while(substr($ref,$in,1)==" "){$in++;}

$br1=strposneg($ref," ",$in);

if($br1!=-1){
$b2=substr($ref,$in,$br1-$in);
$notbook=substr($ref,$br1+1);
}// end if
else {

$b2=substr($ref,$in);
echo($b2." | ");
$notbook="";
}// end else

if ($blast=="-"){$book=substr($book,0,strlen($book)-1);}
//echo("b1 |$book| b2 |$b2|<br>");
$ref=$book."-".$b2;
//echo("ref |$ref|<br>");
$break1=0;

// redetermine value of "book"
if ($break1!=0){$book=substr($ref,0,$break1);}
else {$book=$ref;}
}// end long if
*/

// find the break beween chapter and verse
$break2=strpos($ref,":",$break1);

// if the reference includes more than just the book
// if the book reference is a range, ignore chapter and verse
if(($break1!=0)&&((strposneg($book,"-")==-1)||($lang=="ru"))){
// if a book NUMBER is specified, and is less than 
// 10, add a leading zero
// CURRENTLY DOES NOT WORK
//echo("$book space<BR>"); // Diagnostic for below code
//if(("$book".""=="1")){
//$book="0$book";
//}// end if

// find the value for "chapter"
if($break2===false){$chapter=substr($ref,$break1+1);}
else{$chapter=substr($ref,$break1+1,$break2-($break1+1));}

// find the value for verse
if($break2!=0){$verse=substr($ref, $break2+1);//$break1=strpos($ref," ",2);
}

}// end if 
else{
// if only a book reference

$chapter=null;
$verse=null;
}// end else

//echo ("book |$book| chapter |$chapter| verse |$verse|<br>");

// store values in array and return
$exRef=array(book=>"$book", chapter=>"$chapter", verse=>"$verse", style=>$case);
return $exRef;
}// end explodeRef

function implodeRef($ref){

//printout($ref);

$book=getbook($ref["book"]);

$imref=$book["fullname"];//$ref["book"];

//if(strlen($ref["book"])==1){$imref="0".$imref;}

if($ref["chapter"]!=""){$imref.=" ".$ref["chapter"];
if($ref["verse"]!=""){$imref.=":".$ref["verse"];}
}

//echo("ref $imref<br>");

return $imref;

}// end imploderef

function random_lookup($lookup){

global $default_search_bible_version;

$look=explode("'",$lookup);

//$i=0;
$stop=count($look);

for($i=0;$i!=$stop;$i++){

$item=trim(strtolower($look[$i]));

$split=explode(" ",$item);

if($split[0]=="random"){

$ref=random_passage($split[1],$default_search_bible_version);

$look[$i]=implodeRef($ref);
}



}

$lookup=implode("'",$look);

return $lookup;

}

// random_passage
// returns random verse or chapter

function random_passage($chap,$version){

$version = sanitize($version,true);

if (strtolower($chap)=="verse"){

$res=mysql_query("select * from `bible_$version` ORDER BY `index` DESC LIMIT 1");

$arr=mysql_fetch_array($res);
$num=$arr["index"];

//echo($num);

$index=mt_rand(0,$num);
//echo($index);

$res=mysql_query("select * from `bible_$version` where (`index` = '$index')");
//echo("select * from `bible_$version` where (`index` = '$index')".mysql_error());
$array=mysql_fetch_array($res);

$book=$array['book'];
$chapter=$array['chapter'];
$verse=$array['verse'];
}

if (strtolower($chap)=="chapter"){

$res=mysql_query("select * from `bible_$version` where (`verse`='1')");
$arr=array();

while($arr[]=mysql_fetch_array($res)){}


$num=count($arr);
//echo("num $num");

$in=mt_rand(0,$num);

$book=$arr[$in]['book'];
$chapter=$arr[$in]['chapter'];
$verse='';

}

$exRef=array(book=>"$book", chapter=>"$chapter", verse=>"$verse");

return($exRef);

}// end random_passage

// getBook
// returns an array from the table bible_books with all the information for a particular book
// @ param - book - either the full name, short name, or number of the book
// @ param - lang - 2 character language
// @ return - array containing all content in row for this book from bible_books

function getBook($book, $langu=""){

global $lang, $default_language, $table_prefix;


if($langu==""){$langu=$lang;}

$in=$book;
//echo($default_language);
//echo("<br>lang1 $lang");
if ($langu==""){$langu=$default_language;}



//echo($lang);
// if no book
if ($book==null){return null;}

// special cases
// This ensures that users get what they expected
$book=gbSpecialCase($book, "Ro", "Rom");
$book=gbSpecialCase($book, "Jud", "Jude");
$book=gbSpecialCase($book, "Am", "Amos");

// strip out period
$per=strposneg($book,".");
if ($per==strlen($book)-1){$book=substr($book,0,$per);}

$book = sanitize($book,true);
$langu = sanitize($langu,true);

//echo("langu|$langu");

// if fullname or short name
if(($book==0)||(strlen($book)>2)){

$q1="SELECT * FROM `bible_books_$langu` WHERE ((`short` LIKE '% $book%') OR (`short` LIKE '$book%') OR (`fullname` = '$book%') OR (`fullname` LIKE '$book') );";
}// end if
// if book number
else{


$q1="SELECT * FROM `bible_books_$langu` WHERE (`number` = '$book');";
//echo($langu);
//}
//if ($diag){
//echo("|$q1|<br>");
}//end else
error_reporting(E_ERROR | E_PARSE);

$rs=mysql_query($q1);
$array = mysql_fetch_array($rs);

// if no book found using above methods, try searching fullname using REGEXP
// this was omitted from query above because it tended to return the wrong book
// when multiple books matched the query, as only the first one is returned.
if ($array==null){
	$rs=mysql_query("SELECT * FROM `bible_books_$langu` WHERE (`fullname` REGEXP '$book');");
	$array = mysql_fetch_array($rs);
}// end if

// maybe book name is not in selected language

if (($array==null)&&($langu!=$default_language)){

// get book from the default language book list
$array=getBook($in, $default_language);

// does this book list exist?
$res=mysql_query("select * from `bible_books_$langu` LIMIT 1");
//echo(mysql_error());

// if book list for langu exists, use it
if (mysql_fetch_array($res)!=null){
$array=getBook($array["number"],$langu);

}

//error_reporting(E_ERROR | E_WARNING| E_PARSE);
//if($array2!=null){$array=$array2;}
}

return $array;

}// end getBook

// gbSpecialCase
// implements case-insensitive special cases for getBook
// @ param book - book name
// @ param case - string to compare with book
// @ param set - string to set book to if it matches case
// @ return book - book name (may be modified)

function gbSpecialCase($book, $case, $set){

$bo=strtolower($book);
$BO=strtoupper($book);
$Bo=ucfirst($bo);
$B_O=ucwords($bo);

if (($book==$case)||($bo==$case)||($BO==$case)||($Bo==$case)||($B_O==$case)){
return $set;}
else {return $book;}

}// end gbSpecialCase

// makeQuery
// Builds the lookup query for book, chapter or verse references
// This preforms this action one at a time (ie, one of the above)
// @ param findme - The book, chapter or verse reference
// @ param type - Indicates whether findme is a book, chapter or verse reference

function makeQuery($findme, $type, $table_alias = '') {

	$verse=explode(",",$findme);
	if ($type!="book") {$query =" AND (";}
	else {$query = " (";}

	$i=0;
	
	$table_alias = (!empty($table_alias)) ? "`$table_alias`." : '';
	
	// process each verse or verse range
	while($verse[$i]!=null) {
		error_reporting(E_ERROR);
		$br=strpos($verse[$i],"-");
		if($type=="book"){$br=strpos($verse[$i],"-",2);}
		//error_reporting(E_ERROR | E_WARNING| E_PARSE);
		if ($i>0){$query.=" OR ";}
		// verse range lookup

		// determine if this is a single verse or verse range??
		if($br>0){
		//echo($verse."?<br>");
		$vers=$verse[$i]." ";

		$lo=substr($vers,0,$br);

		$hi=substr($vers,$br+1);
		//if (($lo+0)>($hi+0)){//echo "low hi";
		//return "error $lo $hi";
		//}
		//echo("Low $lo High |$hi|<br>");

		if($type=="book"){
		//print"Low [$lo] High [$hi]<br>";

		$lo=bookNum($lo);
		$sp=strpos($hi," ",2);
		$hi=substr($hi,0,$sp);
		//echo("book $hi<br>");
		$hi=bookNum($hi);

		}// end if
		if (($hi==" ")||($hi==null)){$query.="($table_alias`$type`>=$lo)";}
		else{$query.="($table_alias`$type`>=$lo AND $table_alias`$type`<=$hi)";}

		}//end if
		else{
		$ver=$verse[$i];
		if ($type=="book"){$ver=bookNum($ver);}

		$query.="$table_alias`$type`='".$ver."'";
		}//end else
		// end verse range
		$i+=1;
	}// end while (end process verse)
	$query.=")";

	return $query;

}// end makeQuery

// wholeWordQuery
// builds the mySQL query for "whole word only" searches
// @ param word - the word to be searched for
// @ return qu - the (partial) query for a whole word

function wholeWordQuery($word){

if ((strpos($term, " ")===false)&&(strpos($term, "<sp>")===false)&&(strpos($term, "\"")===false)){$op="LIKE";}
else{$op="REGEXP";}

$word = sanitize($word,true);

$s=array("$word", "$word.", "$word,", "$word;" , "$word!", "$word:", "$word?", "$word\\'", "$word\"", "\"$word ","\\'$word ","\\'$word\\'","“$word ","$word”");

//print_out($s);

$i=1;
$qu="((`text` $op '% $word %')";
while(!empty($s[$i])){

$qu.=" OR (`text` $op '% ".$s[$i]."%')";
$qu.=" OR (`text` $op '".$s[$i]."%')";
$i+=1;
}

$qu.="OR (`text` $op '".""."$word %'))";
//$qu.="";

//echo("<pre>$qu</pre>");

return $qu;
}// end wholeWordQuery

// bookNum
// returns the book number of the given book
// @ param - book - the book fullname, shortname (or even number) to get the number of
// @ returns - the number
function bookNum($book,$lang=""){

$lobook=getBook($book,$lang);
return $lobook["number"];

}// end bookNum

// shortcut
// Replaces a predefined search limit (shortcut) with the predefined reference
// Ex: "OT" is replaced with "Genesis-Malachi"
// @param ref - the reference list to check
// @return ref - the reference or if the reference was a predefined search, the 
// reference defined by the definintion

function shortcut($ref){

$refe=explode(";",$ref);
$in=0;
while($re=$refe[$in]){
$re=sanitize($re,true);

$qu="SELECT * FROM `bible_shortcuts_en` WHERE ((`name` = '$re') OR (`short1` = '$re') OR (`short2` = '$re') OR (`short3` = '$re'));"; 

$res=mysql_query($qu);
if($sm=mysql_fetch_array($res)){
///echo($sm["reference"]);
$refe[$in]= $sm["reference"];}

$refe[$in]=trim($refe[$in],";");
$in++;
}//end while

$ref=trim(implode("; ", $refe),";");
//echo("|$ref|");


return $ref;


}// end shortcut

// commonWord
// indicates if a given word is a commonly, frequently used word
// checks word against array of words
// @ param - word - the word to be checked
// @ returns - true if a common word, false otherwise.
function commonWord($word){

global $version_array;
if(count($version_array)==1){return false;}
// this function currently disabled
//return false;


$word=str_replace(array("%",")","(","{","}","[","]"),"",$word);

$common = array("and","or","the","a","an","it","he","her","they","there","of","his","hers",
"their", "them","to","she","at");

$i=0;
while($common[$i]!=null){
if($word==$common[$i]){return true;}
$i+=1;
}
return false;
}// end commonWord


// crossChapterLookup
// assists getVerses in looking up references across multiple chapters
// but can also be used independantly
// @ param - book - name or number of the book
// @ param - c1 - starting chapter
// @ param - v1 - starting verse
// @ param - c2 - ending chapter
// @ param - v2 - ending verse
// @ param search - string of words to search for
// @ param searchtype - specifies all words, any word, exact phrase, or advanced search
// @ param version - Bible Version
// @ returns verses - array of verses

function crossChapterLookup($book, $c1, $v1, $c2, $v2, $search, $searchtype, $version){


//echo("I am here");

// if the the first chapter is greater than the second chapter,
// there is a user error
if ($c1>$c2){
error("Your second chapter is greater than your first!");
return null;}

// if both chapters happen to be the same
// ex: Matt 26:5-26:41
if ($c1==$c2){return $verses=getVerses($book, $c1, $v1."-".$v2, $search, $searchtype, $version);}
$vers=array();

// special case #2  
// Ex Gen 1:5-2:-
// Interpreted as Genesis 1 verse five through (all of) chapter 2
// For looking up cross chapter passages where the entire final chapter
// is desired and the number of verses in the chapter is unknown.
// This single line of code is all that is required.

if ($v2=="-"){$v2="";}


$ref="$book $c1:$v1-; ";

for($i=$c1+1;$i<$c2;$i++){

$ref.="$book $i; ";

}

$ref.="$book $c2:1-$v2";

//echo($ref);

//return getSearch("$ref",$search, $version,$searchtype);


$vers[]=getVerses($book, $c1, $v1."-", $search, $searchtype, $version);
//$bnum=booknum($book);
$chap=$c1+1;
while($chap<$c2){
$vers[]=getVerses($book, $chap, "", $search, $searchtype, $version);
$chap+=1;
}


/*
// merge the resultant arrays
$i=0;
while($ver=$vers[$i]){
//$verses=array_merge($verses, $ver);
$i+=1;
}
*/



$vers[]=getVerses($book, $c2, "1-$v2", $search, $searchtype, $version);

/*
echo("<pre>");
print_r($vers);
echo("</pre>");
*/

$chap=$c1;
$num_bib=count(explode(",",$version));

$verses=array();
//echo("num_bib $num_bib");



//if($num_bib==1){return $vers;}

/*
for($bib_num=0;$bib_num<$num_bib;$bib_num++){
$bib=array();
for($chap=0;$chap<=$c2-$c1;$chap++){

//echo("chap:$chap num:$bib_num<br>");
//$num=$chap+$bib_num-$c1;
if($num_bib==1){
$bib=array_merge($bib,$vers[$chap]);
}
else{
$bib=array_merge($bib,$vers[$chap][$bib_num]);
}

}// end for

if($num_bib==1){return $bib;}

$verses[]=$bib;
}// end for
*/

//echo("vers coming<br>");
//print_out($vers);

$vnum=0;
while($n=$vers[0][$vnum]){
$thar=array();
foreach($vers as $list){

$thar=array_merge($thar, $list[$vnum]);

}// end foreach

$verses[]=$thar;
$vnum++;
}// end while

//echo("vnum|$vnum");

//$ch=count($verses[3]);
//echo("ch $ch|<br>");
//print_r($verses[0][0]["text"]);

/*
echo("<pre>");
print_r($verses);
echo("</pre>");
*/

//echo("verses coming<br>");
//print_out($verses);

return $verses;
}// end crossChapterLookup

// getChapters and getNumChapters
// finds the number of chapters in a particular book
// @ param - book - The book reference
// @ returns - The number of chapters in this book

function getNumChapters($book){return getChapters($book);}
function getChapters($book,$lang="") {

global $language, $version;

$bok = bookNum($book,$lang);

$ver=explode(",",$version);
$vers=$ver[0];

$vers = sanitize($vers,true);
$book = mysan($book);

$qu="select * from `bible_$vers` where (`book` = '$book') ORDER BY `index` DESC LIMIT 1;";
//echo("$qu<br>");
$res=mysql_query($qu);
$ve=mysql_fetch_array($res);

//echo($ve["im

return $ve["chapter"];

}// end getChapters

// countChapters
// counts the number of chapters uploaded in a particular book
// @ param - book - The book reference
// @ param - version - Bible Version
// @ returns - The number of chapters found in this book
function countChapters($book, $version=""){
global $bss_default_version;
if ($version==""){$version=$bss_default_version;}

}// end countChapters

// getNumVerses
// finds the number of verses in a particular chapter
// @ param - book - The book reference
// @ param - chapter - The chapter reference
// @ param - version - Bible version to use (Bibles before KJV numbered their verses differently)
// @ returns - The number of verses in this chapter

function getNumVerses($book, $chapter, $versi=""){

global $version, $table_prefix;

if (empty($versi)){$versi=$version;}

$versi = sanitize($versi,true);
$table_prefix = sanitize($table_prefix,true);
$chapter = sanitize($chapter,true);

//if ((strposneg(",",$versi))!=-1){
$versi=explode(",",$versi);
$versi=$versi[0];
//}



// GET BETTER WAY TO DO THIS!!!!
// Doesn't work for Luther Bible!!!

$bnum = bookNum($book);
$query = "Select * from `$table_prefix$versi` WHERE (`book` = '$bnum') AND (`chapter` = '$chapter') ORDER BY `verse` DESC LIMIT 1;";
$res = mysql_query($query);
$verse = mysql_fetch_array($res);
return $verse['verse'];

}// end getNumVerses

// strposneg
// gives the position of the first occurence of $search in $string
// unlike strpos, returns -1 if not found
// @ param $string - string to search
// @ param $search - string to find
// @ param $offset - string offset
// @ return $pos - position of $search, -1 if not found
function strposneg($string, $search, $offset=0){



if (strlen($string)<($offset)){return -1;}

$pos=strpos($string, $search, $offset);
$len=strlen($search);
if (substr($string,0,$len)==$search){return 0;}
if ($pos>0){return $pos;}
else {return -1;}

}// end strposneg

// phpcompare
// compares two versions of php
// yes, i know that php has this function built in,
// but only in newer versions.
// Note: this strips the version to the first five characters
// @param version1 version of php in "m.n.o" format
// @param version2 version of php in "m.n.o" format
// @return 0 if both are equal, -1 if version1 < version2, 
//	and +1 if version1 > version2
function phpcompare($version1, $version2){

$version1=substr($version1,0,5);
$version2=substr($version2,0,5);

$v1=explode(".",$version1);
$v2=explode(".",$version2);

if ($version1==$version2){return 0;}
if ($v1[0]>$v2[0]){return 1;}
if ($v1[0]<$v2[0]){return -1;}
if ($v1[1]>$v2[1]){return 1;}
if ($v1[1]<$v2[1]){return -1;}
if ($v1[2]>$v2[2]){return 1;}
if ($v1[2]<$v2[2]){return -1;}

}// end phpcompare

// splitSearch
// @param $sea search query
// @return $search_array array of search terms
function splitSearch($sea="", $keep_underscores=false, $mysql_escape = false){

global $search;
if ($sea==""){$sea==$search;}

$sea=stripextraspaces($sea);
$sea=trim($sea);

//echo("|$sea|<br>");
$first_quo=array();
$last_quo=array();
$exact=array();

$fi=-1;
$la=-1;
$i=true;

// find the positions of the quotations
while($i==true){

$fi=strpos($sea, "\"", $la+1);
if ($fi!==false){
$la=strpos($sea, "\"", $fi+1);
if ($la===false){break;}
//$first_quo[]=$fi;
//$last_quo[]=$la;

$exact[]=trim(substr($sea,$fi, $la-$fi+1));//($sea,$fi, $la-$fi+2);

}// end if

else{$i=false;}

//echo("exact");
//print_out($exact);

}// end while
$exact_sv=array();

foreach ($exact as $mm){
//echo("$mm<br>");
$mm=trim($mm);
$mm=str_replace(array(")","("),"",$mm);
$exact_sv[]=advTransform($mm);
//echo("tx: <xmp>".advTransform($mm)."</xmp><br>");
//echo("!$mm!<br>");
}

$sea=str_replace($exact, $exact_sv, $sea);
//echo("|$sea|<br>");
$find=array("(",")", "||","&&","!!","&","*","+","|","-","NOT", "AND","OR","!=","XOR","^","^^");
$sea=str_replace($find, " ",$sea);
$sea=stripExtraSpaces($sea);
//echo("|$sea|<br>");

$array=explode(" ",trim($sea));
//print_out($array);

if (!$keep_underscores){
$array=invAdvTransform($array);
}// end if

//foreach($array as $i){
//echo("|$i|<br>");
//advancedQuery($i);
//}

// remove repeated words
$i=0;
while($i<count($array)){

$value=$array[$i];
$j=$i+1;
while($j<count($array)){

if ($array[$j]==$value){unset($array[$j]);}
$j++;
}// end while

$i++;
}// end while (repeated words)

//$array=trim($array);

//print_out($array);

if($mysql_escape){foreach($array as &$arr){$arr = mysan($arr,true);}}

return $array; 
}// end splitSearch

// advancedSearch
// produces query for advanced searches

function advancedSearch($search){

//echo("I am here");

$search=str_replace("\\","",$search);

//echo($search);

$terms_u=splitSearch($search, true);
$terms=splitSearch($search);

/*
echo("<xmp>");
print_out($terms);
print_out($terms_u);
echo("</xmp>");
*/

$search=str_replace($terms, $terms_u, $search);

$rep=array();
$i=0;
$stop=count($terms);
while ($i<$stop){

$rep[]=" ".advancedQuery($terms[$i])." ";
$terms[$i]=" ".$terms[$i]." ";
$terms_u[$i]=" ".$terms_u[$i]." ";
//echo("|$terms[$i]|<br>");
$i++;
}// end while

//print_r($rep);

// are parentenses balanced?
if(!checkPar($search,"(",")")){return null;}
if(!checkPar($search,"{","}")){return null;}
if(!checkPar($search,"[","]")){return null;}
if(!checkPar($search,"<",">")){return null;}

//foreach($terms as $t){echo "$t<br>";}

$search=stripExtraSpaces($search);

// prep the search string before converting it to query
$ad_ser=array("(",")","{","}","[","]");
$ad_rep=array(" ( ", " ) "," ( ", " ) "," ( ", " ) ");
$search=str_replace($ad_ser, $ad_rep, $search);

//echo($search);

$search=" $search ";
// operator aliases
$and=array("AND", "*","&&","&");
$oR=array("OR","+","||","|");
$NOT=array("NOT", "-","!=","-");
$XOR=array("XOR","^^","^");

$search=str_replace($XOR," ^ ",$search);
$search=str_replace($and, " & ",$search);
$search=str_replace($oR, " | ",$search);
$search=str_replace($NOT, " - ",$search);


// if only NOT searches, return null

// count number of NOTs and ORs
$cneg=count(explode("-",$search))-1;
$cor=count(explode("|",$search))-1;
$ct=count($terms);
$ss=str_replace("| ","+",$search);
//echo("|cneg:$cneg|cor:$cor|ct:$ct|$search|".strpos($search,"| -")."|");

/*
if ((($ct==$cor+1)&&($cneg!=0))||($cneg==$ct)||(strpos($search,"| -")!==false)){
echo("You can't do just NOT searches, it will return the entire Bible!<br><BR>");
//return null;
}
*/

$search=advancedExamine($search);

$query=str_replace($terms_u, $rep, $search);
$query=stripExtraSpaces($query);

$ser=array("&","|","-","^");
$rer=array("AND","OR"," NOT ","XOR");
$query=str_replace($ser,$rer, $query);

$query=str_replace("\\\"","",$query);
//$query=str_replace("_"," ",$query);
//$query=str_replace("^","_",$query);
//echo ("query $query<br>");


return "($query)";
}// end advancedSearch

// advancedQuery
// creates advanced search query for single term
// @param $term - term to create query for
// @return $query - query for the term
function advancedQuery($term){

$term = mysql_real_escape_string($term);

$op="REGEXP";
if (( (strpos($term, " ")===false) && (strpos($term, "<sp>")===false) && (strpos($term, "\"")===false) )){//||((strpos($term, "<sp>")===false) && (strpos($term, "_")===false))){]
$op="LIKE";
$wc="%";
}
else{

$op="REGEXP";
$term=str_replace("\"","",$term);
$term=trim($term);
$wc="";
}


//echo($op);
//$op="LIKE";

global $wholeword;

if ((($wholeword=="Whole words only.")||($wholeword=="true"))&&($op!="REGEXP")){
//$query=wholeWordQuery($term);
//echo("$query<br>");


return wholeWordQuery($term);}

$query="(`text` $op '$wc$term$wc')";
//echo("$query<br>");
return $query;

}// end advancedQuery

function advTransform($query,$inv=false){

$tx=array(")","(", " ",  "AND", "OR", "NOT","XOR","{","}","[","]","&","!","-","|","*","+","^","PROX","NEAR","'","_");
$rx=array("<rp>","<lp>","<sp>","<and>","<or>","<not>","<xor>","<lc>", "<rc>","<lb>","<rb>","<amp>","<ex>","<neg>","<ver>", "<sta>","<pl>","<hat>","<prx>","<ner>","<sq>","<usc>");

// Debugging code
//$txc=count($tx);
//$rxc=count($rx);

//if($txc!=$rxc){echo("Transform error: txc:$txc rxc:$rxc");}

if(!$inv){$que=str_replace($tx,$rx,$query);}

else{$que=str_replace($rx,$tx,$query);}

//echo("<xmp>$que</xmp><br><BR>");

return $que;

}// end advTransform

function invAdvTransform($query){return advTransform($query,true);}



function advancedExamine($search){

//echo("|$search|<br>");

//$search=stripextraspaces($search);

//echo("||$search||<br>");
$i=1;

$search=stripextraspaces($search);
$len=strlen($search);
while($i<$len){

$pos=strpos($search, " ",$i);
/*
if($pos1===false){$pos1=10000000;}
$pos2=strpos($search, "(",$i);
if($pos2===false){$pos2=10000000;}
$pos3=strpos($search, ")",$i);
if($pos3===false){$pos3=10000000;}
*/
//$pos=$pos1;//min($pos1,$pos2,$pos3);

// IMPLIED AND

if ($pos>=($len-1)){break;}

$lo=substr($search,$pos-1,1);
$hi=substr($search,$pos+1,1);
$lo_type=advancedType($lo);
$hi_type=advancedType($hi);
//echo("search $search| lo:$lo || hi:$hi || type:$lo_type -- $hi_type |<BR>");

if ((($lo_type=="let") and ($hi=="(")) or (($hi_type=="let") and ($lo==")")) or (($lo_type=="let") and ($hi_type=="let")) or (($lo_type=="let")and($hi_type=="not")) or (($lo==")")and($hi=="(")) or (($lo==")")and($hi_type=="not"))){

$find="$lo $hi";
$rep="$lo & $hi";
//echo("| $find || $rep || $lo || $hi || $lo_type -- $hi_type |<BR>");
$search=str_replace($find, $rep, $search);
$pos+=1;
if (($hi_type!="rpar")&&($hi_type!="lpar")&&($lo_type!="rpar")&&($lo_type!="lpar")){$pos+=2;}

}// end if

$i=$pos+1;
}// end while

return $search;
}// end advancedExamine

// proxand
// strict proximity search
// @ lookup - search limitation
// @ param - terms array of search terms
// @ param - range verse range
// @ return - verses

function proxand($lookup,$terms, $range){

return proximity2($lookup, $terms, $range);

//echo("proxand<br>");

global $default_search_bible_version;

if($lookup==""){$lookup="Genesis - Revelation";}


//echo("|range|$range");

//echo("Om here |$lookup|".$terms[0].$terms[1]."|$range|$default_search_bible_version<br>");

$res1=getSearch($lookup,$terms[0],$default_search_bible_version,array("Boolean Search","wholeword"));
$ref1=proxexplode($res1,$range);

//echo(count($res1)."||<br>");
//foreach($res1 as $ref){echo("|".$ref["text"]."<br>");}

//$i=1;

foreach($terms as $term){
//while($term=$terms[$i]){

$ref1=proxcheck($term, $ref1);

//$i++;
}// end while

$ref="";

foreach($ref1 as $r){$ref.=referenceimplode($r)."; ";}

$ref=substr($ref,0,strlen($ref)-2);

//echo("|$ref|<br>");

if(count($ref1)==0){return null;}

$res=getSearch($ref,implode(" OR ",$terms),"",array("Boolean Search","wholeword"));
//$res=getSearch(implode("; ",$ref1),implode(" OR ",$terms),"",array("Boolean Search","wholeword"));
$res=repeat_remove($res);

//print_out($res);
return $res;

}// end proxand

function referenceimplode($ref){

$book=getBook($ref[0]);

$r=$book["fullname"];

if($ref[1]!=""){$r.=" ".$ref[1];}
if($ref[2]!=""){$r.=":".$ref[2];}

return $r;

}//end referenceimplode

function proxboolean($lookup, $query){

global $default_proximity_range;
//echo("proxbool lookup $lookup<br>");

if($lookup==""){$lookup="OT; NT";}

$query=stripextraspaces($query);
$query=str_replace("PROX ","PROX()",$query);

$N=array();
$op=array();
$terms=array();

$i=0;
$len=strlen($query);

while($i<($len-5)){

$prox=strpos($query, "PROX(",$i);
if($prox===false){break;}

$rp=strpos($query,")",$prox);
//echo("i|$i|prox|$prox|rp|$rp|<br>");
$dif=$rp-$prox-5;

// find query errors
if(($dif==0)&&($dif>4)){return null;}

$nn=substr($query,$prox+5,$dif);

$op[]="PROX($nn)";

if($nn==""){$nn=$default_proximity_range;}

$N[]=$nn;

$i=$rp+1;
}// end while

$query=str_replace($op,"@",$query);
$query=stripextraspaces($query);
$terms=explode("@",$query);

$i=0;
while($term=trim($terms[$i])){
$terms[$i]=$term;
$i++;
}

$ct=count($terms);
$cn=count($N);

//print_out($terms);
//print_out($N);

if($ct!=($cn+1)){
echo("prime error ct|$ct|cn|$cn| query|$query|<br>");
return null;
}

return proximity2($lookup, $terms, $N);

// OLD CODE  
// REMOVE??

/*
if ($cn!=1){
echo("Sorry. You can only have one PROX() operator per query.<br><BR>");
return null;
}
*/

//foreach ($terms as $o){echo("$o<br>");}


/*
$n1=$N[0]+1;
$n=$N[0];
echo ("n $n n1 $n1");
*/



//$old_terms=array($terms[0],$terms[1]);

//$res=proxand($lookup,$old_terms,$N[0]);

/*
$i=1;
while($nn=$N[$i]){

$ref=proxexplode($res,$nn);
$res=proxand2($lookup,$terms[$i+1],$old_terms,$ref);

$old_terms[]=$terms[$i+1];
$i++;
}//while
*/

//return $res;

}// end proxboolen

// proxand2
// strict proximity search (multiple terms)
// @ lookup - search limitation
// @ param - terms array of search terms
// @ param - range verse range
// @ param - old_terms terms that have already been searched for
// @ param - old_ref reference from previous searches
// @ return - verses
function proxand2($lookup,$term, $range, $old_terms, $old_ref){

global $default_search_bible_version;


$res1=getSearch($lookup,$term,$default_search_bible_version,array("Boolean Search","wholeword"));
$ref1=proxexplode($res1,$range);

$res=getSearch(implode("; ",$ref1),implode(" OR ",$old_terms),"",array("Boolean Search","wholeword"));
$res=repeat_remove($res);

return $res;

}// end proxand2

// special get verses for proximity search

function getVersesQuick($book,$chapter, $lo,$hi, $search){

global $default_search_bible_version;

$book = mysan($book);
$chapter = mysan($chapter);
$lo = mysan($lo);
$hi = mysan($hi);
//$search = mysan($search);
$dsbv = mysan($default_search_bible_version);

//global $get_quick;
//$get_quick+=1;

//echo("getVersesQuick $get_quick<br>");

$query="select `book`,`chapter`,`verse` from `bible_$dsbv` where ((`book`='$book') AND (`chapter`='$chapter') AND ((`verse`>=$lo) AND (`verse`<=$hi)) AND ".advancedSearch($search).")";

//$br=strpos($verse,"-");
//if($br!==false){
//$lo=substr($verse,0,$br);
//$hi=substr($verse,$br+1);
//$query.="";
//}
//else{$query.="(`verse`='$verse')";}

//if($search!=""){$query.=" AND ".advancedSearch($search).")";}
//else{$query.=")";}
//$query.=")";

//echo("$query");

$res=array();
$rset=mysql_query($query);
//echo(mysql_error());
while($rar=mysql_fetch_array($rset)){
$res[]=$rar;

}
//echo($query."<br><BR>");

return $res;
}

function getVersesQuick2($ref, $search,$version,$wholeword){

$res=exploderef($ref);

return getVersesQuick($ref['book'],$ref['chapter'], $ref['verse'], $search,$version,$wholeword);

}

function proxexplode($res, $range){

//echo("proxexplode: $range<br>");

$ref=array();

//print_out($res);

$i=0;
while($ver=$res[0][$i]){

$ref[]=getContextarray($ver["book"],$ver["chapter"],$ver["verse"],$range);

$i++;
}// end while 

//print_out($ref);

return $ref;

}// proxexplode

// new, improved proximity search algorithm
// proximity2
function proximity2($look, $terms, $range) {
	
	if(in_array(200,$range) || $range == 200) {
		// stay here
	}
	else {
		//return proximity3($look, $terms, $range);
	}
	
	//echo('PROXIMITY2<BR>');

	global $default_search_bible_version, $lookup, $parallel_search;
	//echo('here o bob');
	if($parallel_search) {
		echo("<center>You cannot use verse proximity or chapter searches with multiple Bibles</center><br><br>");
		return null;
	}

	//echo("lookup $lookup<br>");

	//echo("Om here |$lookup|".$terms[0].$terms[1]."|$range|$default_search_bible_version<br>");

	//error_reporting(E_ALL);

	// use search exploder!!!
	//$terms=$search;//explode(" ",$search);

	//echo("|range|$range<br>");

	// check for parenthenses

	foreach($terms as $t) {
		if((!checkPar($t,"(",")",false))|(!checkPar($t,"(",")",false))|(!checkPar($t,"(",")",false))|(!checkPar($t,"(",")",false))) {
			echo("<center>The PROX(N) operator cannot be place inside parenthenses or brackets.</center><br><br>");
			return null;
		}// end if
	}// end foreach

	$new=proxorder($terms, $range);
	$range=$new["range"];
	$terms=$new["terms"];

	//print("lookup $look<br>");
	//print_out($terms);
	//print_out($range);

	if($look==""){$look=$lookup;}
	if($look==""){$look="Genesis - Revelation";}

	//echo("Om here |$lookup|".$terms[0].$terms[1]."|$range|$default_search_bible_version<br>");

	$res=getSearch($look,$terms[0],$default_search_bible_version,array("Boolean Search","wholeword"));

	//print_out($res);

	$res_ref="";

	if($res==null){return null;}

	$terms_new=array_slice($terms,1);
	//$range_new=array_slice($range,1);

	foreach ($res[0] as $v){

	$res_ref.=proxcheck2($v["book"],$v["chapter"],$v["verse"],$terms_new,$range);

	//print_out($v);

	}

	// remove repeat passages
	$ref_arr_old=explode(";",$res_ref);
	$ref_arr=array();

	foreach($ref_arr_old as $re) {
		if(array_search($re,$ref_arr)===false){$ref_arr[]=$re;}
	}

	$res_ref=implode(";",$ref_arr);
	$res_ref=str_replace(";;",";",$res_ref);

	if ($res_ref==""){return null;}

	$res_init=getSearch($res_ref,implode("|",$terms),$default_search_bible_version,array("Boolean Search","wholeword"));

	// sort the results by "index"
	$index=array();
	// Obtain a list of columns
	foreach ($res_init[0] as $key => $row) {
		$index[$key]  = $row['index'];
	}

	//print_out($res_init[0]);

	// Sort the data with volume descending, edition ascending
	// Add $data as the last parameter, to sort by the common key
	if(array_multisort($index, SORT_ASC, $res_init[0])){}

	//sort($res_init);
	
	
	//print_out($res_init);
	
	return $res_init;
}//

function proximity3($look,$terms,$range) {
	echo("look $look"); 
	print_r($terms);
	print_r($range);
	
	$single_range = (!is_array($range)) ? true : false;
	
	$sql_selects = array();
	$sql_join = array();

	$num = count($terms);
	
	$ref = exploderef($look);
	//print_r($ref);
	
	$lookup_query = '';
	// book
	if (!empty($ref['book'])) {
		$lookup_query.=makeQuery($ref['book'], "book",'b1');
		// chapterS
		if (!empty($ref['chapter'])) {
			$lookup_query.=makeQuery($ref['chapter'], "chapter",'b1');
			// verse
			if(!empty($ref['verse'])) { 
				$lookup_query = makeQuery($ref['verse'], "verse",'b1');
			}
		}
		$lookup_query = " AND ($lookup_query)";
	}

	for($i=1;$i<=$num;$i++) {
		$sql_selects[]= "b{$i}.index as b{$i}";
		
		$lookup_query_2 = str_replace("`b1`","`b{$i}`",$lookup_query);
		
		if($i != 1) {
			$key = $terms[$i - 1];
			$prox = ($single_range) ? $range : $range[$i - 2];
			$i_old = $i-1;
			
			$sql_join[]= "inner join bible_kjv as b{$i} on b{$i}.index >= b{$i_old}.index -{$prox} and b{$i}.index <= b{$i_old}.index +{$prox} and b{$i}.book = b{$i_old}.book and (b{$i}.text like '%$key%') $lookup_query_2";
		}
	}
	
	$sql_pre = "
	select ".implode(',',$sql_selects)."
	from bible_kjv b1
	".implode('
	',$sql_join)."
	where (b1.text like '%{$terms[0]}%') $lookup_query;
	";
	
	echo('<br><br><pre>'.$sql_pre.'</pre>');
	
	$res2 = mysql_query($sql_pre);
	echo(mysql_error());
	$indices = array();

	while($row = mysql_fetch_assoc($res2)) {
		foreach($row as $ind) $indices[] = intval($ind);
	}

	$indices = array_values(array_unique($indices,SORT_NUMERIC));

	$sql = "
	select * from bible_kjv where `index` in (".implode(',',$indices).")
	group by `index`
	order by `index`
	";

	$res = mysql_query($sql);
	$results = array(array());
	
	while($row = mysql_fetch_assoc($res)) {
		$results[0][] = $row;
	}
	return $results;
}

// recursive helper function for proximity2
function proxcheck2($book, $chapter, $verse, $terms, $range){

global $default_search_bible_version;

$vlo=$verse-$range[0];
if($vlo<0){$vlo=0;}
$vhi=$verse+$range[0];

//print_out($range);

//echo("Range: ".$range[0]." vlo $vlo vhi $vhi book $book chapter $chapter verse $verse<br>");

$res=getVersesQuick($book,$chapter, $vlo,$vhi, $terms[0]);

if ($res==null){return "";}

//$refarr=array();

$res_ref="";

if (count($terms)>1){

$terms_new=array_slice($terms,1);
$range_new=array_slice($range,1);

foreach ($res as $v){

$res_ref.=proxcheck2($v['book'],$v['chapter'],$v['verse'],$terms_new,$range_new).";";

}//end foreach

$res_test=$res_ref;

if(str_replace(";","",$res_test)==""){return "";}

if($res_ref!=""){$res_ref.="$book $chapter:$verse;";}

}// end if
else{

foreach($res as $v){

$res_ref.=$v['book']." ".$v['chapter'].":".$v['verse'].";";

}
$res_ref.="$book $chapter:$verse;";

}// end else

//echo("p2 res_ref $res_ref<br>");



return $res_ref;

}// end proxcheck2

// optimizes the order of the terms and range for a proximity search
// $terms - array of search terms
// $range - array or integer
// returns array containing optimized terms, range

function proxorder($terms, $range){

global $default_search_bible_version;

if (!is_array($range)){$range=array_fill(0,count($terms),$range);//}
}
/* This code produces invalid results as the prox search is not commutitive

//if ((array_product($range))==($range[0]^count($range))){

// same range - check all terms
//echo("I am here");
$cn=array();

foreach ($terms as $term){

$res=getSearch($lookup,$term,$default_search_bible_version,array("Boolean Search","wholeword"));

$cn[]=count($res[0]);

}//end foreach

array_multisort($cn, $terms);

//print_out($terms);
//print_out($range);

}// end if
else{
*/

// different ranges - check first and last terms only

$res1=getSearch($lookup,$terms[0],$default_search_bible_version,array("Boolean Search","wholeword"));

$res2=getSearch($lookup,$terms[count($terms)-1],$default_search_bible_version,array("Boolean Search","wholeword"));

if((count($res2[0]))<(count($res1[0]))){

$terms=array_reverse($terms);
$range=array_reverse($range);

//print_out($terms);
//print_out($range);

}// end if

//}// end else

return array("terms" => $terms, "range" => $range);

}//

function proxcheck($term,$ref){

global $default_search_bible_version;

//echo("proxcheck: $term<br>");

//print_out($ref);

$ref2=array();

foreach($ref as $r){

//print_out($r);
//$r=implode("; ",$ref);
//echo($r);
$res=getVersesQuick($r[0], $r[1], $r[2], $term, $default_search_bible_version,"wholeword");
//$res=getVerses($r[0], $r[1], $r[2], $term, array("Boolean Search","wholeword"), $default_search_bible_version);
//$res=getSearch($r,$term,$default_search_bible_version,array("Boolean Search","wholeword"));
//$ref2=proxexplode($res, 1);



if(is_array($res[0])){
//print_out($res);
$ref2[]=$r;
}

}// end while



return $ref2;

}// end proxchech

function repeat_remove($verses){
$verses2=array();
$used=array();

$cn=count($verses);

$i=0;
while($index=$verses[0][$i]["index"]){

if(array_search($index,$used)===false){
$verses2[]=$verses[0][$i];
$used[]=$index;
}// if

$i++;
}//while


return array($verses2);

}// proxremoverepeat

// par - paragraph
// op - AND, OR
// not - NOT
// let - letter
// sp - space
function advancedType($char){

if (($char=="(")){return "lpar";}
if (($char==")")){return "rpar";}
if (($char=="&") or ($char=="|") or ($char=="^")){return "op";}
if ($char=="-"){return "not";}
if ($char==" "){return "sp";}
return "let";
}// end advancedType

// returns the language (standard 2 character)
// for the given "version"


function getBible($ver){

//echo("ver $ver");

$ver = mysan($ver);

$res=mysql_query("select * from `bible_versions` where (`shortname` = '$ver') LIMIT 1");
//echo(mysql_error());
return mysql_fetch_array($res);

}//

function print_out($item){

echo("<pre>");
print_r($item);
echo("</pre>");

}

function previousBook($book, $version="") {

	global $default_bible_version;

	if($version==""){$version=$default_bible_version;}

	$book = mysan($book);
	$version = mysan($version);

	$res=mysql_query("select `book` from `bible_$version` where `book` < '$book' ORDER BY `index` DESC LIMIT 1;");

	$ar=mysql_fetch_array($res);

	return $ar["book"];
}

function nextBook($book, $version="") {
	global $default_bible_version;

	if($version==""){$version=$default_bible_version;}

	$book = mysan($book);
	$version = mysan($version);

	$res=mysql_query("select `book` from `bible_$version` where `book` > '$book' ORDER BY `index` LIMIT 1;");
	//echo(mysql_error());
	$ar=mysql_fetch_array($res);
	//print_r($ar);

	return $ar["book"];
}

function previousChapter($book,$chap, $version=""){

global $default_bible_version;

if($version==""){$version=$default_bible_version;}

$book = mysan($book);
$version = mysan($version);
$chap = mysan($chap);

$res=mysql_query("select `chapter` from `bible_$version` where (`book`= '$book' AND `chapter` < '$chap') ORDER BY `index` DESC LIMIT 1;");


$ar=mysql_fetch_array($res);

if($ar==null){

$book=previousBook($book,$version);

$res=mysql_query("select `chapter` from `bible_$version` where (`book`= '$book') ORDER BY `index` DESC LIMIT 1;");

$ar=mysql_fetch_array($res);

}// end if

return array($book,$ar["chapter"]);

}// end function

function nextChapter($book,$chap, $version=""){
global $default_bible_version;

if($version==""){$version=$default_bible_version;}

$book = mysan($book);
$version = mysan($version);
$chap = mysan($chap);

$res=mysql_query("select `chapter` from `bible_$version` where (`book`= '$book' AND `chapter` > '$chap') ORDER BY `index` LIMIT 1;");
//echo(mysql_error());
$ar=mysql_fetch_array($res);

if($ar==null){

$book=nextBook($book,$version);

$res=mysql_query("select `chapter` from `bible_$version` where (`book`= '$book') ORDER BY `index` LIMIT 1;");

$ar=mysql_fetch_array($res);

}// end if

return array($book,$ar["chapter"]);

}

?>