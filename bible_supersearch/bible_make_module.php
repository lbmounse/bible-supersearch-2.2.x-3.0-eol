<?php
// SECURE
// converts MySQL Bible to a Bible SuperSearch module

$bible=$_GET["bible"];
$verify=$_GET["verify"];

require_once("bible_mysql.php");
connect();
require_once("bible_inst_functions.php");
require_once("bible_system.php");

require_once("bible_inst_auth.php");
if ($auth==false){return;}

$dir=$module_dir;

$bible = mysan($bible);

// converts MySQL Bible into Bible SuperSearch module
ini_set("memory_limit","20000000");

?>

<html><title>Module Creator</title>
<style>
body{margin:50px;}

</style>
<big><center>Bible SuperSearch Module Creator</center></big><br><BR>

<?php

if ($bible!=""){
$res=mysql_query("select * from `bible_versions` where `shortname` = '$bible'");
$bible_info=mysql_fetch_array($res);
$lang=$bible_info["language_short"];
$long=$bible_info["fullname"];

if (empty($bible_info)){

echo("<B>ERROR: The '$bible' Bible version is not properly installed does not exist on your system</b>. <br>Make sure that you have it installed properly, including the entry into the 'bible_versions' table. <br> See the 'Developers' Documentation' for details.<br><BR><BR>");

$verify="";
$bible="";

}//

}// end if

if ($prog_status=="development"){$sup="This module is officially supported by BibleSuperSearch.com";}
else{$sup=$module_dis;}

// interface components


if ((!file_exists($dir))||(!is_writable($dir))){

echo("<b>ERROR: Directory '$dir' does not exist or is not writable. &nbsp; To convert Bibles to modules, you must create a directory (folder) named '$dir' in your Bible SuperSearch directory, and enable read/write permissions (777) for this directory.</b><br><BR><BR>");

$verify="";

}//



if ($verify==""){

echo("This will convert a Bible in your MySQL database to a Bible SuperSearch module. &nbsp; Approximately 6 MB of space on your webserver is required to store the created module files. &nbsp; The module files will be placed in the '$dir' directory in your Bible SuperSearch directory. &nbsp; After the module is created, you may download it and/or move it to your Bible SuperSearch directory.<br><BR>

This convertor assumes that the Bible is functioning properly in the Bible SuperSearch script.<Br>Before you convert a Bible to a module, please make sure you have throughtly tested it in Bible SuperSearch. <br>
If your Bible does not appear in the Bible version menu, or 'testing' the Bible fails, this convertor will NOT work!

<br><br>Use of this converter is subject to the following terms: You shall distribute modules created with a disclaimer stating that it is not supported by Bible SuperSearch. &nbsp; You shall include the copyright information, if any, in the 'description' of the Bible, and you must have the copyright holder's permission to use and/or redistribute this Bible, if required. &nbsp; Use this convertor ONLY if you accept these terms.

<br><BR><BR>");

}//

echo("<center>");

if ($bible==""){

//$bible="martin"; // bible short name

echo("

<b>Enter the 'shortname' of the Bible to convert</b><br><BR>
<table><tr><td>
<form method=get>Bible 'shortname' <input type=text name=bible><input type=hidden name=verify value=true>
<input type=submit value='Create Module'> </form> </td>

<td><form method=get action='bible_inst_manager.php'><input type=submit value='Cancel'></form></td></tr></table>

<br><br>Conversion may take a little while to proccess. &nbsp; Please wait.
");

}// end if

if (($bible!="")&&($verify=="")){

echo("
<b>Create module for the '$bible' Bible Version?</b><br><br>
<table><tr><td>
<form method=get><input type=hidden name=bible value='$bible'><input type=hidden name=verify value=true>
<input type=submit value='Create Module'> </form> </td>

<td><form method=get action='bible_inst_manager.php'><input type=submit value='Cancel'></form></td></tr></table>

<br><br>Conversion may take a little while to proccess. &nbsp; Please wait.");
}//

if (($bible!="")&&($verify=="true")){

$res=mysql_query("select * from `bible_versions` where `shortname` = '$bible'");
$bible_info=mysql_fetch_array($res);
$lang=$bible_info["language_short"];
$long=$bible_info["fullname"];



$readme="
Bible SuperSearch module for
$long ($bible)

For more Bibles, please see www.BibleSuperSearch.com

$sup
$module_requirements

Installation Instructions
* Extract all files
* Upload all these files to your Bible SuperSearch directory
* Open the Bible Manager (\"bible_install.php\")
* Install \"$bible\"
* Once installed, test it
* If all is well, you are done!

";

makefile($bible."_readme.txt",$dir, $readme);

// bible book list/



$file="
-- Bible SuperSearch 
-- www.BibleSuperSearch.com
--
-- Book list for the ".$bible_info["language"]." language.

DROP TABLE IF EXISTS `bible_books_$lang`;
CREATE TABLE `bible_books_$lang` ( `number` int(11) NOT NULL auto_increment,  `fullname`tinytext NOT NULL,  `short` tinytext NULL,  `chapters` int(11) NOT NULL,  PRIMARY KEY  (`number`)) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;


";
error_reporting(E_ERROR);
$re=mysql_query("select `chapters` from `bible_books_en`");
$chap=array("");
while($chap[]=mysql_fetch_array($re)){}

$booklist=mysql_query("select * from `bible_books_$lang`");

while($bo=mysql_fetch_array($booklist)){

$fn=$bo["fullname"];
$sh=$bo["short"];
$num=$bo["number"];


$file.="INSERT INTO `bible_books_$lang` (`number`,`fullname`,`short`,`chapters`) VALUES ('$num','$fn','$sh','".$chap[$num]["chapters"]."');
";

}// end while
error_reporting(E_ERROR | E_WARNING| E_PARSE);
if($booklist!=null){
makefile("bible_books_$lang.sql", $dir,$file);
}//
//echo("Book file Done!<br><BR>");

//return;

$res=mysql_query("select * from `bible_$bible` ORDER BY `index` DESC LIMIT 1");
$ar=mysql_fetch_array($res);

/*
echo("<pre>");
print_r($ar);
echo("</pre>");
*/

echo(mysql_error());
$num_ver=$ar["index"];

$res=mysql_query("show table status like 'bible_$bible'");
$ar=mysql_fetch_array($res);

/*
echo("<pre>");
print_r($ar);
echo("</pre>");
*/

$module_size=$ar["Data_length"]+$ar["Index_length"];


$num_of_files=ceil($module_size/(1800*1024));
if($num_of_files<3){$num_of_files=3;}
$lines_per_file=ceil($num_ver/$num_of_files);


//echo("<br><BR>number $num_ver modzise $module_size num_files $num_of_files lines $lines_per_file<br><BR>");

$limit=array();

for($i=1;$i<=$num_of_files;$i++){


$start=($i-1)*$lines_per_file;
$stop=$lines_per_file;

$lim="LIMIT $start,$stop";

//echo("$lim<br>");

$limit[]=$lim;

}


/*
echo("<pre>");
print_r($ar);
echo("</pre>");
*/

// book text

$index=0;
//$limit=array("LIMIT 0,10000","LIMIT 10000,10000","LIMIT 20000,15000");

$i=0;
while($lim = $limit[$i]){

$in=$i+1;

$text0="

-- Bible SuperSearch
-- Bible Installation MySQL script
-- For '$long' (".$bible_info["language"].")
--
-- Part $in of $num_of_files
--
-- These three Bible installation scripts must be run IN ORDER!!
-- For installation instructions, please see README.txt
--
-- www.BibleSuperSearch.com
-- 
-- $module_requirements
--
-- $sup
--

";

// $text0.="-- WARNING: THIS BIBLE MODULE WAS CREATED BY A THIRD PARTY.  IT IS NOT SUPPORTED BY BIBLESUPERSEARCH.COM";

if (($i==0)&&($bible_info["description"]=="")){$text0.="
-- The TEXT of the '$bible' Bible version included in this script is in the public domain in most
-- parts of the world.   It is up to the user to determine if it is in the
-- public domain in their locale.   It is free to copy and redistribute
-- verbatim, but modifying the TEXT is forbidden by the Word of the Most High 
-- God that are contained within.   The only modifications allowed are fixing
-- typographical errors to make it conform with the actual text of the '$bible' Bible version.
--
-- Deuteronomy 4:2   Ye shall not add unto the word which I command you, neither
-- shall ye diminish ought from it, that ye may keep the commandments of the
-- LORD your God which I command you.  KJV
--
-- Revelation 22:16, 18, 19   I Jesus ... testify unto every man that heareth
-- the words of the prophecy of this book, If any man shall add unto these
-- things, God shall add unto him the plagues that are written in this book:  
-- And if any man shall take away from the words of the book of this prophecy,
-- God shall take away his part out of the book of life, and out of the holy
-- city, and from the things which are written in this book.   KJV
--
-- This program is released with no warranty of any kind, 
-- either written or implied.
-- -------------------------------------------------------
-- ";}

// if there is a description, display it

$desc=str_replace("<br>","\r\n",$bible_info["description"]);
$desc=strip_tags($desc);
$desc="-- ".str_replace(array("\r\n","\n\r","\n","\r"),"\r\n--",$desc)."

";

if (($i==0)&&($bible_info["description"]!="")){$text0.=$desc;}

if($i==0){
$text0.="-- Insert into bible version register table.


DELETE FROM `bible_versions` where `bible_versions`.`shortname` = '$bible';
INSERT INTO `bible_versions` (`index`, `shortname`, `fullname`, `description`, `language`, `language_short`) VALUES ('', '$bible', '".str_replace(array("'","\r\n"),array("\'",""),$bible_info["fullname"])."', '".str_replace(array("'","\r\n","\n\r","\n","\r"),array("\'","","","","",""),$bible_info["description"])."','".$bible_info["language"]."','$lang');

-- --------------------------------------------------------

-- Create table 'bible_$bible'

DROP TABLE IF EXISTS `bible_$bible`;
CREATE TABLE `bible_$bible` (  `index` int(11) NOT NULL auto_increment,  `book` int(2) NOT NULL default '0',  `chapter` int(3) NOT NULL default '0',  `verse` int(3) NOT NULL default '0',  `text` text NOT NULL,  KEY `index` (`index`)) ENGINE=MyISAM PACK_KEYS=0;

";
}// end if

$text="";

$res=mysql_query("select * from `bible_$bible` $lim");
//echo(mysql_error()." select * from `bible_$bible` $lim<br>");

$k=0;
while($ver=mysql_fetch_array($res)){
//$k++;
$tex=str_replace("'","\'",$ver["text"]);
$tex=stripnewline($tex);

$index++;
$te="insert into `bible_$bible` values('',".$ver["book"].",".$ver["chapter"].",".$ver["verse"].",'$tex');";

//$te="insert into `bible_$bible` values('$index',".$ver["book"].",".$ver["chapter"].",".$ver["verse"].",'$tex');";

//alternate code
//$te="insert into `bible_$bible` values(".$ver["index"].",".$ver["book"].",".$ver["chapter"].",".$ver["verse"].",'$tex');";

$text.=$te."
";

}// end while


//$test=str_replace(");",");
//",$text);

makefile($bible.($in).".sql", $dir, $text0.$text);

//echo("Part $in of 3 done! $lim k=$k<br><BR>");

//break;
$i++;
}// end while

$links="";

for($i=1;$i<=$num_of_files;$i++){

$links.=modlink($bible."$i.sql")."<br>";

}

$links.=modlink($bible."_readme.txt");

if($booklist!=null){$links.="<br>".modlink("bible_books_$lang.sql");}

echo("Module for the '$bible' Bible created. &nbsp; The files are located in the '$dir' directory. &nbsp;


<!--<br> You should download these and/or move them to your main Bible SuperSearch directory. <BR><BR>

Note: If you do not place the files into your main directory, the Manager will continue to list the module as 'Not Available'--><br><BR>

You may download the files now with the links below.<br>Right click on them, and click \"Save As ...\"<br><BR>

$links

<br><BR><BR><BR>
<table><tr>
<td><form method=get action='bible_inst_manager.php'><input type=submit value='Return to Bible SuperSearch Manager'></form></td>

<td>
<form method=get>
<input type=submit value='Create Another Module'> </form> </td>

</tr></table>


");

}// end if

function modlink($href){

global $dir;

return "<a href='$dir$href'>$href</a>";

}// end modlihnk


function makefile($file_name, $dir, $file){

$filename=tempnam($dir, $file_name);
chmod($filename, 0777);
write_file($filename,$file);
if(is_file($dir.$file_name)){unlink($dir.$file_name);}
rename($filename, $dir.$file_name);

}// end makefile


function stripnewline($text){

return str_replace(array("\r\n","\n\r","\n","\r"),"",$text);


}// 
 

?>