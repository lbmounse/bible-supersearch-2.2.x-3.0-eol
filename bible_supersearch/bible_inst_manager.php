<?php
// SECURE
//session_save_path("modules");
//session_start();

echo("<title>Bible SuperSearch Manager</title>");

//error_reporting(E_ERROR | E_PARSE);

//error_reporting(E_ALL);

require_once('bible_misc.php');

$user_post=sanitize($_POST["user"]);
$pass_post=sanitize($_POST["pass"]);
$host_post=sanitize($_POST["host"]);


//echo("$user||$pass");

require_once("bible_inst_functions.php");
require_once("bible_system.php");

$session=getSession();



mysql_close();

//if(!check_login_file()){return;}

if(($session["auth"]!="true")&($user_post!="")){

//echo("I am here");

require_once("bible_login.php");
if ($host_post==""){$host_post=$host;}
if ($host_post==""){$host_post="localhost";}

// new username/password
if(($user!=$user_post)||($pass!=$pass_post)){

//echo("I am here 2");

//echo("$host_post $user_post $pass_post");
error_reporting(0);
mysql_connect($host_post,$user_post,$pass_post);
error_reporting(E_ALL ^ E_NOTICE);
//echo("$host_post $user_post $pass_post");
echo(mysql_error());

if(mysql_error()!=""){

echo("
Your username/password was rejected by the server.<br><BR>

Please try again.<BR><BR>");

$showhost=true;
require_once("bible_inst_login.php");
return;
}// end inner if

// store new username and password

if (($user_post!=$user)||($host_post!=$host)||($pass_post!=$pass)){

changelogin($host_post,$user_post,$pass_post,$db);
}

}// end if

//printout($session);

connect();

if(($session==null)&&($db!=null)){

//echo("I am here 99");
runmysql("bible_sessions.sql");
$session=getSession();

}



//session_id("5");
$session["user"]=$user_post;
//$session["pass"]=$pass_post;
$session["auth"]="true";

setSession($session);
////echo($_SESSION['auth'].$_SESSION['user'].$_SESSION['pass']);
//echo(SID);

}//
else{
require_once("bible_inst_auth.php");
//echo("I am here 11");
if ($auth==false){return;}
}// end else

if(($_POST["auth"]=="true")&&($db!="")){

echo("You must now log in again to continue the installation.<br><br>");

require_once("bible_inst_login.php");

return;
}

?>


<div style="text-align:center;color:darkred;font-size:140%; font-weight:bold;">Bible SuperSearch Installation Manager</div><br><br>

<center>

<?php
menu("bible_inst_manager.php");


require("bible_login.php");

//echo("I am here 2<br>");

//checkFiles();


// installation: select database// detect no 'bible_versions' here ==> prompt to install Bible SuperSearch
if ($db==""){

echo("<big>Bible SuperSearch installation</big><br><BR>Checking for required files:<br><BR>");

if(checkFiles()==false){
echo("<big style='color:red'>Some required files are missing. &nbsp; Please upload them before you continue with the installation</big>.<br><BR>");
return;}
//echo("I am here 3<br>");
?>

<br><BR>
Step 1: Select Database &nbsp;
<form action=bible_inst_database.php method=post><input type=hidden name=auth value=true><input type=submit value="Go"></form> 
<?php
return;
}// end if

// detect no 'bible_versions' here ==> prompt to install Bible SuperSearch


// php 4 workaround equivalent for scandir
$dir=array();
//echo($module_dir);

if($module_dir==""){$d = dir('.');}
else{$d =dir("$module_dir");}

while (false !== ($entry = $d->read())) {
   $dir[]= $entry;
//echo($entry."<br>");
}

// end scandir workaround

$installed=array();
$inst=array();
require_once("bible_mysql.php");
connect();

$res=mysql_query("select * from `bible_versions` order by `index`");
$err=mysql_error();
if ($err!=""){

runmysql("bible_sessions.sql");
$session=getSession();
$session["auth"]="true";
setSession($session);

// detect no 'bible_versions' here ==> prompt to install Bible SuperSearch
?>
Bible SuperSearch installation<br><BR>

Step 2: Create Bible SuperSearch tables &nbsp;
<form action=bible_inst_setup.php><input type=submit value="Go"></form> 
<?php
return;
}//
while($in=mysql_fetch_array($res)){$installed[]=$in;}
if (empty($installed)){
$stat="stat";
?>
Bible SuperSearch installation<br><BR>

Step 3: Install a Bible Version:<br><BR>

<?php

}//

else{$stat="";}

foreach($installed as $t){$inst[]=$t["shortname"];}

$Available=array();


$i=0;
$sql=array();
foreach($dir as $file){
//while($file=$dir[$i]){

if(strpos($file,".sql")!==false){

$ipos=strpos($file, "1.sql");
$version=substr($file, 0,$ipos);
$extension=substr($file, $ipos+1);
//echo("$version<br>");

if((array_search($version."2.sql",$dir)!==false)&&(array_search($version."3.sql",$dir)!==false)&&($file==$version."1.sql")){

$Available[]=$version;
//echo("$version--<br>");

}//

}// end if

//echo("$i:$file<br>");
$i++;
}

//echo("I am here 5<br>");

$supported=array();
$supported[]=array("short" => "kjv", "long" => "Authorized King James Version");
$supported[]=array("short" => "tyndale", "long" => "Tyndale Bible");
$supported[]=array("short" => "tr", "long" => "Textus Receptus 1550/1884 NT(Greek)*");
$supported[]=array("short" => "trparsed", "long" => "Textus Receptus Parsed 1550/1884 NT(Greek)");
$supported[]=array("short" => "1858rv", "long" => "Reina Valera 1858 NT (Spanish)");
$supported[]=array("short" => "1909rv", "long" => "Reina Valera 1909 (Spanish)");
$supported[]=array("short" => "sagradas", "long" => "Sagradas Escrituras 1569 (Spanish)");
$supported[]=array("short" => "rvg", "long" => "Reina Valera Gómez (Spanish)");
$supported[]=array("short" => "martin","long" => "Martin 1744 (French)");
$supported[]=array("short" => "epee","long" => "La Bible de l'Épée (2005) (French)");
$supported[]=array("short" => "oster","long" => "Ostervald 1996 (French)");
$supported[]=array("short" => "afri","long" => "Afrikaans 1953 (Afrikanns)*");
$supported[]=array("short" => "svd","long" => "Smith Van Dyke (Arabic)");
$supported[]=array("short" => "bkr","long" => "Bible Kralicka (Czech)*");
$supported[]=array("short" => "stve","long" => "Staten Vertaling (Dutch)");
$supported[]=array("short" => "finn","long" => "Finnish 1776 (Finnish)*");
$supported[]=array("short" => "luther", "long" => "Luther 1545 (German)");
$supported[]=array("short" => "karoli","long" => "Karoli (Hungarian)*");
$supported[]=array("short" => "diodati","long" => "Diodati 1649 (Italian)");
$supported[]=array("short" => "lith","long" => "Lithuanian Bible (Lithuanian)*");
$supported[]=array("short" => "maori","long" => "Maori Bible (Maori)*");
$supported[]=array("short" => "cornilescu","long" => "Cornilescu (Romanian)");
$supported[]=array("short" => "synodal","long" => "Synodal 1876 (Russian)");
$supported[]=array("short" => "thaikjv","long" => "Thai KJV (Thai)*");


$remote="off";

if($enable_remote_install=="on"){

//global $supported, $remote;

/// 
// array of remote servers at time of this release
$rem_servers=array("http://biblesuper.joshost.com/","http://www.alive-in-christ.com/bible/");

//try to get updated list of servers from Bible SuperSearch server
include("http://www.alive-in-christ.com/bible/modules/servers.txt");

//echo("moddir |$module_dir");
global $allow_url_include;
$allow_url_include=true;
//try to connect to remote servers
//foreach($rem_servers as $s){

$server=pickRemoteServer($rem_servers);

$file=$server.$module_dir."remote.txt";
//echo("file |$file|<br>");

include($file);
//if able to connect
//if ($remote=="on"){
//$server=$s;
//$supported=getme();
//break;
//}// end if

//}// end foreach

if ($remote=="on"){echo("<br>Bible SuperSearch does not endorse anything advertised above.<BR>Remote install enabled. &nbsp; Connected to server 
\"$server\"<br>");}
else{echo("Remote install disabled: could not find server. &nbsp; Is your internet connection working? &nbsp; If you are using PHP =< 5.2.0, you must enable 'allow_url_include' in your 'php.ini' for Remote Module Installation to work.");}

}// end remote install

//else{echo("Remote install disabled.  You may enable it in \"Options\"<br><BR>");}



GlorifytheLord("yes");

echo("
This manages installation and uninstallation of Bibles.<br>To adjust your Bible version menu, please go to the \"Bible List\" page.<Br><BR>

<table border=2>
<tr><td colspan=6 align=center><b>Officially Supported Bibles</b></td></tr>
<tr><td><b>Name</b></td><td><b>Description</b></td><td><b>Module
</td><td><b>Installed</td><td><b>Action</td><td><b>Test</td></tr>");
$sup=array();
foreach ($supported as $t){
$sup[]=$t["short"];

if (array_search($t["short"],$Available)===false){$status="Not Available";}//
else{$status="Available";}

if($status=="Not Available"){

if($remote=="off"){
$action="Download";}
else{$action="Remote Install";}
}// end if
else{$action="Install";}

if (array_search($t["short"],$inst)===false){$ins="No";
$test="<span style='color:grey'>Test</a>";
}//
else{$ins="Yes";
$action="Uninstall";
$test="<a href=bible_test.php?bible=".$t["short"].">Test</a>";
}



echo("<tr><td>".$t["short"]."</td><td>".$t["long"]."</td><td>$status
</td><td>$ins</td><td>");

actionLink($t["short"],$action);


echo("$action</a></td><td>$test</td></tr>");
}// end 

$A=$Available;
$s=$supported;
sort($A);
sort($s);

if($A!=$s){
echo("<tr><td colspan=6><b><center>Unsupported Bible Versions</center></b><br>
Bibles listed below are available on your system, <br>but are not supported by BibleSuperSearch.com</td></tr>");
}// end if

foreach($Available as $t){



if (array_search($t,$sup)===false){

if (array_search($t,$inst)===false){
$ins="No";
$action="Install";
$name="n.a";
$test="<span style='color:grey'>Test</a>";
}//end if
else{$ins="Yes";
$pos=array_search($t, $inst);
$name=$installed[$pos]["fullname"]." (".$installed[$pos]["language"].")";
$action="Uninstall";
$test="<a href='bible_test.php?bible=".$t."'>Test</a>";
}// end else



$status="";
echo("<tr><td>$t</td><td>$name</td><td>Available</td><td>$ins</td><td>");

actionLink($t,$action);
echo("$action</a></td><td>$test</td></tr>");

//$status="Not Available";

}// end

}// end 

foreach($installed as $t){

if ((array_search($t["shortname"],$sup)===false)&&(array_search($t["shortname"],$Available)===false)){
$test="<a href=bible_test.php?bible=".$t["shortname"].">Test</a>";

echo("<tr><td>".$t["shortname"]."</td><td>".$t["fullname"]." (".$t["language"].") </td><td><a href='bible_make_module.php?bible=".$t["shortname"]."' title='Make Bible SuperSearch module for this Bible version.'>Create Module</a></td><td>Yes</td><td>");

actionLink($t["shortname"], "uninstall");
echo("Uninstall</a></td><td>$test</td></tr>");

}// end if

}//end foreach

echo("</table>");

function actionLink($version, $action){
global $server;

$ac=explode(" ",$action);
$act=strtolower($ac[0]);
if ($act=="remote"){$act.="&server=$server";}

echo("<a href='bible_inst_actions.php?version=$version&action=$act'>");

return;
}// end actionLink

?><br><BR>
* These Bibles do not have a list of the Biblical books in their languages.<br>

<BR>
<B><a href="bible_add_interface.php">Add Unsupported Bibles Not Listed Above</a></b>
