<?php 
// SECURE
session_start(); 
ini_set("memory_limit","40000000");
?>

<style>
body{text-align:center;font-size:150%;}
</style>

<?php
// Bible Version installation actions

// verify logged in

require_once("bible_inst_auth.php");
if ($auth==false){return;}

// require_once("bible_inst_id.php");
require_once("bible_mysql.php");
connect();

require_once("bible_inst_functions.php");
require_once("bible_system.php");
require_once("bible_misc.php");
$sub=$_POST;
if (empty($sub)){$sub=$_GET;}

$action=$sub["action"];
$version=mysan($sub["version"]);
$server=$sub["server"];
$verify=$sub["verify"];

// install
if ($action=="install"){

install($version);

echo("Installation of the Bible version '$version' complete.<BR><BR>

<form action=bible_inst_manager.php><input type=submit value='Continue'></form>"); 
}

// remote install
if ($action=="remote"){

install($version, $server);
echo("Installation of the Bible version '$version' complete.<BR><BR>

<form action=bible_inst_manager.php><input type=submit value='Continue'></form>"); 


}// end remote

// verify
if ((($action=="uninstall")||($action=="remove"))&&($verify!="true")){

if ($action=="uninstall"){echo("WARNING: This will uninstall the '$version' Bible version.");}
else {echo("FINAL WARNING: This will completely remove Bible SuperSearch from your system.");}

echo("<br><br>
Do you wish to continue?<br><BR>

<table align=center><tr><td>
<form action=bible_inst_actions.php method=post><input type=hidden name=action value=$action><input type=hidden name=verify value=true><input type=hidden name=version value=$version><input type=submit value=' Uninstall $version'></form></td><td><form action=bible_inst_manager.php><input type=submit value=Cancel></form></td></tr></table>");

return;
}// end else

//uninstall
if ($action=="uninstall"){
uninstall($version);
echo("Bible version '$version' uninstalled.<br><br>

<form action=bible_inst_manager.php><input type=submit value='Continue'></form>");

}// end if

// complete uninstall
if ($action=="remove"){

$res=mysql_query("select * from `bible_versions`");

// uninstall all bible versions
while($ver=mysql_fetch_array($res)){

uninstall($ver["shortname"]);

}// end while

mysql_query("drop table `bible_versions`");
mysql_query("drop table `bible_shortcut`");
mysql_query("drop table `bible_books_en`");
mysql_query("drop table `bible_shortcuts_en`");
mysql_query("drop table `bible_session`");

?>

The Bible SuperSearch tables have been removed from your system. <br><Br> To complete the uninstallation, please delete all Bible SuperSearch files from your server.

<?php

}// end if

if($action=="download"){

?>

Please go to <a href=http://www.biblesupersearch.com target="_new">www.BibleSuperSearch.com</a> to download this Bible version.

<?php

}// end if

?>
