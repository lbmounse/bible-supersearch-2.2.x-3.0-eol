<?php

/**
 * Plugin Name: Bible SuperSearch Legacy
 * Plugin URI: http://www.biblesupersearch.com/downloads/
 * Description: This is a temporary plugin to allow use of Bible SuperSearch 2.x.x with WordPress.  As the current Bible SuperSearch code is not compatible with WordPress, it simply wraps the software interface with a disguised iframe.  A future rewrite of Bible SuperSearch will work as a native plugin.
 * Version: 2.2.00
 * Author: Luke Mounsey
 * Author URI: http://www.biblesupersearch.com
 * License: GPL2 or Greater
 */

wp_enqueue_script( 'biblesupersearch_legacy_javascript', plugin_dir_url(__FILE__) . 'bible_legacy.js', array('jquery'));
add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'biblesupersearch_add_action_links');
function biblesupersearch_add_action_links($links) {
	$nlinks = array(
		"<a href='" . plugin_dir_url(__FILE__) . "bible_install.php' target='_NEW'>Manage</a>"
	);

	return array_merge($nlinks, $links);
}

add_shortcode('biblesupersearch_legacy', 'biblesupersearch_legacy_query');
function biblesupersearch_legacy_query($atts, $content) {
	extract(shortcode_atts(array(
		'interface' => 'user_friendly2',
		'iframe_id' => 'bible_legacy_frame'
	), $atts));
	
	$plugin_url = plugin_dir_url(__FILE__);
	
	$html = "
		<iframe 
		id='{$iframe_id}' scrolling='no'
		src='{$plugin_url}bible_supersearch.php?interface={$interface}' 
		width='100%' onload='bibleSuperSearchResize(\"{$iframe_id}\")' border='0' style='border:0;'
	></iframe>
	";
	
	return $html;
}

add_shortcode('biblesupersearch_legacy_version_list', 'biblesupersearch_legacy_version_list');

function biblesupersearch_legacy_version_list() {
	$base_path = plugin_dir_path(__FILE__);
	require_once($base_path . "bible_login.php");
	$res = mysql_query("select * from `{$db}`.`bible_versions` Order by `index`");
	$html = mysql_error();
	$html .= '<table border="0">';

	while($row = mysql_fetch_assoc($res)) {
		$html .= '<tr><th>' . $row['fullname'] . '</th><th>' . $row['language'] . '</th></tr>';
		$html .= '<tr><td colspan="2">' . $row['description'] . '</td></tr>';
		$html .= '<tr><td colspan="2"></td></tr>'; 
	}

	$html .= '</table>';

	return $html;
}