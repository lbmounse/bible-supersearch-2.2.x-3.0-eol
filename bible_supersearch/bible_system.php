<?php


// System configuration
// Debugging settings
// WARNING: These settings are NOT intended to be changed by the end user.
// Do NOT change these settings unless you know what you are doing!
// Changing these settings may cause your system to function erratically, or cease to function at all.

$show_query=false;
$debug_mode=0;
$search_prod="bss";
$prod_version="2.1.45";
$prod_version_short="2.1";
$prog_status="stable";
$module_requirements="Requirements: Bible SuperSearch >= 1.6.30";
$module_dis="This is an unofficial module, and is NOT supported by BibleSuperSearch.com";
$module_dir="modules/";
$max_parallel_bibles=4;
$standard_interface="user_friendly1";
$default_proximity_range=5;

//error_reporting(E_WARNING|E_PARSE|E_ERROR);
//error_reporting(0);


?>
