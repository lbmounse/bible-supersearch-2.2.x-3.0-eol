<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php
// SECURE

// Bible SuperSearch Version 1.6.00
// The free, open source, PHP and MySQL web-based   
// Bible Reference retrevial and search utility
//
// Copyright (C) 2006 Luke Mounsey
// www.BibleSuperSearch.com
// www.Alive-in-Christ.com
//
// bible_display.php
// Contains all functions needed to display verses of the Bible.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your optiocn) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License included in the file 
// "license.txt" for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// bible_display.php version
$bible_display_version=1.6;

$error="";

global $table_display;

// displayVerses
// Main display function
// prints the verses to the webpage
// arranges the verses differently 
// according to the case or style
// @ param ref - associative reference array (see function explodeRef in bible_lookup.php for details)
// @ param verses - array of verses
// @ param issearch - true if search was conducted, false otherwise. (defaults to false)
// @ param min - minimum index of verses to be displayed
// @ param max - maximum index of verses to be displayed
//   if nothing is entered for either of these, the entire contents of verses will be displayed
//   otherwise, only the range min to max will be displayed
// @ param version - Bible version(s)
// @ returns true if displayed, false otherwise

function displayVerses($ref, $verses, $issearch=false, $min=0, $max=-1,$verion=""){

global $default_bible_version, $search, $table_display, $language, $version,$lang,$style,$size;

if ($version==""){$version=$default_bible_version;}

/*
echo("<pre>");
print_r($verses);
echo("</pre>");
*/

//echo(imploderef($ref));

$font_size=fontsize();

//echo(count($verses));

//echo("version $version<br>");
$version=str_replace(" ","",$version);
$version_array=explode(",",$version);

//$version_array[]="rvg";//diagnostic

$num_bibles=count($version_array);
//echo("num_bibles=$num_bibles<br>");

//if ($language==""){$language=getLanguage($version);}


//echo(count($verses[0])."|");

$language_array=array();
$bible_array=array();

$bib_num=0;
// verse index for individual Bibles
$in=array();
$count_verses=array();// number of verses in each Bible selection
//$ignore_bible=array();// if no verses, ignore Bible

while($bib=$version_array[$bib_num]){
//echo($version_array[$bib_num]);
$language_array[]=getLanguage($bib);
$bible_array[]=getBible($bib);
$in[]=0;
$count_verses[]=count($verses[$bib_num]);
//if($count_verses[$bib_num]==0){$ignore_bible[]=1;}
//else{$ignore_bible[]=0;}
$bib_num++;
}// end while

//foreach($count_verses as $cn){echo ("cn $cn<br>");}

if(array_sum($count_verses)==0){

if(!$issearch){
//echo("<div class='center'>Your request for \"<span class='bss_errorsearch'>".imploderef($ref)."</span>\" returned no results.&nbsp; Please edit your request and try again.</div><br>");
}
return null;

}
/*
if((count($version_array))==1){

$vers=array();
$vers[]=$verses;
$verses=$vers;
}
*/

// echo error message if no verses 
$stop=count($verses)-1;
$i=0;
while($i!=$stop){


if ($verses[$i][0]==null){

//echo(" &nbsp; Your request for \"<span class='bss_errorsearch'>".imploderef($ref)."</span>\" from <b>".$version_array[0]."</b> returned no results.<br><BR>");

}// end if

$i++;

}// end while

// find the index of verses
// Bible with most verses serves as index
$mx=max($count_verses);
$mn=min($count_verses);
$bib_num=0;
while($count_verses[$bib_num]!=$mx){$bib_num++;}
$verse_index=$verses[$bib_num];

//echo("max:$mx min:$min index num $bib_num<br><BR>");
//foreach($verse_index as $mm){echo($mm["text"]."<br>");}

$td=$table_display;

if(($num_bibles!=1)&&($td==false)){

$td=true;
$table_display=true;

echo("<p class=center>Note: Easy-Copy is not avalible when using multiple Bibles</p>");

}

//get the case from the ref array
//$case=$ref["style"];// depricated
if(is_array($ref)) {
$book=$ref["book"];
$chapter=$ref["chapter"];
$verse=$ref["verse"];
}
//echo("|verse:".$ref["verse"]."|");

//$test2=$verses[0][0]["text"];

//echo("test $test2");
$test2=$verses[0][0]["text"];
//echo("test |$test2|$version<br>");


$col_width=100/count($version_array);


$test2=$verses[0][0]['text'];
//echo("test |$test2|<br>");

//echo("$book|$chapter|$verse|<br>");

// do we have content?
if (($verses==null)&&($ref==null)){
error("Your request for \"<span class='bss_errorsearch'>$search</span>\" produced no results. &nbsp; Please edit your request and try again.");
//return null;
}
// or is it a search with no results?
// (a search does not have a lookup reference)
if ($verses[0]==null){
//error("Your search produced no results. &nbsp; Please edit your request and try again.");
//return false;
}

//echo("|$book|$chapter|$verse|<br>");
// determine the case, or mode of displaying the verses
// default case = individual
$case="individual";

// if no book, chapter or verse, case = search
if ((empty($book))&&(empty($chapter))&&(empty($verse))){$case="search";}
// if book but no chapter or verse, case = book
if (!(empty($book))&&(empty($chapter))&&(empty($verse))){$case="book";}
// if chapter but no verse, case = chapter
if ((!empty($chapter))&&((empty($verse))||($verse=="-"))){$case="chapter";}
// if there are multiple verses specified in a given chapter, case = range
if ((strpos($verse,"-")!=0)||(substr($verse,0,1)=="-")||(strpos($verse,","))){$case="range";}
// If case = book and wholebook = false, case = chapter
if (($case=="book")&&($wholebook="false")){$case="chapter";}
// If multple chapters are listed, case = book
if ((strpos($chapter, "-")!=0)||(strpos($chapter, ",")!=0)){$case="book";}
// If multiple books are listed, case = search
// (only searches are allowed to request multiple books)
if ((strposneg($book,"-",2)!=-1)||(strpos($book,","))){$case="search";}
// random verse
if (strtolower($book)=='random'){
if(strtolower($chapter)=='chapter'){$case="chapter";}
else{$case="individual";}
}

// If cross chapter range, case = book
if (((strpos($verse,"-")!=0)||(substr($verse,0,1)=="-"))&&(strpos($verse,":")!=0)){
$case="book";
$crossch=true;}
else{$crossch=false;}

// If multiple chapter/verse reference within a reference
if (strposneg($verse, ":")!=-1){
// if there is a range in the multi chapter/verse referece, case = book
if (strposneg($verse,"-")!=-1){$case="book";}
// otherwise, case = verse
else {$case="verse";}
}// end if

// checks to make sure chapter isn't really a range within the same chapter
// this adds up the verse number for up to the first 20 verses.   If this does not
// equal 1+2+3+...+n-1+n, then it is a range, not a chapter.
if (($case=="chapter")&&($verse!="")){
$i=0;
$count=0;
$vcount=0;
while(($i!=20)&&($verses[$i]!=null)){
$ve=$verses[$i];
$count+=$ve["verse"];
$i+=1;
$vcount+=$i;
}// end while

if ($count!=$vcount){$case="range";}
}// end if

// DEBUGGING

//echo("case=$case<br>");

// search case modification
// should limited searches be displayed normally (as determined by case statements above)
// or all as individual verses?
$regsearchcase=false;
if ((!$regsearchcase)&&($issearch)){$case="search";}

// echo("case:".$case."<br>");// case debugging code - comment out for website use

// modify reference if random passage
if (strtolower($book)=='random'){
$book=$verses[0][0]["book"];
$chapter=$verses[0][0]['chapter'];
}


// modifies verse reference for indefinite ranges, ie Matt 4:5- or Acts 10:-20
if ($verse[0]=="-"){$verse="1".$verse;}
if ($verse[strlen($verse)-1]=="-"){$verse.=getNumVerses($book, $chapter, $version);}


// $ALIGN 
echo("<center><div id=main style='text-align:left; margin-left:auto;margin-right:auto;");

if(!$td){echo("font-size:$font_size%;font-family:$style'>");}
else{echo("'>");}

//echo("lang $lang");
$bookfile=getBook($book);



// displaying chapters, books, and ranges
if (($case=="chapter")||($case=="range")||($case=="book")){

// build the heading for the set of verses

//echo("$language<br>");

$head="<div class=passage_header>".$bookfile["fullname"];



// add chapter info if "chapter" or "range"
if(($case=="chapter")&&($language_array[0]=="en")){$head.=" Chapter ";}
if($chapter==""){$chapter="1";}
if($case!="book"){$head.=" ".$chapter;}
// add verse range if "range"
if((!empty($verse))&&($case!="book")){$head.=":".$verse;}



echo("$head</div>");// finish the heading and display it
//if($table_display){echo("</td></tr>");}
// print 

if ((!$textonly)){
prevNext($book,$chapter, $case,$lang);

echo("
");

}// end if

//font-size:$font_size%;font-family:$style

if($td){echo("<table border=0 style='margin-left:auto;margin-right:auto; width:100%;font-size:$font_size%;font-family:$style'>

");}
//if($td){}

if($num_bibles!=1){

if($td){echo("<tr>");}
foreach ($version_array as $bib){

$bib=getBible($bib);

echo("<td colspan=2 valign='bottom' style='text-align:center;width:$col_width%' ><b>".$bib["fullname"]."</b><br><br></td>");

}// end foreach

if($td){echo("</tr>");}

}// end if


// write each individual verse from verses
$i=0;

$chap=0;

$vtest=$verses[0];

while($i<=$mx){

$bib_num=0;

if($td){
$v.="
<tr>";
}

while($bib_num<$num_bibles){

// is this a "right to left" language?
// easy copy not supported for "right to left"
if(isRTL($language_array[$bib_num])){
$rtl=true;
$td=true;
}

else{$rtl=false;}

$align="left";

if($td){$align="justify";}
if($rtl){$align="right";}

$ver=$verses[$bib_num][$in[$bib_num]];

$write=compareRef($ver,$verse_index[$i]);

if($write){$in[$bib_num]++;}
//else{break;}


// build the verse reference to be displayed
$v="";
if($td){//$v.="
//<tr>";

if($rtl){$v.="<td class=t_text valign=top style='text-align:right;width:$col_width%;'>".$ver["text"]."</td><td class='t_verse' valign='top' style='text-align:left'>";}
else{$v.="<td class=t_verse valign=top >";}

}// end if

//if($verses[$i+1]!=null){$v.=" &nbsp; ";}

//if((!$table_display)&&($rtl)){$v.=$ver["text"];}

// add initial asthetic spaces
$ve=$ver["verse"];

if (($ve<10)&&(!$td)){$v.=" &nbsp; ";}
if (($ve<100)&&(!$td)){$v.=" &nbsp; ";}

$v.="<span class=ran_ref>";
// book display
if ($case=="book"){

	// chapter header for book displays
	if (($ver["chapter"]>$chap)&&($ver["chapter"]!=null)&&($bib_num==0)){
		$chap=$ver["chapter"];
		$cols=$num_bibles*2;
		if($td){echo("<tR><td colspan=$cols class=chapter align=center><br>Chapter $chap<br><br></td></tr>");}
		else{
			echo("<br><div class=chapter>Chapter $chap</div><br>
");
		}// end else

	}//end if

//$v.=$ver["chapter"].":"; //uncomment this code if you want book display to give chapter references for individual verses
}// end while


$v.=$ver["verse"];

$v.="</span>";
if(($td)&&(!$rtl)){$v.="</td><td class='t_text' valign='top' style='width:$col_width%;'>";}
//if(($table_display)&&($rtl)){$v.="<td class='t_verse' valign='top'>";}

else{$v.=" &nbsp; ";}// end else

if((!$td)&&($rtl)){$v.=$ver["text"];}
if(!$rtl){$v.=$ver["text"];}

if($td){$v.="</td>";}

if(!$write){$v="<td>&nbsp;</td><td>&nbsp;</td>";}
echo("$v");


if(!$td){echo("<Br>");}
$bib_num++;


}//end while
$i+=1;

if($td){echo("</tr>");}

}// end inner while

//if($table_display){echo("<tr><td colspan=2>");}
if($td){echo("</table>");}

// next chapter, this chapter, last chapter
if (($case!="book") && (!$textonly)){prevNext($book,$chapter, $case,$lang);}// end if

if ($case=="book"){echo("<br><BR>");}

}//end if

// INDIVIDUAL VERSES/ SEARCHES

//else{// 
if(($case=="verse")||($case=="search")||($case=="individual")){
// individual verses and searches display

echo("<div class='bss_verses' style='text-align:$align;'>");

//if(!$td){echo("font-size:$font_size%;font-family:$style'>");}
//else{echo("'>");}

//echo($case);

// search highlighting
if ($case=="search"){
if ($max==-1){$max=count($verses)-1;}
$verses=array_slice($verses,$min,$max-$min+1);

$sa=explodeSearch($search, $searchtype);
$bib_num=0;

while($bib_num<$num_bibles){
$verses[$bib_num]=highlight($verses[$bib_num], $sa);
$bib_num++;
}//end while


}// end if

$j=0;//$min;

//$max=count($verses[0])-1;

// INDIVIDUAL VERSES AND SEARCHES
// write verses to display
//$sa=explodeSearch($search, $searchtype);
//font-size:$font_size%;font-family:$style

if($td){echo("<table style='width:100%;font-size:$font_size%;font-family:$style'>");}


//echo("

//<table border=0>
//");

while($j<=$mx){

$bib_num=0;

if($td){echo("<tr>
");}

while($bib_num<$num_bibles){

$ver=$verses[$bib_num][$in[$bib_num]];

if($issearch){$write=true;}
else{
$write=compareRef($ver,$verse_index[$j]);
}

if($ver==null){

$write=false;

}


if($write){$in[$bib_num]++;}


$text=$ver["text"];
// get book file
$bf=getBook($ver["book"],$language_array[$bib_num]);
//echo($ver["book"]."@<br>");

$ver["text"]=highlightVerse($ver["text"],$sa);



if(isRTL($language_array[$bib_num])){
$rtl=true;
$align="text-align:right;";}
else{
$align="";
$rtl=false;
}

$v="";
if(!$td){
$v="<p style='$align margin:0px;' class=width>";
}

if($td){
$v.="<td class=s_verse style='$align width:$col_width%;padding-right:6px;padding-left:6px;' valign=top><nobr>";
}

if(($num_bibles>1)&&($rtl)){

//$v.="(".strtoupper($bible_array[$bib_num]["shortname"]).") &nbsp; ";

}

$v.="<span class=ver_ref >".contextLink($ver["book"],$ver["chapter"],"","",$lang).$bf["fullname"];
$v.=" ";
$v.=$ver["chapter"];
$v.="</a>:".contextLink($ver["book"],$ver["chapter"], $ver["verse"],"",$lang);
$v.=$ver["verse"];
$v.="</a></span>";

//if($td){
$v.="</nobr>";

if($num_bibles==1){$v.=" &nbsp; ";}
///}//</td><td class=t_text>";}
//else{$v.=" &nbsp; ";}

if(($num_bibles>1)){

$v.="<br>(".strtoupper(substr($bible_array[$bib_num]["shortname"],0)).")";

}

//$v.="</nobr>";

if($td){$v.="<br>";}

//else{
//$v.=" &nbsp; ";//}

$v.=$ver["text"].'<br><br>';

if((!$write)&($td)){$v="<td>";}

//print_r($ver["text"]);

if(!is_array($ver["text"])){echo("$v");}
if($td){
echo("</td>");
}
else{echo("</p>");}

$bib_num++;
//echo("</td>");

}// end inner while

$j+=1;

if($td){echo("</tr>");}

}//end outer while

if($td){
echo("</table>");
}

}// end else if

echo("</div></center>");
return true;
}//end displayVerses

// error
// Builds a list of error messages to print
// @ param msg - message to print
// @ returns - none
function error($msg){

//global $error;

//$error="$error$msg<br><br>";

// The queue does not work in version 1.0
// So it prints it here instead
echo("<div class=bss_error>$msg<br><br></div>");

}// end error

// dispError
// not working implemented version 1.0
// displays the error messages accumulated by function "error" above
// @ param msg - message to display.  If none given, will display $error
// @ returns - none
function dispError($msg=""){

global $error;

if ($msg=""){$msg=$error;}

echo("<div class='bss_error'>$msg</div>");

}// end dispError

// contextLink
// returns a complete <a href=> tag for a "in context" link
// using getContext to generate the context
function contextLink($book, $chapter, $verse="", $range="",$lang=""){

$context=getContext($book, $chapter, $verse,$range,$lang);

$lookup=array($context,"");

if ($verse==""){$title="Show this chapter: $context";}
else{$title="Show this verse in context: $context";}


return makelink(array("lookup","search","all","any","one","none","phrase","prox","prox_range"),$lookup,$title,"ct");
}// end contextLink

// prevNext
// displays links to previous book, previous chapter, current chapter (range only), 
// next chapter, and next book.   Intended to be placed before or after blocks of Scripture text
// @ param book - the book reference.  Can be full name, short name or book number
// @ param chapter - the chapter reference (number)
// @ param case - the display layout case of the reference 

function prevNext($book, $chapter, $case,$lang=""){

global $versions, $wholebook, $browsing_buttons;

if (($chapter=="")&&($wholebook="false")){$chapter="1";}
//if (strposneg($chapter, "-")!=-1){$chapter="";}
$bok=getBook($book,$lang);
$bo=$bok["fullname"];
$bnum=$bok["number"];

if(count($versions)==1){$ver=$version;}
else{$ver="";}

$pb=previousBook($bnum,$ver);
//if ($pb<1){$pb=null;}
$pb=getBook($pb,$lang);
$nb=nextBook($bnum,$ver);
//if ($nb>66){$nb=null;}
$nb=getBook($nb,$lang);

$nc=nextChapter($bnum,$chapter,$ver);
$pc=previousChapter($bnum,$chapter,$ver);
//$cc=
$pcb=getBook($pc[0],$lang);
$ncb=getBook($nc[0],$lang);


$style="bible_images/prevnext/$browsing_buttons/";
$type=imagetype($style);

echo("<br><p class=center> ");

if ($pb!=null){echo(makeLink("lookup",$pb["fullname"],"Previous Book: ".$pb["fullname"])."<img src='".$style."pb.$type' border=0 alt='Previous Book'></a> ");}
else{echo("<img src='".$style."pb_nl.$type' border=0 alt='Previous Book'> ");}

if ($pc[1]!=null){echo(makeLink("lookup",$pcb["fullname"]." ".$pc[1],"Previous Chapter: ".$pcb["fullname"]." ".$pc[1]));
echo("<img src='".$style."pc.$type' border=0 alt='Previous Chapter'></a> ");}
else echo("<img src='".$style."pc_nl.$type' border=0 alt='Previous Chapter'> ");

if ($case=="range"){echo(makeLink("lookup",$bok["fullname"]." ".$chapter,"Show Entire Chapter: ".$bok["fullname"]." ".$chapter));
echo("<img src='".$style."sc.$type' border=0 alt='Show Entire Chapter'></a> ");}
else{echo("<img src='".$style."sc_nl.$type' border=0 alt='Show Entire Chapter'> ");}

if ($nc[1]!=null){echo(makeLink("lookup",$ncb["fullname"]." ".$nc[1],"Next Chapter: ".$ncb["fullname"]." ".$nc[1]));
echo("<img src='".$style."nc.$type' border=0 alt='Next Chapter'></a> ");}
else{echo("<img src='".$style."nc_nl.$type' border=0 alt='Next Chapter'> ");}

if ($nb!=null){echo(makeLink("lookup",$nb["fullname"],"Next Book: ".$nb["fullname"]));

echo("<img src='".$style."nb.$type' border=0 alt='Next Book:'></a> <br></p><br>");}
else {echo("<img src='".$style."nb_nl.$type' border=0 alt='Next Book:'> <br></p><br>");}

/*
echo("<br><p class=center>[ ");
if ($pb!=null){echo("<a href='?lookup=".$pb["fullname"]."' title='Last Book'>");}
echo("<img src='images/prevnext/standard/pb.jpg' border=0></a> ]  [ ");
if ($pc!=null){echo("<a href='?lookup=".$pc[0]." ".$pc[1]."' title='Last Chapter'>");}
echo("&lt;=</a> ]  [ ");
if ($case=="range"){echo("<a href='?lookup=".$bok["fullname"]." ".$chapter."' title='Show Entire Chapter'>");}
echo("&lt;=&gt;</a> ]  [ ");
if ($nc!=null){echo("<a href='?lookup=".$nc[0]." ".$nc[1]."' title='Next Chapter'>");}
echo("=&gt;</a> ]  [ ");
if ($nb!=null){echo("<a href='?lookup=".$nb["fullname"]."' title='Next Book'>");}
echo("=&gt;&gt;</a> ]<br></p><br>");
*/

}// end prevNext


// getLastChapter
// finds the reference of the previous chapter in the Bible
// @ param - book - the current book
// @ param - chapter - the current chapter
// @ returns - array containing new book and chapter, at index 0 and 1
// @ returns - null, if current chapter is Genesis 1

function getLastChapter($book, $chapter="",$lang=""){

if (($chapter==null)||($chapter=="")){$chapter=1;}

if ($chapter!=1){
$bo=getbook($book,$lang);
return array($bo["fullname"], $chapter-1);
}
if (($chapter == 1) && ($book == 1)){return null;}
$bo=getbook($book-1,$lang);
return array($bo["fullname"], getChapters($book-1));

}// end getLastChapter

// getNextChapter
// finds the next chapter in the Bible
// @ param - book - the current book
// @ param - chapter - the current chapter
// @ returns - array containing new book and chapter, at index 0 and 1
// @ returns - null, if current chapter is Rev 22

function getNextChapter($book, $chapter,$lang=""){
if (($book == 66) && (($chapter == 22)||($chapter==null)||($chapter==""))){return null;}
$ch=getChapters($book);
if (($chapter!=$ch)&&($chapter!=null)&&($chapter!="")){
$bo=getbook($book,$lang);
return array($bo["fullname"], $chapter+1);
}//end if
$bo=getbook($book+1,$lang);
return array($bo["fullname"], 1);

}// end getNextChapter




// getContext
// @param - book, chapter, verse
// @param - range
// @return - reference string of range of verses from +- range around verse
// does not return verses outside of book and chapter
// if no verse is given, returns a reference including the book and chapter
function getContext($book, $chapter, $verse="", $range="",$lang=""){

$bo=getBook($book,$lang);
$max=getNumVerses($book, $chapter);

if ($range==""){$range=2;}
if ($verse==""){return $bo["fullname"]." $chapter";}

$hi=$verse+$range;
if ($hi>$max){$hi=$max;}
$lo=$verse-$range;
if ($lo<1){$lo=1;}

return $bo["fullname"]." $chapter:$lo-$hi";
}// end getContext


// getContextarray
// @param - book, chapter, verse
// @param - range
// @return - reference array of range of verses from +- range around verse
// does not return verses outside of book and chapter
// if no verse is given, returns a reference including the book and chapter
function getContextarray($book, $chapter, $verse="", $range="",$lang=""){

//$bo=getBook($book,$lang);
$max=getNumVerses($book, $chapter);

if ($range==""){$range=2;}




$hi=$verse+$range;
if ($hi>$max){$hi=$max;}
$lo=$verse-$range;
if ($lo<1){$lo=1;}
$ver="$lo-$hi";
if ($verse==""){$ver="";}

$ref=array($book,$chapter,$ver);
return $ref;

//return $bo["fullname"]." $chapter:$lo-$hi";

}// end getContextarray

// getContextBook
// @param - book, chapter, verse
// @param - range
// @return - range of verses from +- range around verse
// will return verses in this range even if they are outside of chapter
// does not return verses outside of book
function getContextBook($book, $chapter, $verse, $range=2){

}// end getContextBook

// versionDisplay
// @param - ver
// @param - display (array)
// @return - none
// generates partial "GET" hyperlinks for Bible Version and display settings.
// echos it to display

function versionDisplay($ver=null, $display=null){

global $version, $disp;

if ($ver==null){$ver=$version;}
if ($display==null){$display=$disp;}

$dis=implodeDisp($display);
echo("&version=$version&display=$dis");

}// end versionDisplay

// explodeDisp
// @param - display (String)
// @return - disp (Array)
// breaks the display settings string into an associative array
function explodeDisp($display){

}// end explodeDisp

// implodeDisp
// @param - disp (Array)
// @return - display (String)
// combines the associative array disp into a display settings string 
function implodeDisp($disp){

}// end implodeDisp

// highlight
// highlights all words in $verse that match $search
// @param - verse - array of verses
// @param - search - array of search terms
// @returns - verse - array of verses with highlighting
function highlight($verses, $search){

global $wholeword, $searchtype;

//foreach($search as $va){echo("$va<br>");}

//echo("highlight search<br>");

$sa=$search;
$search=array();
foreach($sa as $term){

if ((strlen($term)>1)&&($term!="PROX")){$search[]=$term;}

}

//print_out($search);

$search=str_replace("_"," ",$search);
$st=$searchtype;


// requires PHP 4.0.5
$o=0;
while($verses[$o]!=""){
$in=0;
while($se=$search[$in]){

$verses[$o]["text"]=" ".$verses[$o]["text"];

if (strpos($se," ")!==false){$st="Exact Phrase";}
else{$st=$searchtype;}
if($st=="regexp"){$st="Exact Phrase";}

//echo($se." $st<br>");

//whole words only
if (($wholeword)&&($st!="Exact Phrase")&&(strpos($se,"%")!==0)&&(strpos($se,"%")!==strlen($se)-1)){
//$se=" $se";

$Se=ucfirst($se);

//echo ("am here<br");
$sea=array(" $se ", " $se."," $se,"," $se;"," $se:","\"$Se\"","\"$Se "," $se\"","\'$se "," $se\'"," $se?"," $se!", " $se'","“$se","</span>$se ", " $se<span class=hl>");

//print_out($sea);

//,,``“

$wh=0;
while($ser=$sea[$wh]){
//echo("|$se| ");
$verses[$o]=highlightVerse($verses[$o],$ser);

$wh++;
}// end while

}// end if
else {
// partial words allowable

//$se=trim(str_replace("%","",$se));
if($st=="Exact Phrase"){
// exact phrase searches
$text=$verses[$o]["text"];
$text_lo=strtolower($text);
$search_lo=str_replace("%","".""."",strtolower($se));

$i=strpos($text_lo, $search_lo);
if ($i===false){
//echo("Exact search error bible_display.php 500<br>text_lo: $text_lo<br>search_lo: $search_lo");

}// end if
else{
$sear=substr($text,$i,strlen($se));

$verses[$o]=highlightVerse($verses[$o],$sear);
}// end else

}// end if
else{
// all others

$verses[$o]=highlightVerse($verses[$o],$se);

}// end inner else

}// end else (partial words)

$in++;
}// end while
$o++;
}// end while

return $verses;
}// end highlight

// highlightVerse
// highlights all instances of ser in ver, maintaining the original case
// @param vers - verse to search (accociative array)
// @param se - string to search for
// @param rep - replacement
// @return vers - the highlighted verse

function highlightVerse($vers,$se){

$ver=$vers["text"];
$se_se=strtolower($se);
$SE_SE=strtoupper($se);
$Se_se=ucfirst($se);
$Se_Se=ucwords($se);

$rere=genRep($se);
$re_re=genRep($se_se);
$RE_RE=genRep($SE_SE);
$Re_re=genRep($Se_se);
$Re_Re=genRep($Se_Se);

// improve this!!!!
$rere=str_replace("%","",$rere);
$se_se=str_replace("%","",$se_se);
$SE_SE=str_replace("%","",$SE_SE);
$Se_se=str_replace("%","",$Se_se);
$Se_Se=str_replace("%","",$Se_Se);


$ver=str_replace($se, $rere, $ver);
$ver=str_replace($se_se, $re_re, $ver);
$ver=str_replace($SE_SE, $RE_RE, $ver);
$ver=str_replace($Se_se, $Re_re, $ver);
$ver=str_replace($Se_Se, $Re_Re, $ver);


/*
// This doesn't work!
$ver=ereg_replace($se, $rere, $ver);
$ver=ereg_replace($se_se, $re_re, $ver);
$ver=ereg_replace($SE_SE, $RE_RE, $ver);
$ver=ereg_replace($Se_se, $Re_re, $ver);
$ver=ereg_replace($Se_Se, $Re_Re, $ver);
*/

$ver=unhighlightpunctuation($ver);

$vers["text"]=$ver;
return $vers;
}// end highlightVerse

// explodeSearch
// separates the Search into separate search terms
// also removes all logical operators and parenthenses from boolean search
// used to generate search array for highlighting search terms
// @ param $search - search query
// @ param $type - search type (all, any, phrase, boolean)
// @ returns $search_array - array of all search terms

function explodeSearch($search, $type=""){

global $searchtype;
if ($type==""){$type=$searchtype;}


if (($type=="Any Word")||($type=="All Words")){return explode(" ", $search);}

if ($type=="Exact Phrase"){
// revise this "exact phrase" in future versions!!!!!
if (substr(phpversion(),0,1)>=5){
//$search2=str_ireplace("Lord", "LORD", $search);
$se=array($search);//array($search,$search2);
return $se;
}// end special if
return array($search);
}// end if - exact phrase

// Advanced (Boolean) Searches
$array=splitSearch($search);
$array=str_replace("\\\"","",$array);
//$array=str_replace("%","",$array);
return $array;

}// end explodeSearch

// unhighlightpunctuation
// removes any highlighting that may have encluded punctuation
// @param $ver - verse to check
function unhighlightpunctuation($ver){

$punct = array(".",",","\"","'",";",":"," ","?","!");

$comp=phpcompare(phpversion(),"5.0.0");

$in=0;
while($pu=$punct[$in]){

if ($comp!=-1){
$ver=str_ireplace("$pu</span>","</span>$pu",$ver);
$ver=str_ireplace("<span class=hl>$pu","$pu<span class=hl>",$ver);
}// end if
else{
$ver=str_replace("$pu</span>","</span>$pu",$ver);
$ver=str_replace("$pu</SPAN>","</SPAN>$pu",$ver);
$ver=str_replace("$pu</Span>","</Span>$pu",$ver);
$ver=str_replace("<span class=hl>$pu","$pu<span class=hl>",$ver);
//$ver=str_replace("<span class=hl>$pu","$pu<span class=hl>",$ver);
//$ver=str_replace("<span class=hl>$pu","$pu<span class=hl>",$ver);
}// end else
$in++;
}// end while
return $ver;
}// end unhighlightpunctuation

// generates the replacement string for the highlighter
function genRep($ser){

global $wholeword;
$ser=ltrim($ser);
$rep="<span class=hl>$ser</span>";

if (($wholeword)&&(strpos($ser,"%")!==0)&&(strpos($ser,"%")!==strlen($ser)-1)){
$rep=" $rep";}

return str_replace("%","",$rep);
}// end genRep

//pageRange
// calculates the min/max verse indices for paginated searches
function pageRange($num_ver,$page_num){

global $verses_per_page;
$min=($page_num-1)*$verses_per_page;
$max=($page_num)*$verses_per_page-1;

$limit=$num_ver-1;
if (($min>$limit)){return false;}

if (($max>$limit)||($page_num==="all")){$max=$limit;}
if ($page_num==="all"){$min=0;}

//echo("max $max min $min ");

return array("max"=>$max,"min"=>$min);

}// end page Range

function paginationIndex($num_ver,$page_num){

global $verses_per_page;

$index="<div class=page>";

$num_page=ceil($num_ver/$verses_per_page);

if ($page_num!=1){$index.=pageLink($page_num-1);}
$index.="&lt;&lt;&lt;</a>";

$i=1;
while($i<=$num_page){

$index.=" &nbsp; ";
if($i!=$page_num){$index.=pageLink($i);}
else{$index.="<span class=current_page>";}
$index.="$i";

if($i!=$page_num){$index.="</a>";}
else{$index.="</span>";}
$index.=" ";


$i+=1;
}//end while

$index.=" &nbsp; ";
if ($page_num!=$num_page){$index.=pageLink($page_num+1);}
$index.="&gt;&gt;&gt;</a></div>";

return $index;

}// end paginationIndex

function pageLink($page_num){

global $page;


$title="Page $page_num";
if($page_num=="one"){
$title="Paginate results";
$page_num=1;
}
if($page_num=="all"){$title="Show all results";}


return makeLink("page",$page_num,$title,"page");

/*$link="?".$_SERVER["QUERY_STRING"];//."&page=$page_num";

$link2=str_replace("page=$page","page=$page_num",$link);

if($link2==$link){$link2.="&page=$page_num";}

//echo();

return "<a href='$link2' title='Page $page_num' class=page>";
*/
}// end paginationLink


// sees if two references are equal
// @param, verse1, verse2 - verse arrays
function compareRef($verse1, $verse2){

if(($verse1['book']==$verse2['book'])&&($verse1['chapter']==$verse2['chapter'])&& ($verse1['verse']==$verse2['verse'])){
return true;
}
else{return false;}


}//

function isRTL($language){

if(($language=="ar")||($language=="he")||($language=="dv")||($language=="fa")||($language=="ha")||($language=="ps")||($language=="ur")||($language=="yi")){return true;}
else{return false;}

}//

function fontsize(){

global $style, $size;

if ($size=="large"){$font=1;}
if ($size=="med"){$font=0;}
if ($size=="small"){$font=-1;}

if ($style=="monospace"){$font+=2;}

$font=100+20*$font;

return "$font";

}//

?>

<!--
<script>

// ifie
// writes text if the user's browser is Internet Explorer
// Used for browser-specific formatting
// @ param - text text to write
// @ returns - none

function ifie(text){
nav=navigator.appName;
if ((nav=="Microsoft Internet Explorer")||(nav=="MSIE")){
document.write(text);
}// end if

}// end ifie


// last
// restores the values of the last lookup/search into the form
// @ param look - reference
// @ param search - search query
// @ param type - search type
// @ param wholeword - true if whole word search is selected
// @ returns none

function last(look, search, type, wholeword){

// me is the NAME of the form
me.lookup.value=look;
me.search.value=search;
//me.searchtype.value=type;
//me.wholeword.value=wholeword;

}// end last

// clearForm
// Clears the form
// @ param - none
// @ returns - none

function clearForm(){

me.lookup.value="";
me.search.value="";
}// end clearForm

// lowres
// indicates if the screen resolution is less than 1024x768
// Used for formatting
// @ param - none
// @ return - true if below 1024x768, false otherwise
function lowres(){
if (screen.width < 1024){return true;}
else{return false;}
}

</script>
-->
