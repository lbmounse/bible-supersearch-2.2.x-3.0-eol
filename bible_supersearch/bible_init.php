<?php
// SECURE

// Bible SuperSearch Version 2.1
// The free, open source, PHP and MySQL web-based   
// Bible Reference retrevial and search utility
//
// Copyright (C) 2006-2009 Luke Mounsey
// www.BibleSuperSearch.com
// www.Alive-in-Christ.com
//
// bible_init.php
// Extracts the submitted search data and runs any other scripts that need
// to be executed at the BEGINNING of the Bible SuperSearch display page.
// This includes the "You requested... Edit this search" functionality
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License included in the file 
// "license.txt" for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// File Version
$bible_init_version=1.6;
// Bible SuperSearch Short Version
// Included in this file for convenience
require_once("bible_system.php");
require_once("bible_config.php");
// Official Version is in "bible_lookup.php"
$bss_version_short=$prod_version_short;

//error_reporting(E_ALL);
//get submitted data
if ($_POST!=null){$sub=$_POST;}
else {$sub=$HTTP_POST_VARS;}

if ($sub==null){

if ($_GET!=null){$sub=$_GET;}
else {$sub=$HTTP_GET_VARS;}

}// end if

require_once("bible_misc.php");

$var_list = array ('lookup','search','searchtype','submit','wholeword','version','version1','version2','version3','version4','within','old_search','old_searchtype','old_lookup','chap2chap','chapter1','verse1','chapter2','verse2','any','all','one','none','phrase','proxrange','prox','interface','display','entry','wholebook','language','page','table_display','style','size','lookup2','lookup3');

//error_reporting(E_ALL);


foreach($var_list as $var) {
	$$var = sanitize($sub[$var]);
}

/*

// retrieve the form variables from $sub
$lookup=$sub["lookup"];
$search=$sub["search"];
$searchtype=$sub["searchtype"];
$submit=$sub["submit"];

$wholeword=$sub["wholeword"];
$version=$sub["version"];
$version1=$sub["version1"];
$version2=$sub["version2"];
$version3=$sub["version3"];
$version4=$sub["version4"];
// -->> $webservice_default_version=$sub["webservice_default_ver"];
$within=$sub["within"];
$old_search=$sub["old_search"];
$old_searchtype=$sub["old_searchtype"];
$old_lookup=$sub["old_lookup"];
$chap2chap=$sub["chap2chap"];
$chapter1=$sub["chapter1"];
$verse1=$sub["verse1"];
$chapter2=$sub["chapter2"];
$verse2=$sub["verse2"];
$any=stripextraspaces($sub["any"]);
$all=$sub["all"];
$one=stripextraspaces($sub["one"]);
$none=$sub["none"];
$phrase=$sub["phrase"];
$proxrange=$sub["proxrange"];
$prox=$sub["prox"];
$interface=$sub["interface"];
$display=$sub["display"];
$entry=$sub["entry"];
$wholebook=$sub["wholebook"];
$language=$sub["language"];
$page=$sub["page"];
$table_display=$sub["table_display"];
$style=$sub["style"];
$size=$sub["size"];
$lookup2=$sub["lookup2"];
$lookup3=$sub["lookup3"];

*/


//echo("lookup $lookup lookup2 $lookup2 lookup3 $lookup3 search $search<br><BR>");

//echo($searchtype);

if ((empty($submit))&&(!empty($prox))){
$search=$prox;
$submit=true;
$searchtype="proximity";
}

if (empty($lookup)){$lookup=$sub["lookup2"];}
if ((empty($lookup))&&($sub["lookup3"]!="New Testament")){$lookup=$sub["lookup3"];}

if ((empty($lookup))&(!empty($chap2chap))){
$lookup="$chap2chap $chapter1";
if((!empty($verse1))&(!empty($chapter1))){$lookup.=":$verse1";
if(!empty($chapter2)){$lookup.="-$chapter2";}
if (empty($search)){
$search=$sub["search2"];
if(!empty($verse2)){$lookup.=":$verse2";}
}// end if
}// end if
}// end if

if (empty($search)){

if(!empty($all)){$search="($all) & ";}
if(!empty($any)){$search.="(".str_replace(" ","|",$any).") & ";}
if(!empty($one)){$search.="(".str_replace(" ","^",$one).") & ";}
if(!empty($none)){$search.="-($none) & ";}
if(!empty($phrase)){$search.="(\\\"$phrase\\\") & ";}

//echo($search);

$search=substr($search,0,strlen($search)-3);

$search_noquoteexcape=str_replace("\\","",$search);

//echo("search |$search|<br>");

}// end if

//echo("search |$search|<br>");

if(strtolower($lookup)=="chapter"){$lookup="Random Chapter";}
if(strtolower($lookup)=="verse"){$lookup="Random Verse";}

$lookup=trim($lookup);
$lookup=trim($lookup,";");

require_once("bible_mysql.php");
connect();



//$lookup=random_lookup($lookup);

// default values
//$wholeword=defaultValue($wholeword, "Whole Words Only.");
$display=defaultValue($display, "default");
$searchtype=defaultValue($searchtype, "All Words");
$style=defaultValue($style,"sans-serif");
$size=defaultValue($size,"med");


$wholebook=defaultValue($wholebook, "false");
$language=defaultValue($language,getLanguage($version));
$page=defaultValue($page,1);
$table_display=defaultValue($table_display,"true");
$proxrange=defaultValue($proxrange,5);


// Parallel Bible versions, default 
if($version==""){$version=str_replace(array(",,,",",,"),",","$version1,$version2,$version3,$version4");}

$version=trim($version);
if($version[0]==","){$version=substr($version,1);}
$len=strlen($version);
if($version[$len-1]==","){$version=substr($version,0,$len-1);}
if($version==""){$version1=$default_bible_version;}
$version=defaultValue($version, "kjv");
//echo($version);

$version_array=explode(",",$version);
$default_search_bible_version=$version_array[0];

if($table_display=="true"){$table_display=true;}
else{$table_display=false;}


// named constants
$default_language="en";
$default_version="kjv";
// semi-default language
$va=explode(",",$version);
if(count($va)!=1){$lang="";}
else{$lang=getLanguage($va[0]);}




// THIS CONTROLS WHERE THE FORM LINKS TO
// IN MOST CASES YOU DO NOT NEED TO MODIFY IF YOU ARE 
// RUNNING BIBLE SUPERSEARCH INDEPENDENTLY ON YOUR WEBSITE
// Gets url for form submission
// This insures that the form will submit properly even if the 
// user changes the filename from "bible_supersearch.php"

if ($HTTP_SERVER_VARS!=null){$url=$HTTP_SERVER_VARS['PHP_SELF'];}
else {$url=$_SERVER['PHP_SELF'];}

//$url = $_SERVER['REQUEST_URI'];
$que_pos = strpos($url, '?');
if($que_pos !== FALSE) {
	//$url = substr($url, 0, $que_pos -1);
}

$page_link=$url."?".$_SERVER["QUERY_STRING"];

// Uncomment the following code ONLY if you need to link this form
// to another page other than itself.  Edit the url to meet this need.
// $url="http://www.biblesupersearch.com";


// defaultValue
// sets default value if variable is empty
// @ param variable - the variable
// @ param default - the default value
// @ return value - the value of the variable
//			Equals variable if variable is not empty
//			Equals defalut if variable is empty
function defaultValue($variable, $default){

if (empty($variable)){return $default;}
else {return $variable;} 

}// end defaultValue

// youRequested
// displays entered lookup and search querys
// gives option of editing (repeating) last search
// @ param - globals lookup and search
// @ returns - none

function youRequested(){
global $lookup,$search,$searchtype,$wholeword,$within;

//echo("search [$search]");

$search_noquoteexcape=str_replace("\\","",$search);

if($within=="true"){$wh=" Previous search searched for";}

if(!empty($lookup)){$exp="<b>$lookup</b>";

if(!empty($search)){


$exp.="$wh searched for <b>$search_noquoteexcape</b>";

}
}//end if
elseif(!empty($search)){$exp="$wh<b> $search_noquoteexcape</b>";}

if (!empty($exp)){
return"<div style=\"left:auto; right:auto; width:auto;\"><div class=request>You requested: <a href='javascript:last(\"$lookup\", \"$search\", \"$searchtype\", \"$wholeword\")' title='Edit this request' class=request>$exp</a></div></div>";}
}//end youRequested

// editSearch
// provides a html button that reinserts their last search into the form
// @ param - none (uses global variables)
// @ returns - none
function editSearch($value="edit search"){

global $lookup,$search,$searchtype,$wholeword, $submit;

$onclick="last(\"$lookup\", \"$search\", \"$searchtype\", \"$wholeword\")";

return"<input class=\"button button_med\" type=\"button\" value=\"$value\" onclick='$onclick'>";

}// end editSearch

// searchOptions
// creates a dropdown menu with the search options
// @param select - item to select
// @return - none

function searchOptions($select="All Words"){

$text='<select name="searchtype" class=searchtype>';


$options=array("All Words","Any Word", "Exact Phrase","Words Within 5 Verses","Words Within Same Chapter","Boolean Expression","Regular Expression");
$value=array("All Words","Any Word", "Exact Phrase","proximity","chapter","Boolean Search","regexp");

$i=0;
while($op=$options[$i]){

$text.="<option value=\"".$value[$i]."\"";
if ($value[$i]==$select){$text.=" selected>";}
else{$text.=">";}
$text.=" $op </option>";

$i++;
}// end while
$text.="</select>";

return $text;
}// end searchOptions





function bookdropdown($testament="all"){


$text='';
// generates drop down list using PHP and MySQL
// don't use exept for generating the HTML for a list
/*
if ($testament=="ot"){
$res=mysql_query("SELECT * FROM `bible_books_$language` ORDER BY `number` LIMIT 39;");}

if ($testament=="nt"){
$res=mysql_query("SELECT * FROM `bible_books_$language` ORDER BY `number` LIMIT 39,27;");}

if ($testament=="all"){$res=mysql_query("SELECT * FROM `bible_books` ORDER BY `number`;");}
*/

if ($testament=="nt"){$lk="3";}
else{$lk="2";}
$text.="<select name=\"lookup".$lk."\" class='book_list'>";

if ($testament=="nt"){$text.="<option value=''> New Testament </option>";}
else {$text.="<option value=''> Old Testament </option>";

$text.=otDropDown();
}// end else

if ($testament!="ot"){

if ($testament=="all"){$text.="<option value=''> New Testament </option>";}

$text.=ntDropDown();



}// end if

// generates drop down list using PHP and MySQL
// don't use exept for generating the HTML for a list
/*
$i=0;
while ($b=mysql_fetch_array($res)){

if ($i==39){echo("<option value=''> New Testament </option>");}

echo("<option value=\"".$b["fullname"]."\"> ".$b["fullname"]." </option>");

$i++;
}// end while
*/

$text.="</select>";
return $text;

}// end NTdropdown

// produces HTML code for OT drop down menu
function otDropDown(){

return'
<option> Genesis </option><option> Exodus </option><option> Leviticus </option><option> Numbers </option><option> Deuteronomy </option><option> Joshua </option><option> Judges </option><option> Ruth </option><option> 1 Samuel </option><option> 2 Samuel </option><option> 1 Kings </option><option> 2 Kings </option><option> 1 Chronicles </option><option> 2 Chronicles </option><option> Ezra </option><option> Nehemiah </option><option> Esther </option><option> Job </option><option> Psalms </option><option> Proverbs </option><option> Ecclesiastes </option><option> Song of Solomon </option><option> Isaiah </option><option> Jeremiah </option><option> Lamentations </option><option> Ezekiel </option><option> Daniel </option><option> Hosea </option><option> Joel </option><option> Amos </option><option> Obadiah </option><option> Jonah </option><option> Micah </option><option> Nahum </option><option> Habakkuk </option><option> Zephaniah </option><option> Haggai </option><option> Zechariah </option><option> Malachi </option>
';

}// end otDropDown

// produces HTML code for NT drop down menu
function ntDropDown(){

return'

<option> Matthew </option><option> Mark </option><option> Luke </option><option> John </option><option> Acts </option><option> Romans </option><option> 1 Corinthians </option><option value="2 Corinthians"> 2 Corinthians </option><option value="Galatians"> Galatians </option><option value="Ephesians"> Ephesians </option><option value="Philippians"> Philippians </option><option value="Colossians"> Colossians </option><option value="1 Thessalonians"> 1 Thessalonians </option><option value="2 Thessalonians"> 2 Thessalonians </option><option value="1 Timothy"> 1 Timothy </option><option value="2 Timothy"> 2 Timothy </option><option value="Titus"> Titus </option><option value="Philemon"> Philemon </option><option value="Hebrews"> Hebrews </option><option value="James"> James </option><option value="1 Peter"> 1 Peter </option><option value="2 Peter"> 2 Peter </option><option value="1 John"> 1 John </option><option value="2 John"> 2 John </option><option value="3 John"> 3 John </option><option value="Jude"> Jude </option><option value="Revelation"> Revelation </option>

';

}//

// makes HTML for formatting buttons

function formattingButtons(){

global $table_display, $search, $searchtype,$lookup,$wholeword,$formatting_buttons,$interface,$default_interface;


$buttons="<div class=center>";

$dir= "bible_images/formatting/$formatting_buttons";
$type=imagetype($dir);

//$buttons.="</a>";

$buttons.=makeLink("size","large","Large","")."<img src='$dir/large.$type' border=0 ></a> ";
$buttons.=makeLink("size","med","Medium","")."<img src='$dir/med.$type' border=0></a> ";
$buttons.=makeLink("size","small","Small","")."<img src='$dir/small.$type' border=0></a> ";
//$buttons.=makeButtons("med.jpg","","Medium Text");
//$buttons.=makeButtons("small.jpg","","Small Text");
$buttons.=makeLink("style","serif","Serif","")."<img src='$dir/serif.$type' border=0></a> ";
$buttons.=makeLink("style","sans-serif","Sans Serif","")."<img src='$dir/sans-serif.$type' border=0></a> ";
$buttons.=makeLink("style","monospace","Monospace","")."<img src='$dir/monospace.$type' border=0></a> ";
/*
$buttons.=makeButtons("serif.jpg","","Serif");
$buttons.=makeButtons("sans-serif.jpg","","Sans-Serif");
$buttons.=makeButtons("monospace.jpg","","Monospace");
//$buttons.=makeButtons("ez-copy.jpg","");
*/
if($table_display){$buttons.=makeLink("table_display","false","Enable Easy Copy","forma f-style")."<img src='$dir/ez-copy-on.$type' border=0></a>
";}
else{$buttons.=makeLink("table_display","true","Disable Easy Copy","forma f-style")."<img src='$dir/ez-copy-off.$type' border=0></a>
";}

$buttons.=makeButtons("$formatting_buttons/clear_form.$type","javascript:clearForm()","Clear form");
$buttons.=makeButtons("$formatting_buttons/edit_search.$type","javascript:last(\"$lookup\", \"$search\", \"$searchtype\", \"$wholeword\")","Edit last search");
//$buttons.=makeButtons("search_within.jpg","","Search within last search");
$buttons.=makeButtons("$formatting_buttons/help.$type","bible_supersearch_user_guide.html","Help","_new");

if (substr($default_interface,0,8)!="advanced"){

if($interface=="advanced"){
$im="basic.$type";
$link="";
$text="Basic Search";
}
else{

$im="advanced.$type";
$link="advanced";
$text="Advanced Search";

}

$buttons.=makeLink("interface",$link,$text,"")."<img src='$dir/$im' border=0></a> ";
}// end advanced

$buttons.="</div><br>";

return $buttons;
//return $buttons;

}// end formattingButtons

function makeButtons($file,$link, $desc="",$target=""){

return "<a href='$link' title='$desc' target='$target'><img src='bible_images/formatting/$file' border=0 alt='$desc'></a>
";

}

function formMemory(){

global $table_display, $interface,$style,$size,$default_language,$search,$searchtype;

//echo("deflang $default_language");

if($table_display){$td="true";}
else{$td="false";}

return"<input type='hidden' name='table_display' value='$td'>
<input type='hidden' name='style' value='$style'>
<input type='hidden' name='size' value='$size'>
<input type='hidden' name='interface' value='$interface'>
<input type='hidden' name='old_search' value='$search'>
<input type='hidden' name='old_searchtype' value='$searchtype'>
<input type='hidden' name='old_lookup' value='$lookup'>
";

}//

function limitSearch($ref_name="Above Passage(s)",$lst=false){
$text="";
if($lst){$text.="<option value=''> Limit Search to </option>";}
$text.="
<option value='' selected> Entire Bible </option>
<option value=''> $ref_name </option>
";

$res=mysql_query("select * from `bible_shortcuts_en` where `display` = 'yes'");
echo(mysql_error());
while($array = mysql_fetch_array($res)){

$text.="<option value='".$array['reference']."'> ".$array['name']." </option>
"; 

}// end while

return $text;

}// end limitsearch

// displays a tip about searching within specified passages
function searchTip(){

global $submit;

if($submit!="true"){$text="<br><br><small>Tip: Entering both a passage and a search query will result in the limitation of the search to the specified passage(s).</small>";}
else{$text="";}

return $text;

}

function randomMenu(){

$text="
<select name='lookup2'>
<option value=''> Random </option>
<option value='Random Chapter'> Chapter </option>
<option value='Random Verse'> Verse </option>
</select>

";
return $text;
}

function advancedsearchbible(){

global $version1, $version2, $version3, $version4, $webservice_default_version;

$res=mysql_query("select count(*) from `bible_versions`");
$numbib=mysql_fetch_array($res);
$numbib=$numbib["count(*)"];

//echo($numbib);

$text=bibleversions($version1,1);

if ($webservice_default_version==""){

	if ($numbib>1){$text.=" &nbsp; &nbsp; ".bibleversions($version2,2)."<br>";}
	if ($numbib>2) {$text.=bibleversions($version3,3);}
	if ($numbib>3){$text.=" &nbsp; &nbsp; ".bibleversions($version4,4);}

}

$text.="<br>";

return $text;
}

?>