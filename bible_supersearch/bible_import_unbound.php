<?php
// SECURE

ini_set("memory_limit","20000000");

// Single-File Bible Convertor
// "Unbound"
//
// Converts Bibles in a single file (as opposed to one book/file) to MySQL for use on Bible SuperSearch.
// will convert single-file Bibles in both formats avaliable on http://unbound.biola.edu/

// Files can be in one of the following two formats
// book_index<tab>chapter<tab>verse<tab>text
// EX: 01	1	1	In the beginning God created the heaven and the earth.
// or
// book_index<tab>chapter<tab>verse<tab><tab>subverse(ignored)<tab>text
// Ex: 01	1	1		10	In the beginning God created the heaven and the earth.
//
// Where book_index is the number of the book, Genesis = 1, Revelation = 66.   Subverse is ignored.
//
// Converting from Unbound Bible format.
// Many Bibles in these formats are avaiable for download on "The Unbound Bible" download site.
// You can use either the old or the new format.
// http://unbound.biola.edu/
// To use one of these Bibles, download the compressed file, and extract the files
// <bible>_utf8.txt and <bible>.html, where <bible> is the name of the Bible. 
// Modify the options below, and upload this file, and the two extracted files to your webserver.
// Open this file "bible_add_unbound.php" in a web browser to execute the script.
// When import is complete, test the file.
// For details, please see the "General Bible Importation Instructions"

// script options

$dir=""; // directory of Bible files
$file="kjv_apocrypha";//File name, minus "_utf8.txt" example: "kjv_utft8.tx" => enter "kjv"
//$shortname="";
$bible="kjv"; // short name or abbreviation
$name="Authorized King James Version"; // full name, to appear in the Bible version menu
$language="en"; // 2 character language code
$language_long="English";  // full English name of the language 
//Which testaments does this Bible contain: ot,nt,both
$testaments="both"; 
// Where did you get this Bible?   
$source="This Bible imported from The Unbound Bible <a href='http://unbound.biola.edu/'>http://unbound.biola.edu/</a>";

// install book list into table
$book_list=false;
// import the Bible into database
$install_bible=false;
// make an entry into the Bible versions table for this Bible
$insert_into_bible_table=true;

// end options

require_once("bible_mysql.php");
connect();

// book list
if($book_list){


// extract book list

$su="DROP TABLE IF EXISTS `bible_books_$language`;";
$su2="CREATE TABLE `bible_books_$language` (
  `number` int(11) NOT NULL auto_increment,
  `fullname` tinytext NOT NULL,
  `short` tinytext NULL,
  `chapters` int(11) NOT NULL,
  PRIMARY KEY  (`number`)
) ENGINE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1 ;";



mysql_query($su);
echo(mysql_error());
mysql_query($su2);
echo(mysql_error());

if(!isfile($language.".txt")){
echo("Note: book list for this language is not avaliable.<br><BR>");
break 2;
}// end if

$list=file($language.".txt");

if($testaments=="nt"){$st=40;}
else{$st=1;}

foreach($list as $book){

$bo=explode("	",$book);
$book=mb_convert_case(mb_strtolower($bo[0],"utf-8"),MB_CASE_TITLE,"utf-8");

echo (" &nbsp; $book #$st<br>");
$qu="INSERT INTO `bible_books_$language` ( `number` , `fullname` , `short` , `chapters` ) VALUES (NULL , '$book', '', ''
);";

mysql_query($qu);
echo(mysql_error());

$st+=1;
}// end foreach

$res=mysql_query("select * from `bible_books_$language");
while($b=mysql_fetch_array($res)){
echo($b["fullname"]."<br>");
}// end while

}//end if

if($install_bible){

$su="DROP TABLE IF EXISTS `bible_$bible`;";
$su2="CREATE TABLE `bible_$bible` (
  `index` int(11) NOT NULL auto_increment,
  `book` int(2) NOT NULL default '0',
  `chapter` int(3) NOT NULL default '0',
  `verse` int(3) NOT NULL default '0',
  `text` text NOT NULL,
  KEY `index` (`index`)
) ENGINE=MyISAM PACK_KEYS=0;";

mysql_query($su);
echo(mysql_error());
mysql_query($su2);
echo(mysql_error());

$loc=$file."_utf8.txt";
if($dir!=""){$loc="$dir/$loc";}

$bib=file($loc);

//$bib=explode("\n",$bib);

$i=7;
$ver=explode("	",$bib[10]);
if($ver[3]==""){$t=5;}
else{$t=3;}

while($ver=$bib[$i]){

$ver=explode("	",$ver);

$book=substr($ver[0],0,2);
if ($book>66){break;}// omit any heretical books
$chapter=$ver[1];
$verse=$ver[2];
$text=str_replace("'","\'",$ver[$t]);

$qu="insert into `bible_$bible` values(NULL, '$book', '$chapter', '$verse', '$text')";
mysql_query($qu);

//echo(mysql_error()." $book $chapter:$verse<br>");

$i++;

//while(substr($bib[$i],0,1)=="#"){$i++;}

//if($i==21){break;}

}// end while

}// end if

// add to Bible version table

if(is_file($file.".html")){$desc=file($file.".html");}
else{echo("The file ".$file.".html is not available. &nbsp; Create this file with a short HTML description of this Bible, or edit the `decription` feild for it in the MySQL table `bible_versions`<br><BR>");}


$description=implode("<br>",$desc)."<br><br>".$source;
$description=str_replace("'","\'",$description);

$q1="delete from `bible_versions` where `shortname` = '$bible'";
mysql_query($q1);

$qu="insert into `bible_versions` values(NULL, '$bible', '$name', '$description', '$language_long','$language')";
mysql_query($qu);
echo(mysql_error());

echo("done");

?>
