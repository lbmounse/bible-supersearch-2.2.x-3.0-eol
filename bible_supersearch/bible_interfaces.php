<?php
// SECURE
// Bible SuperSearch Version 2.0.80
// The free, open source, PHP and MySQL web-based   
// Bible Reference retrevial and search utility
//
// Copyright (C) 2006, 2007, 2008 Luke Mounsey
// www.BibleSuperSearch.com
// www.PreservedWord.com
//
// bible_interfaces.php
// Contains functions used by all interfaces
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License included in the file 
// "license.txt" for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Init

// run initial scripts
//require("bible_init.php");




// header text

?>


<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Bible SuperSearch <?php echo("$bss_version_short $lookup  $search");?></title>
<style type="text/css">
@import url('interfaces/bible_stylesheet.css');
@import url('interfaces/bible_stylesheet_<?php echo($interface); ?>.css');
</style>
<meta name="description" content="KJV Bible SuperSearch">
<meta name="keywords" content="">
<meta name="author" content="aliveinchrist.us">
</head>
<!-- Import JavaScript functions -->
<script src="bible_javascript.js"></script>
<script>
	function notifyParent() {
		
	}
	

</script>
<body onload='notifyParent()'>


<center>


<?php

// display form

echo($interface_text);

global $style,$size;

//echo('<div class="width" style="margin-left:auto;margin-right:auto;margin-top:0;font-family:$style">');

echo(formattingButtons());



// Send the search to the PHP scripts.
require_once("bible_main.php");


if ($enable_booklinks=="on"){booklink();}

function booklink(){

?>
<!-- Bible Book List -->

<script>
// this helps format the fancy booklist
document.write("<div class=booklist ");
if (lowres()){
document.write("style='font-size:80%'");}
document.write(">");
</script>
<noscript>
<div class=booklist>
</noscript>

<div class=testamentheader>Old Testament</div>

<script>
if (lowres()){wid=500;}
else{wid=710;}
ifie("<div style='width:"+wid+"'>");
</script><span class=booklink><nobr><a href='?lookup=Genesis'>Genesis</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Exodus'>Exodus</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Leviticus'>Leviticus</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Numbers'>Numbers</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Deuteronomy'>Deuteronomy</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Joshua'>Joshua</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Judges'>Judges</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Ruth'>Ruth</a></nobr></span> <span class=booklink><nobr><a href='?lookup=1 Samuel'>1 Samuel</a></nobr></span> <span class=booklink><nobr><a href='?lookup=2 Samuel'>2 Samuel</a></nobr></span> <span class=booklink><nobr><a href='?lookup=1 Kings'>1 Kings</a></nobr></span> <span class=booklink><nobr><a href='?lookup=2 Kings'>2 Kings</a></nobr></span> <span class=booklink><nobr><a href='?lookup=1 Chronicles'>1 Chronicles</a></nobr></span> <span class=booklink><nobr><a href='?lookup=2 Chronicles'>2 Chronicles</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Ezra'>Ezra</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Nehemiah'>Nehemiah</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Esther'>Esther</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Job'>Job</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Psalms'>Psalms</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Proverbs'>Proverbs</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Ecclesiastes'>Ecclesiastes</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Song of Solomon'>Song of Solomon</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Isaiah'>Isaiah</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Jeremiah'>Jeremiah</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Lamentations'>Lamentations</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Ezekiel'>Ezekiel</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Daniel'>Daniel</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Hosea'>Hosea</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Joel'>Joel</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Amos'>Amos</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Obadiah'>Obadiah</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Jonah'>Jonah</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Micah'>Micah</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Nahum'>Nahum</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Habakkuk'>Habakkuk</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Zephaniah'>Zephaniah</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Haggai'>Haggai</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Zechariah'>Zechariah</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Malachi'>Malachi</a></nobr></span> 

<script>
ifie("</div>");
</script>
<div class=testamentheader>New Testament</div>
<script>
if (lowres()){wid=500;}
else{wid=688;}
ifie("<div style='width:"+wid+"'>");
</script>
<span class=booklink><nobr><a href='?lookup=Matthew'>Matthew</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Mark'>Mark</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Luke'>Luke</a></nobr></span> <span class=booklink><nobr><a href='?lookup=John'>John</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Acts'>Acts</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Romans'>Romans</a></nobr></span> <span class=booklink><nobr><a href='?lookup=1 Corinthians'>1 Corinthians</a></nobr></span> <span class=booklink><nobr><a href='?lookup=2 Corinthians'>2 Corinthians</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Galatians'>Galatians</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Ephesians'>Ephesians</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Philippians'>Philippians</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Colossians'>Colossians</a></nobr></span> <span class=booklink><nobr><a href='?lookup=1 Thessalonians'>1 Thessalonians</a></nobr></span> <span class=booklink><nobr><a href='?lookup=2 Thessalonians'>2 Thessalonians</a></nobr></span> <span class=booklink><nobr><a href='?lookup=1 Timothy'>1 Timothy</a></nobr></span> <span class=booklink><nobr><a href='?lookup=2 Timothy'>2 Timothy</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Titus'>Titus</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Philemon'>Philemon</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Hebrews'>Hebrews</a></nobr></span> <span class=booklink><nobr><a href='?lookup=James'>James</a></nobr></span> <span class=booklink><nobr><a href='?lookup=1 Peter'>1 Peter</a></nobr></span> <span class=booklink><nobr><a href='?lookup=2 Peter'>2 Peter</a></nobr></span> <span class=booklink><nobr><a href='?lookup=1 John'>1 John</a></nobr></span> <span class=booklink><nobr><a href='?lookup=2 John'>2 John</a></nobr></span> <span class=booklink><nobr><a href='?lookup=3 John'>3 John</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Jude'>Jude</a></nobr></span> <span class=booklink><nobr><a href='?lookup=Revelation'>Revelation</a></nobr></span> <script>
ifie("</div>");

</script></div>

<!-- End Bible Book list -->

<?php

}// end booklink

?>



<!-- End Body -->


<div class=center>
<br><BR>

<span class=info>Bible SuperSearch<br>
<a href="http://www.biblesupersearch.com" target="_NEW">www.BibleSuperSearch.com</a></span>

<br><BR></div>
<!-- End homelink -->

</body>
</html>
