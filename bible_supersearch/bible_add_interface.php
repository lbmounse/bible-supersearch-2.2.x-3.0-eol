<?php 
// SECURE
//session_start(); 
require_once("bible_inst_functions.php");

//if(!check_login_file()){return;}

require_once("bible_inst_auth.php");
if ($auth==false){return;}
?>

<title> Unsupported Bible Importer </title>
<center><big><BIG>Bible Importer</big></big></center>

<a href="bible_inst_manager.php">Return to the Manager Homepage</a>  <br><br>


<a name=inst><center><big><BIG>Instructions</big></big></center></a>



The <a target="_blank"
 href="http://unbound.biola.edu/index.cfm?method=downloads.showDownloadMain">Unbound
Bible</a> has a large repository of public domain Bibles for
download. &nbsp;Bible SuperSearch includes a script that imports
any Bible in the Unbound format. &nbsp; Download each Bible that
you desire. I reccommend that you only use Bibles that are Textus Receptus based. I am still researching some of the Bibles in this list. All that are Textus Recuptus based will be released as official modules in the future.<br>
<br>
For each Bible that you desire, <br>
<ul>
  <li>Download the Bible from <a
 href="http://unbound.biola.edu/index.cfm?method=downloads.showDownloadMain" target="_blank">http://unbound.biola.edu/index.cfm?method=downloads.showDownloadMain</a></li>
  <li>extract the files &lt;bible_name&gt;_utf8.txt and
&lt;bible_name&gt;.html from the .zip file. &nbsp;<br>
&nbsp; &nbsp; &nbsp; &nbsp;Example: arabic_svd_utf8.txt
and arabic_svd.html.</li>
<li>Upload the extracted files to your Bible SuperSearch main directory.</li>
  <li>Refresh this page.</li>
  <li>Select the Bible to import below.</li>
  <li>Fill in the required information.</li>
  <li>Import the Bible.</li>
  <li>Repeat as nessessary.</li>
</ul>
<br>

<form action="bible_add_unbound.php" method=post>

<?php

$bibles=getBibles();

echo("Bible file to import <select name=file>");
dropmenu($bibles);
echo("</select>");

?>

<br><BR>SHORT Bible abbreviation  <input type=text name=bible maxlength=8 size=8> (must be unique, alphanumeric only. If you enter the abbreviation of a Bible already installed, that Bible will be deleted.)<br><BR>
Full Bible name <input type=text name=name> This appears in the menu, along with the English language name, as &lt;full_name&gt; (&lt;language&gt;).<br><BR>
Language: English name <input type=text name=language_long> (ie 'Spanish') &nbsp; <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php" target="_new">2 character ISO 639-1 language code</a> <input type=text name=language maxlength=2 size=2> (ie 'es')<br><BR>

Which testaments does this Bible contain?  <select name=testaments><option value="both"> Both OT and NT</option><option value="nt"> NT </option><option value="ot"> OT </option></select>

<br>
Where did you get this Bible text:<br>
<textarea name=source rows=3 cols=100>This Bible imported from The Unbound Bible http://unbound.biola.edu</textarea>

<br><BR>Advanced Options (Do not change these if you do not know what you are doing.)<br>
Import book list <select name=book_list><option value="yes"> Yes </option><option value="no" selected> No </option></select>
Import Bible <select name=install_bible><option value="yes"> Yes </option><option value="no"> No </option></select>
Insert into Bible version table <select name=insert_into_bible_table><option value="yes"> Yes </option><option value="no"> No </option></select> &nbsp; 

<input type=hidden name=submit value=true><input type=submit value=" IMPORT BIBLE "></form>




This script imports Bibles in a single file into a MySQL database table for use with Bible SuperSearch. <BR>

Files can be in one of the following two formats<BR><BR>
<pre>
book_index[tab]	chapter[tab]	verse[tab]	text<BR>
EX: 01	1	1	In the beginning God created the heaven and the earth.<BR>
or<BR>
book_index[tab]	chapter[tab]	verse[tab][tab]		subverse(ignored)[tab]	text<BR>
Ex: 01	1	1		10	In the beginning God created the heaven and the earth.
</pre>

<br>
Where book_index is the number of the book, Genesis = 1, Revelation = 66.   Subverse is ignored.<BR>
<BR>

This script uses two files:<BR><BR>

&lt;bible_name&gt;_utf8.txt - Contains the Bible text, formated as above<br><br>

&lt;bible_name&gt;.html - Contains a HTML-formatted description of this Bible.<br><BR>

Both files are required.


<BR><BR><BR><BR>

<?php


// script options

$dir="."; // directory of Bible files
//$file="kjv_apocrypha";//File name, minus "_utf8.txt" example: "kjv_utft8.tx" => enter "kjv"
//$shortname="";
//$bible="kjv"; // short name or abbreviation
//$name="Authorized King James Version"; // full name, to appear in the Bible version menu
//$language="en"; // 2-3 character language code
//$language_long="English";  // full English name of the language 
//Which testaments does this Bible contain: ot,nt,both
//$testaments="both"; 
// Where did you get this Bible?   
//$source="This Bible imported from The Unbound Bible <a href='http://unbound.biola.edu/'>http://unbound.biola.edu/</a>";

$book_list=false;
$install_bible=false;
$insert_into_bible_table=true;

function getBibles(){

$dir=array();
//echo($dir);

if($dir==""){$d = dir('.');}
else{$d =dir(".");

//print_r($dir);

//echo("IM here");
}

while (false !== ($entry = $d->read())) {
   $dir[]= $entry;

//echo($entry."<br>");
}

$bibles=array();
foreach($dir as $file){

$ipos=strpos($file, "_utf8.txt");
$fil=substr($file, 0,$ipos);

if((strpos($file,"_utf8.txt")!==false)){//&&(array_search($fil."html",$dir))){

$bibles[]=$fil;


}// end if

}// end foreach



return $bibles;

}// end function 

function dropmenu($bibles){

print_r($bibles);

foreach($bibles as $bib){

echo("<option value='$bib'> $bib </option>
");

}// end foreach

}// end function

?>
