-- 
-- Table structure for table `bible_versions`
-- 

DROP TABLE IF EXISTS `bible_versions`;
CREATE TABLE `bible_versions` (  `index` int(3) NOT NULL auto_increment,  `shortname` tinytext NOT NULL,  `fullname` tinytext NOT NULL,  `description` text NOT NULL,  `language` tinytext NOT NULL,  `language_short` char(2) NOT NULL default '',  PRIMARY KEY  (`index`)) ENGINE=MyISAM ;
