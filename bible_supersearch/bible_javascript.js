// Bible SuperSearch Version 1.0.00
// The free, open source, PHP and MySQL web-based   
// Bible Reference retrevial and search utility
//
// Copyright (C) 2006 Luke Mounsey
// www.BibleSuperSearch.com
// www.Alive-in-Christ.com
//
// bible_javascript.js
// JavaScript Functions used by Bible SuperSearch
// These are used mostly for form operations.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License included in the file 
// "license.txt" for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// this code is only relavent for Alive-in-Christ.com's frames layout.
// But it also serves as a template to clear a form if it is located in a different 
// frame than the one in which the verses are displayed.   Comment out if not needed.
// Hint: if your website does not use frames, you do not need this.
// COMMENT OUT IN RELEASE VERSION
// top.left.document.bible.lookup.value="";
// top.left.document.bible.search.value="";


// ifie
// writes text if the user's browser is Internet Explorer
// Used for browser-specific formatting
// @ param - text text to write
// @ returns - none

function ifie(text){
nav=navigator.appName;
if ((nav=="Microsoft Internet Explorer")||(nav=="MSIE")){
document.write(text);
}// end if

}// end ifie


// last
// restores the values of the last lookup/search into the form
// @ param look - reference
// @ param search - search query
// @ param type - search type
// @ param wholeword - true if whole word search is selected
// @ returns none

function last(look, search, type, wholeword){

// me is the NAME of the form
// this is a test
me.lookup.value=look;
me.search.value=search;
//me.searchtype.value=type;
//me.wholeword.value=wholeword;

}// end last

// clearForm
// Clears the form
// @ param - none
// @ returns - none

function clearForm(){

me.lookup.value="";
me.search.value="";
}// end clearForm

// lowres
// indicates if the screen resolution is less than 1024x768
// Used for formatting
// @ param - none
// @ return - true if below 1024x768, false otherwise
function lowres(){
if (screen.width < 1024){return true;}
else{return false;}
}
