<?php
//require_once("bible_inst_auth.php");
//if ($auth==false){return;}
?>
<html>
<title>Bible SuperSearch Tester Version 0.5</title>
This tests to see if you have Bible SuperSearch properly installed.<br><BR>
<?php
// SECURE
require_once("bible_misc.php");

require_once("bible_lookup.php");
//echo" => found<br><BR>";

//echo"Looking for bible_display.php";
require_once("bible_display.php");
require_once("bible_mysql.php");
connect();


$table_prefix="bible_";
$version=$_GET["bible"];
$phpinfo=$_GET["phpinfo"];

if($phpinfo=="true"){

echo("<a href='bible_inst_manager.php'>RETURN TO MANAGER</A> <A HREF=''>RETURN TO BIBLE SUPERSEARCH TESTER</A><BR><br>");

phpinfo();

return;

}

function testFile($file, $required=true){


echo("Looking for \"$file\" ==> ");
if (is_file($file)){

echo("FOUND.<br>");
return 0;

}// end if
else{

if ($required){echo("<br><b>ERROR: \"$file\" NOT FOUND.  Please upload this file, or Bible SuperSearch will not work!!</b><br><BR>");}
else{echo("<BR><b>Warning: \"$file\" NOT FOUND.</b><br><BR>");}

return 1;
}// end else

}// end testFile

if($version==""){

$err=0;
// are you using the correct php version?
$phpver=explode(".",phpversion());
$msl=mysql_get_server_info();
if ($phpver[0]<4){
echo("ERROR: You are using PHP ".phpversion()." Bible SuperSearch requires PHP 4.0.5 or better.");
return false;
}// end if
echo("You are using PHP ".substr(phpversion(),0,5)." (".phpversion().") <br>with MySQL ".substr($msl,0,5)." ($msl) &nbsp; <a href='?phpinfo=true'>PHP Info</a><br>");



// are the required files avaliable?
echo("<br><b>Looking for all required files.   Bible SuperSearch will not run without these.</b><br><br>");

$err+=testFile("bible_lookup.php");
$err+=testFile("bible_display.php");
$err+=testFile("bible_main.php");
$err+=testFile("bible_init.php");
$err+=testFile("bible_mysql.php");
$err+=testFile("bible_login.php");
$err+=testFile("bible_config.php");
$err+=testFile("bible_system.php");
$err+=testFile("bible_misc.php");
$err+=testFile("bible_interfaces.php");
$err+=testFile("bible_supersearch.php");
$err+=testFile("interfaces/bible_stylesheet.css");
$err+=testFile("bible_javascript.js");
$err+=testFile("bible_supersearch_user_guide.html");
$err+=testFile("bible_shortcuts.sql");
$err+=testFile("bible_sessions.sql");
$err+=testFile("bible_versions.sql");


if ($err!=0){
echo("<b>$err ERRORS ABOVE.  PLEASE FIX THEM.</b>");
return false;}

/*
// looking for mysql files
echo("<br><b>Looking for the .SQL files.   These are required ONLY if you installed Bible SuperSearch using the provided installer.   If you installed using phpMyAdmin, ignore any errors below.</b><br><BR>");

testFile("modules/bible_books_en.sql",false);
testFile("modules/kjv1.sql",false);
testFile("modules/kjv2.sql",false);
testFile("modules/kjv3.sql",false);
*/

/*
echo("<br><b>Looking for the image files.   These are not required, but do make Bible SuperSearch look much better.</b><br><BR>");

$err+=testFile("bible_images/background_classic.jpg",false);
$err+=testFile("bible_images/prevnext/standard/pb.gif",false);
$err+=testFile("bible_images/prevnext/standard/pc.gif",false);
$err+=testFile("bible_images/prevnext/standard/sc.gif",false);
$err+=testFile("bible_images/prevnext/standard/nc.gif",false);
$err+=testFile("bible_images/prevnext/standard/nb.gif",false);
$err+=testFile("bible_images/prevnext/standard/pb_nl.gif",false);
$err+=testFile("bible_images/prevnext/standard/pc_nl.gif",false);
$err+=testFile("bible_images/prevnext/standard/sc_nl.gif",false);
$err+=testFile("bible_images/prevnext/standard/nc_nl.gif",false);
$err+=testFile("bible_images/prevnext/standard/nb_nl.gif",false);

*/
//echo"Looking for bible_lookup.php";

//echo" => found<br><BR>";

require_once("bible_system.php");

if (($prod_version==null)||($bss_name==null)){
echo("Bible SuperSearch not found.   Please make sure that you have uploaded all required files in the Bible SuperSearch directory.");
return;}
else{//Give the version of Bible SuperSearch
echo("<br>You are running <b>$bss_name</b> version $bss_version<br><BR>");
}

}// end big if

// is mysql.php avaliable?
//echo"looking for mysql.php";

//echo" => found<br><BR>";

if($version==null){
//can we connect to the mysql database?
//echo"Attempting to connect to the mySQL server.<br>If you successfully used the installer, you will not have any problems here. <br><BR>";
}
//connect();

$version = mysan($version);

$res=mysql_query("select * from `bible_versions` where `shortname` = '$version'");
$bible=mysql_fetch_array($res);

if($version!=""){$lang=$bible["language_short"];}
else{$lang="en";}

echo("Successfully connected to the MySQL database.<br><BR>");
//show table of books

// run the query to get the entire `bible_books` table
$test=mysql_query("Select * from `bible_books_$lang` order by number;");

if ($test==null){
	echo"Unable to find the book names table for the '$version' Bible (`bible_books_$lang`) on the database.  However, this is not required.<br><BR>";
	
}// end if
else{
echo "Table `bible_books_$lang` found.  The following books of the Bible are installed:<br><BR>";
// set up the table
echo"<table border=2><tr><td>number</td><td>fullname</td><td>short</td></tr>";
$count=0;
error_reporting(E_ERROR);
while($row = mysql_fetch_array($test)){
$count+=1;
echo'<tr><td>'.$row["number"].'</td><td>'.$row["fullname"].'</td><td>&nbsp;'.$row["short"].'</td></tr>';
}// end if
error_reporting(E_ERROR | E_WARNING| E_PARSE);
echo"</table>";

if (($count!=66)&&($count!=0)){echo"<BR><BR>There are $count Bible books installed.<BR> You do not have the correct number of books installed.   You should have 66 books.   Please delete and reinstall the \"bible_books\" table.<br>";}
echo "<br>";
}// end else

if($version==null){
$test=mysql_query("Select * from `bible_shortcuts_en` order by `index`;");
if ($test==null){echo("WARNING: table `bible_shortcuts_en` is not avaliable.   Predefined search limits will not work.  If you desire this feature, please reinstall Bible SuperSearch.");
}

echo "Table `bible_shortcuts_en` found.  The following shortcuts/search limiters are installed:<br><BR>";
// set up the table
echo"<table border=2><tr><td>Name</td><td>Alias 1</td><td>Alias 2</td><td>Alias 3</td><td>Reference</td><td>Display</td></tr>";

while($row = mysql_fetch_array($test)){

echo'<tr><td>'.$row["name"].'&nbsp;</td><td>'.$row["short1"].'&nbsp;</td><td>'.$row["short2"].'&nbsp;</td><td>'.$row["short3"].'&nbsp;</td><td>'.$row["reference"].'&nbsp;</td><td>'.$row["display"].'&nbsp;</td></tr>';
}// end while
echo("</table><br>");
}// end if

$err=0;
$istable=false;

if ($version==""){

$re=mysql_query("SELECT * FROM `bible_versions`;");

while($arr=mysql_fetch_array($re)){
$err+=testVersion($arr['shortname']);
$istable=true;
}// end while

}// end if

else{
$err=testVersion($version);
$istable=true;
}// end else

//}// end while
if (!$istable){
echo("Table `bible_versions` not found.   Please reinstall Bible SuperSearch");
return false;
}// end if


if($err!=0){echo("<b>There are problems with your installation.  Please reinstall Bible SuperSearch again.   If you can't get it to work, please visit BibleSuperSearch.com for help");
return false;}



//testing
function testVersion($version){
global $table_prefix;
echo("Testing table for the `$version` Bible version<BR><BR>");

$re=mysql_query("SELECT * FROM `bible_versions` where `shortname` = '$version';");
$version_info = mysql_fetch_assoc($re);

// does bibleverses exist?
$test=mysql_query("Select * from `$table_prefix$version` Where `index` = 1");
if ($test==null){
	echo"Unable to find table `$table_prefix$version` on the database.   Please make sure that it is properly installed.  You may need to rerun the installer.<br><BR>";
	return 1;
}// end if
else{echo"`$version` table found.<br><BR>";}

//test first, last and middle in the verse list
$error=0;// error counter
$error+=testVerses("Gen 1:1 (First verse)", "01", "1", "1", $version);
$error+=testVerses("Ps 23 (Chapter)", "19", "23", "", $version);
$error+=testVerses("2 John (Book)", "63", "", "", $version);
$error+=testVerses("Rev 22:21 (Last Verse)", "66", "22", "21", $version);
// if any errors are found, don't continue
if($error!=0){return 1;}

echo($version_info['description']);
echo('<br><br>');

return 0;
}// end testVersion
// tests the verse lookup capability
// title - heading to be printed
// book - book of the Bible (full, abbreviated or number)
// chapter - chapter of the book
// verse - verse number of the book
// version - Bible Version
function testVerses($title, $book, $chapter, $verse, $version){

//error_reporting(E_ALL);

//echo("b $book c $chapter v $verse ver $version<br>");

$error = 0;// error counter
echo("<b>$title - $version</b><br>");
$verses=quickLookup($book,$chapter,$verse,$version);//getVerses($book,$chapter,$verse,"","",$version);
//print_out($verses);
// was there a verse found?
if ($verses==null){
echo("<b>Note: $title was not found.   This passage may or may not be needed by this translation, depending on how much of the Bible it contains.   If it has been ommitted accidentally, please reinstall this module.</b><br><BR>");
return 0;
}// end if

$i=0;
while($ver=$verses[0][$i]){

echo($ver["text"]."<br>");
$i+=1;
}
echo"<br><BR>";
return 0;
}// end testVerses


// is bible_lookup.php avaliable?
echo"<b>Congratulations!  Bible SuperSearch is installed and ready!</b> <br><br>

To use Bible SuperSearch, open \"bible_supersearch.php\" in you web browser.<br><BR>

The following Bible SuperSearch interfaces are avaliable on your system. &nbsp; Please pick the one you like best.<br><BR><center>";

previewInterfaces();



?>
<a href='bible_inst_manager.php'>RETURN TO BIBLE MANAGER</A>
