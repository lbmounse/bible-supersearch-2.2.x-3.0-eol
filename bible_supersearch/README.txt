BIBLE SUPERSEARCH 2.2.x

NOTICE:  AS OF MAY 2, 2020, THIS LEGACY SOFTWARE HAS GONE END-OF-LIFE, AND WILL NO LONGER BE SUPPORTED OR MAINTAINED.

NOTICE:  THIS LEGACY SOFTWARE IS NOT SUPPORTED IN PHP 7 +

PLEASE DOWNLOAD AND INSTALL THE LATEST VERSION
https://www.biblesupersearch.com/downloads/
https://www.biblesupersearch.com/migration-legacy/

-------------------------------------------------------------------------------------------------------------

Bible SuperSearch 2.2.00
www.BibleSuperSearch.com

Short Instructions

Extract files if needed.

Upload all files to a directory on your website.

If this is a fresh installation:
	Rename bible_login_demo.php to bible_login.php, and grant read/write permissions on this file.
	Rename bible_config_demo.php to bible_config.php, and grant read/write permissions on this file.

Open bible_install.php in a web browser.
Use your MySQL login to log into the installer.
Follow the installer instructions.

See the file "installation_guide.pdf" or "Installation Guide.doc" for complete instructions

Bible SuperSearch

FEATURES:

    * Multiple Bible and Foreign Language Support
    * Several Bible versions included, many more available for download.
    * Parallel Bible - look up passages in up to four Bibles at once. 

    * Switch between nice formatting and formatting that makes copying and pasting easier.
    * Choose text size and style.

    * Looking up verses or passages with standard references. Ex: Rom 5:8 or Jn 3:16-18
    * Looking up multiple passages at once. Ex: Rom 5:8; Jn 3:16-18; Rom 10:9,13

    * "Browsing Buttons" allow easy browsing between chapters and books. 

    * Searching for
          o Any word
          o All Words
          o Exact Phrase
	  o Searching for terms within 5 verses, or any number of verses. (not nessessarily the same verse.)
	  o Searching for terms within the same chapter.
          o Boolean search (with support for grouping terms using parentheses.)
          o All words, within 5 verses
    * Advanced search page

    * Easily see a verse in its context.
    * Searches show 20 verse results per page, but will display all results on request

    * Search within a particular section of Scripture, such as Law, Major Prophets, or Gospels
    * Specify a passage or passages and search within it.

For the webmaster, Bible SuperSearch offers:

    * Fully customizable web interface
    * Several web interfaces to choose from
    * Easy installation manager that bypasses the need to use PHPMyAdmin
    * Testing tool that makes sure the install was successful

System Requirements

These vary depending on which version you are using.
Below are the requirements for the current version.

    * Webserver (Apache preferred)
    * PHP 4.0.5 or better
    * MySQL 3.23 or better (Has been tested on MySQL 4.0.24 and 5.0.38, but should work on older versions)
    * 6 MB MySQL database space
    * 12 MB web space (6MB minimum)
    * If more than one Bible is desired, 6MB additional MySQL space and 6MB (temporary) webspace is requred per Bible.
    * Installing all the Bibles provided with this release will require 130 MB of MySQL space and 130 MB of temp. space.


BIBLE MODULES:
King James Version (English)
Tyndale Bible (English)
Textus Receptus Greek NT
Textus Receptus Greek NT (Parsed)
Sagradas Escrituras 1569 (Spanish)
Reina Valera 1858 NT (Spanish)
Reina Valera 1909 (Spanish)
Reina Valera Gómez (Spanish)
Martin 1744 (French)
La Bible de l'Épée (2005) (French)
Ostervald 1996 (French)
Luther Bible 1545 (German)
Bible Kralicka (Czech)
Afrikaans 1953
Smith Van Dyke (Arabic)
Karoli (Hungarian)
Diodati 1649 (Italian)
Cornilescu (Romanian)
Synodal 1876 (Russian)
Finnish 1776
Lithuanian Bible
Maori Bible
Thai KJV
