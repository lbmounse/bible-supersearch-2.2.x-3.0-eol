<?php

require_once("bible_init.php");

// Compact Simple Interface
$interface_name="Classic";
$interface="classic";
$interface_description="Original interface from the first release.";

$interface_text="

<img src='bible_images/background_classic.jpg'>

<table class=\"classic_outer_table\" cellspacing=0 style='background-image: url('bible_images/background_classic.jpg');'><tr>
<td colspan=2 class=\"classic_title_row\"><b><span class=\"title classic_title\">KJV Bible SuperSearch</span></b></td></tr>
<tr>
<td class=\"classic_form\">
<center>
<table border=0 class=\"element_center\">
<tr><td class=\"classic_table2_lookup\">
<form name=\"me\" action=\"$url\" method=\"get\">

".formMemory()."
<input type=\"text\" class=\"text_box\" name=\"lookup\" size=\"40\" maxlength=\"256\"> <input class=\"button\" type=\"submit\" value=\"look up\"> &nbsp;<br>
<br>

<nobr>".bibleversions()." &nbsp; <a class=\"helplink\"  href=\"bible_supersearch_user_guide.pdf\" target=\"_new\" >help</a></nobr> <br>

<!-- This is old code, but may be useful to you
<a class=link onClick=\"javascript:windowprops = 'left=50,top=50,width=300,height=200, scroll=on';
window.open('bible_search_help.htm', 'help', windowprops);\">help</a>-->


</td><td class=\"classic_table2_search\">

<!-- Search Options: -->
<input type=\"text\" class=\"text_box\" name=\"search\" size=\"20\" maxlength=\"256\"> <input class=\"button\" type=\"submit\" value=\"search\">
<input type=\"checkbox\" name=\"wholeword\" id=\"wholeword\" value=\"Whole words only.\" checked> <label class=label for=\"wholeword\">Whole words only.</label></input>

<table border=0 cellspacing=0 style=\"padding:0;\">
<tr><td>
<input type=\"hidden\" id=\"sub\" name=\"submit\" value=\"true\">
<input type=\"radio\" name=\"searchtype\" id=\"st\" value=\"All Words\" checked selected> <label class=label for=\"st\">All Words</label></td>
<td><input type=\"radio\" name=\"searchtype\" id=\"st2\" value=\"Any Word\"><label class=label for=st2> Any Word</label></input></td>
<td style=\"padding-left:10px;\">
<input class=\"button button_med\" type=\"button\" value=\"clear entry\" onclick=\"clearForm()\">
</td><td></td></tr>
<tr><td>
<input type=\"radio\" name=\"searchtype\" id=st3 value=\"Exact Phrase\"><label class=label for=st3> Exact Phrase</label><br></td><td>
<input type=\"radio\" name=\"searchtype\" id=st4 value=\"Advanced Search\"><label class=label for=st4> Boolean Search</label><br>
<td class=\"classic_table2_editsearch\" style=\"\">".editSearch()."</td></td></tr></form>
</table>
</td></tr>
<tr><td colspan=2 style=\"text-align:left; spacing:0; padding:0;\">
".youRequested()."
<td></tr>
</table></center>
</td></tr>

</table><br><BR><BR>
";

require_once("bible_interfaces.php");
