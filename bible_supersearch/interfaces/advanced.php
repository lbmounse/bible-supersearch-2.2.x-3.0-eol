<?php

// Parallel interface

require_once("bible_init.php");
// Interface Name
$interface_name="Advanced";
$interface="advanced";
$interface_description="Advanced Search Page.";


$interface_text="

<span class=title>Bible SuperSearch Advanced Search</span>

<div class=adv_form>
<form name=me action=$url method=get>
<div class=adv_header>Word Search</div>
<center><p class=center align=center>

".advancedsearchbible()."

</p></center>



Find verses containing:
<table style='background-color:transparent;padding:0' border=0>
<tr><td><b>All</b> of the words: </td><td> <input type=text name=all> </td>
<td rowspan=5 style='padding:5px'>
<nobr>Restrict search to:<input type=hidden name=searchtype value='Boolean Search'></nobr>

<select name='lookup2'>".limitSearch("Reference(s) Below").otdropdown().ntdropdown()."</select></nobr><br>
<input type=checkbox name=\"wholeword\" checked id=\"ww\" value=\"Whole words only.\"> <label for=\"ww\"><nobr>Whole Words</nobr></label><br>
<input type=checkbox name='within' id='within' value='true'> <label for='within'>Search within results</label><br>
<br>
<div style='text-align:right'><input type='submit' value='Word Search'></div>



</td></tr>
<tr><td><b>Any</b> of the words: </td><td> <input type=text name=any> </td></tr>
<tr><td style='width:170px'><b>Only one</b> of the words: </td><td> <input type=text name=one> </td></tr>
<tr><td><b>None</b> of the words: </td><td> <input type=text name=none> </td></tr>
<tr><td>The <b>exact phrase</b>: </td><td> <input type=text name=phrase> </td></tr>
</table>
<br>
<div class=adv_restrict>

<!--<select name='chap2chap'><option value=''> Book </option><option> Old Testament </option>".otdropdown()."<option> New Testament </option>".ntdropdown()."</select> &nbsp;<input type=text class=chapter2chapter name=chapter1 maxlength=3> <b>:</b> <input type=text class=chapter2chapter name=verse1 maxlength=3> <b>-</b> <input type=text class=chapter2chapter name=chapter2  maxlength=3> <b>:</b> <input type=text  maxlength=3 class=chapter2chapter name=verse2><br><BR>-->



</div>


<b>Power Search</b><br>
<table style='background-color:transparent;margin-right:auto;padding:0;width:100%' border=0>
<tr><td>Passage<br>Limitation </td><td><textarea name=lookup cols=50 rows=2 class=power></textarea></td><td rowspan=2> 
<input type='submit' value='Power\nSearch'> </td></tr>
<tr><td>Boolean<br>Search</td><td><textarea name=search cols=50 rows=2 class=power></textarea><br></td></tr>
</table>

<br>
<div class=adv_prox>

<b>Proximity Search</b><br>
<table style='width:100%' border=0><tr><td>
Find words within <select name='proxrange' class='prox_range'><option value='1'>&plusmn; 1 verse</option>
<option value='2'>&plusmn; 2 verses</option><option value='3'>&plusmn; 3 verses</option><option value='4'>&plusmn; 4 verses</option>
<option value='5' selected>&plusmn; 5 verses</option><option value='10'>&plusmn; 10 verses</option><option value='20'>&plusmn; 20 verses</option><option value='200'>a Chapter</option></select>&nbsp; &nbsp;
<input type=text name='prox' class='prox_keywords'> </td><td style='text-align:right'><input type='submit' value='Proximity Search'></td></tr></table>
</div>

".formMemory()."
</form>
</div><br>

<div class=adv_form>
<form name=look action=$url method=get>
<div class=adv_header>Passage Retrieval</div>

<p class=center>

".advancedsearchbible()."

</p>

<table style='width:100%' border=0><tr><td style='width:90px'>
Passage(s) &nbsp; </td><td style='width:350px'><input type=text name=lookup style='width:100%'> </td><td style='text-align:right'><input type=submit value='Passage Retrieval'></td></tr>
<tr><td colspan=3><span style='font-size:80%'>Example: Rom 5:1-3; Jn 15:5</span></td></tr></table><br>

<!--<select name='chap2chap'><option value=''> Book </option><option> Old Testament </option>".otdropdown()."<option> New Testament </option>".ntdropdown()."</select> &nbsp;<input type=text class=chapter2chapter name=chapter1 maxlength=3> <b>:</b> <input type=text class=chapter2chapter name=verse1 maxlength=3> <b>-</b> <input type=text class=chapter2chapter name=chapter2  maxlength=3> <b>:</b> <input type=text  maxlength=3 class=chapter2chapter name=verse2> <input type=submit value='Go'> &nbsp; -->

<!--<input type=submit name='lookup2' value='Random Verse' style='width:120px'> 
<input type=submit name='lookup2' value='Random Chapter' style='width:120px'>

<BR><br>-->

Browse by Book: ".bookdropdown("ot")." <input type=submit value='Go'> ".bookdropdown("nt")." 
<input type=submit value='Go'>




".formMemory()."
</form>
</div>
<input type=hidden name=submit value=true>


<br>


".youRequested()."

</center><br>
".formMemory()."
</form>

";

require_once("bible_interfaces.php");
