<?php

require_once("bible_init.php");

// Compact Simple Interface
$interface_name="Compact Simple";
$interface="compact_simple";
$interface_description="Minimalistic compact interface.";

$interface_text="

<form name=me action=$url method=get>
<span class=title>Bible SuperSearch</span>

<input type=hidden name=submit value=true>
<table style=\"margin-left:auto;margin-right:auto; padding:0;\" border=0>


<tr><td>Bible</td><td colspan=2>".bibleversions()."</td><td colspan=2 align=center><nobr>Search Options</nobr></td><td><a href=\"bible_supersearch_user_guide.pdf\">Help</a></td></tr>

<tr><td>Passage</td><td><input type=text name=lookup class=query></td>
<td rowspan=2 style=\"width:40px\"><input type=submit value=\"Go\" style=\"height:50; width:34px;\"></td><td><nobr>". searchOptions($searchtype)."</nobr></td><td></td><td><input type=\"button\" value=\"Clear Form\" class=\"button button_med\" onclick=\"clearForm();\">

</tr>

<tr><td>Search</td><td><input type=\"text\" name=\"search\" class=query></td><td colspan=2><input type=checkbox name=\"wholeword\" checked id=\"ww\" value=\Whole words only.\"> <label for=\"ww\"><nobr>Whole Words Only</nobr></label>

</td><td>".editSearch("Edit Search")."</td>

</tr>
<tr><td colspan=8>".youRequested()."</td></tr>
</table>
</center>
".formMemory()."
</form>
";

require_once("bible_interfaces.php");
