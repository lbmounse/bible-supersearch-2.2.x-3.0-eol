<?php

// Parallel interface

require_once("bible_init.php");
// Interface Name
$interface_name="Parallel";
$interface="parallel2";
$interface_description="Beta interface for Parallel search.";

$interface_text="
<form name=me action=$url method=get>
<span class=title>Bible SuperSearch</span>

<input type=hidden name=submit value=true>
<table style='margin-left:auto;margin-right:auto; padding:5px; background-color:tan;' border=0>


<tr><td>".bibleversions($version1,1)."</td><td style='text-align:right'> &nbsp;Passage </td><td colspan=3> <input type=text name=lookup class=query> &nbsp;<input type=submit value='Look up' style='width:70px;font-size:70%'></td></tr>

<tr><td>".bibleversions($version2,2)."</td><td style='text-align:right'>Search </td><td colspan=3> <input type=text name=search class=query> &nbsp;<input type=submit value='Search' style='width:70px;font-size:70%'></td></tr>

<tr><td>".bibleversions($version3,3)."</td><td colspan=1 style='text-align:right' valign=middle>Look for </td><td colspan=1 valign=top> ". searchOptions($searchtype)." in <select name='lookup2'>".limitSearch()."</select></td><td style='text-align:right'><input type=submit name='lookup2' value='Random Verse' style='width:110px;font-size:70%'></td></tr>

<tr><td>".bibleversions($version4,4)."</td><td colspan=2> &nbsp; &nbsp;<input type=checkbox name=\"wholeword\" checked id=\"ww\" value=\"Whole words only.\"> <label for=\"ww\"><nobr>Whole Words</nobr></label> &nbsp; &nbsp; <input type=checkbox name='within' id='within' value='true'> <label for='within'>Search within results</label></td><td style='text-align:right'><input type=submit name='lookup2' value='Random Chapter' style='width:110px;font-size:70%'></td></tr>

</table>
<br>


".youRequested()."

</center>
".formMemory()."
</form>

";

require_once("bible_interfaces.php");
