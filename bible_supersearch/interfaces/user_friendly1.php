<?php

// Parallel interface

require_once("bible_init.php");
// Interface Name
$interface_name="User Friendly #1";
$interface="user_friendly1";
$interface_description="Beta interface for Parallel search.";

//$url="search.php";

$interface_text="
<form name=me action=$url method=get>
<span class=title>Bible SuperSearch</span>

<input type=hidden name=submit value=true>

<div class=form>

".user_friendly1_versions()."

Enter word(s), phrase(s) or expression(s):<br>

<input type=text name=search class=query><br><br>

Search for: ". searchOptions($searchtype)."<br>Limit search to: <select name='lookup2'>".limitSearch("Below passage(s)")."</select><br>

<input type=checkbox name=\"wholeword\" checked id=\"ww\" value=\"Whole words only.\"> <label for=\"ww\"><nobr>Whole words only</nobr></label>&nbsp; &nbsp;<input type=checkbox name='within' id='within' value='true'> <label for='within'>Search within results</label><br>

<input type=submit value='Search the Bible'><br><BR>

Enter passage(s):<br>
<input type=text name=lookup class=query><br>

<small>Example: John 4; Rom 5:8;</small><br>

 &nbsp;<input type=submit value='Look up Passage(s)' style=''><br><br>

<input type=submit name='lookup2' value='Random Chapter' style='width:110px;font-size:70%'>
<input type=submit name='lookup2' value='Random Verse' style='width:110px;font-size:70%'>
".searchTip()."

<br>
</div>
<br>

".youRequested()."

</center>
".formMemory()."
</form>

";

require_once("bible_interfaces.php");

function user_friendly1_versions(){

global $version, $version1, $version2, $version3, $version4, $webservice_default_version;

$res=mysql_query("select count(*) from `bible_versions`");
$numbib=mysql_fetch_array($res);
$num=$numbib["count(*)"];

if($webservice_default_version!=""){$num=1;}

if($num!=1){$text="Select Bible version(s):<br>";}
else{$text="";}

$text.=bibleversions($version1,1)."<br>";

if($num>=2){$text.=bibleversions($version2,2)."<br>";}
if($num>=3){$text.=bibleversions($version3,3)."<br>";}
if($num>=4){$text.=bibleversions($version4,4)."<br>";}

$text.="<br>";

return $text;


}
