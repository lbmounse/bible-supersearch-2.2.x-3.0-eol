<?php

// Parallel interface

require_once("bible_init.php");
// Interface Name
$interface_name="Compact book browsing";
$interface="compact_book_browsing";
$interface_description="Compact interface with book browsing dropdown menus.";


$interface_text="
<form name=me action=$url method=get>
<span class=title>Bible SuperSearch</span>

<input type=hidden name=submit value=true>
<table style=\"margin-left:auto;margin-right:auto; padding:0;\" border=0>


<tr><td>Bible</td><td colspan=2>".bibleversions()."</td><td colspan=2>Search Options</td><td><a href=\"bible_supersearch_user_guide.pdf\">Help</a></td><td>Browse by Book</td></tr>

<tr><td>Passage</td><td><input type=text name=lookup class=query></td>
<td rowspan=2 style=\"width:40px\"><input type=submit value=\"Go\" style=\"height:50; width:34px;\"></td><td>". searchOptions($searchtype)."</td><td></td><td><input type=\"button\" value=\"Clear Form\" class=\"button button_med\" onclick=\"clearForm();\"><td>".bookdropdown("ot")."</td><td><input type=submit value=\"Go\" style=\"width:30px\"></td>

</tr>

<tr><td>Search</td><td><input type=\"text\" name=\"search\" class=query></td><td colspan=2><input type=checkbox name=\"wholeword\" checked id=\"ww\" value=\"Whole words only.\"> <label for=\"ww\">Whole Words Only</label>

</td><td>".editSearch("Edit Search")."</td>
<td>".bookdropdown("nt")."</td><td><input type=submit value=\"Go\" style=\"width:30px;\"></td>
</tr>
<tr><td colspan=8>".youRequested()."</td></tr>
".formMemory()."
</form>
</table>
<br>
";

require_once("bible_interfaces.php");
