<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<div class='bible_supersearch'>
<?php

// SECURE
// Bible SuperSearch Version 2.0.80
// The free, open source, PHP and MySQL web-based   
// Bible Reference retrevial and search utility
//
// Copyright (C) 2006, 2007, 2008 Luke Mounsey
// www.BibleSuperSearch.com
// www.PreservedWord.com
//
// bible_supersearch.php
// Standard Web interface for Bible SuperSearch
// Feel free to use this interface as a stand-alone, or incorporate it
// into your site layout
// Just paste this entire code into your site template.
// And make sure that you save it as a .PHP file!!! (Not .HTML) 
//
// This is the file the user loads on their computer from your server
// This routes to the selected interface file.
// For additional html interfaces, see www.BibleSuperSearch.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License included in the file 
// "license.txt" for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

//error_reporting(E_ALL);

require_once("bible_config.php");
require_once("bible_system.php");

$interface=$_GET['interface'];

// Webservice specific functions

$webservice_interface=$_GET['webservice_inter'];
$webservice_default_version=$_GET['webservice_default_ver'];

if(($webservice_interface!="")&&($interface!="advanced")){$interface=$webservice_interface;}

// set the interface to use
if($interface==""){$interface=$default_interface;}
if($interface=="standard"){$interface=$standard_interface;}

// manually set interface
// $interface="standard";

require_once("interfaces/$interface.php");

?>
</div>