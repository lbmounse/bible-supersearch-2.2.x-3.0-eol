<?php

// SECURE
// Bible SuperSearch Version 1.6.00
// The free, open source, PHP and MySQL web-based   
// Bible Reference retrevial and search utility
//
// Copyright (C) 2006 Luke Mounsey
// www.BibleSuperSearch.com
// www.Alive-in-Christ.com
//
// bible_misc.php
// Contains functions needed by both bible_init.php and bible_lookup.php
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License included in the file 
// "license.txt" for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// returns the language (standard 2 character)
// for the given "version"
function getLanguage($version){

$version = mysan($version);
$res=mysql_query("select * from `bible_versions` where `shortname` = '$version' LIMIT 1");
$bib=mysql_fetch_array($res);
return $bib["language_short"];


}// end

// element - element name (can be an array)
// value - new element value (can be an array)
// title - hyperlink title
// class - hyperlink stylesheet class

function makeLink($element, $value, $title="", $class=""){

global $sub, $url;

if (!is_array($element)){$element=array($element);}
if (!is_array($value)){$value=array($value);}

$i=0;

$link=$url."?".$_SERVER["QUERY_STRING"];

while($ele=$element[$i]){

$s=strpos($link,"$ele=");
$start=strpos($link,"=",$s)+1;
$end=strpos($link,"&",$start);

if($end===false){$current=substr($link,$start);}
else{$current=substr($link,$start,$end-$start);}
if($s===false){$current="";}

$val=$value[$i];

//echo("element: $ele| value:$val |current: $current<br>");

if($current==""){$link.="&$ele=$val";}
else{$link=str_replace("$ele=$current","$ele=$val",$link);}

$i+=1;
}// end while

return "<a href='$link' title='$title' class='$class'>";

}// end makeLink

function printout($item){

echo("<pre>");
print_r($item);
echo("</pre>");

}

function bibleversions($select="",$number=""){
global $version,$default_version,$webservice_default_version;
if (($select=="")&&($number=="")){$select=$default_version;}

/*
if($webservice_default_version!==""){

if($number!=1){return "";}

$find=" where `shortname` = '$webservice_default_version' ";

}
else{

$find="";

}

*/

//echo("||$select");

$find = '';

$query="select * from `bible_versions` $find order by `index`";
$res=mysql_query($query);
//echo("|$query|");
$list=array();
while($list[]=mysql_fetch_array($res)){}// end while

//echo(count($list));
// only one Bible
if (count($list)==2){
$v=$list[0];
return $v["fullname"]."<input type=hidden name=version value='".$v["shortname"]."'>";
//return;
}// end if

// NO BIBLES
if (count($list)==1){
return "ERROR: NO BIBLE INSTALLED";

}// end if

$text="<select name=version$number class=bible_version>";

if($number!=""){$text.="<option value=''> Parallel Bible #$number </option>";}

$i=0;
while($ver=$list[$i]){

$text.="<option value='".$ver["shortname"]."'";
if ($ver["shortname"]==$select){$text.=" selected";}

$text.="> ".$ver["fullname"]." (".$ver["language"].") </option>";

$i++;
}// end while

$text.="</select>";
return $text;

}// end function bibleversions


// returns an array of all bible supersearch interfaces avaliable


function getInterfaces(){

$prefix="";//"bible_supersearch_";

// php 4 workaround equivalent for scandir
$dir=array();

$d = dir("interfaces");
while (false !== ($entry = $d->read())) {
   $dir[]= $entry;
   //echo("$entry<br>");
}


// end scandir workaround

$int=array();

//return;

//$action=$_POST["action"];

foreach($dir as $file){

//echo("$file<br>");

//(strpos($file,$prefix)!==false)&&

if ((strpos($file,".php")!==false)&&(strpos($file,"~")===false)){

//$name=substr($file,strlen($prefix),strlen($file)-4-strlen($prefix));
$name=substr($file,0,strlen($file)-4);
$name=str_replace("_"," ",$name);
$name=ucwords($name);

$in=array("file" => $file, "name" => $name);

$int[]=$in;

//echo("$file<br>");
}

}// end foreach

sort($int, SORT_STRING);


return $int;

}// end getInterfaces



function interfaceList($sel=""){

$arr=getInterfaces();

$text="";

set_time_limit(300);
$i=0;
while($int=$arr[$i]){

//for($i=0;$i=count($arr)-1;$i++){

$short=substr($int['file'],0,strlen($int['file'])-4);

$text.="<option value='$short'";

if($short==$sel){$text.=" selected";}


$text.="> ".$int['name']."</option>
";

$i+=1;
}


return $text;

}// end interfaceList

function languageList($sel=""){

$res=mysql_query("select `language`,`language_short` from `bible_versions` GROUP BY `language_short`");
echo(mysql_error());
$arr=array();
while($arr[]=mysql_fetch_array($res)){}

$text="";

foreach($arr as $l){

if ($l==null){break;}

$text.="<option value='".$l['language_short']."'";

if($l['language_short']==$sel){$text.=" selected";}

$text.="> ".$l['language']." (".$l['language_short'].") </option>";
 
}// end foreach


return $text;

}// end languageLIst



function previewInterfaces(){

global $default_interface;
$in=getInterfaces();



echo("


<table border=1 cellpadding=4><tr><td><big><center>Preview Interfaces</big><br><BR>To use Bible SuperSearch,<br> link to \"bible_supersearch.php\"</td></tr>
<tr><td><b>Name</b></td><!--<td><b>File</b></td>--></tr>
<tr><Td><a href='bible_supersearch.php' target='_new'>Default (".ucwords(str_replace("_"," ",$default_interface)).")</a></td><!--<td> bible_supersearch.php </td>--></tr>");

foreach($in as $inter){

$short=substr($inter['file'],0,strlen($inter['file'])-4);

echo("<tr><td><a href='bible_supersearch.php?interface=$short' target='_new'>".$inter["name"]."</a></td></tr>");//<td> ".$inter["file"]."</td></tr>");

}// end foreach

echo"
</table><br><BR>
";

}// end previewInterfaces


function getBrowsingButtons($sel=""){

$dir=array();

$d = dir("bible_images/prevnext");
while (false !== ($entry = $d->read())) {
   
   if(($entry!=".")&($entry!="..")){
   $dir[]= $entry;}
}

$text="";

foreach ($dir as $ent){

$text.="<option value='$ent'";
if($sel==$ent){$text.=" selected";}
$text.=">".ucwords($ent)."</option>
";

}//end foreach

return $text;

}// end getBrowsingButtons

function getFormattingButtons($sel=""){

$dir=array();

$d = dir("bible_images/formatting");
while (false !== ($entry = $d->read())) {
   if(($entry!=".")&($entry!="..")){
   $dir[]= $entry;}
}

$text="";

foreach ($dir as $ent){

$text.="<option value='$ent'";
if($sel==$ent){$text.=" selected";}
$text.=">".ucwords($ent)."</option>
";

}//end foreach

return $text;

}// end getBrowsingButtons

function checkPar($text,$st,$et,$msg=true){

//echo($text."<br>");

$stc=substr_count($text,$st);
$etc=substr_count($text,$et);

if($stc!=$etc){

if($msg){echo("Your \"$st\" and \"$et\" are unbalanced.<br><BR>");}
return false;

}// end if

return true;

}//

// stripExtraSpaces
// $param string
// $return string2, with multiple spaces reduced to 1
function stripExtraSpaces($string){

$string2=str_replace("  "," ",$string);
if ($string==$string2){return $string;}
else{return stripExtraSpaces($string2);}

}// end stripExtraSpaces

// returns the file type of the first detected image in $module_dir
// Ex for image.gif returns gif

function imagetype($module_dir){

$dir=array();
//echo($module_dir);

if($module_dir==""){$d = dir('.');}
else{$d =dir("$module_dir");}

$stop=false;
while ((!$stop)&&(false !== ($entry = $d->read()))) {

$pos=strrpos($entry,".");
$type=substr($entry,$pos+1);

if(($type=="jpg")||($type=="gif")||($type=="png")){$stop=true;}

//echo($entry."<br>");
}


if ($type==""){echo("<b>ERROR: No usable images were found in $module_dir. Usable images include .jpg, .gif, and .png formats.</b><br><BR>");}

//echo("|type$type|$entry");

return $type;

}// imagetype

function sanitize($data, $mysql=false, $html=true)
{
	$data = trim($data);
	if($html){$data = strip_tags($data);}
	$data = stripextraspaces($data);
	
	if($mysql){$data = mysql_real_escape_string($data);}
	
	return $data;
}

function mysan($data)
{
	return sanitize($data,true);
}

?>