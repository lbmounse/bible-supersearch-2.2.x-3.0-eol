<?php 
//session_save_path("modules");
//session_start(); ?>



<?php
// Bible list management

// verify logged in

require_once("bible_inst_auth.php");
if ($auth==false){return;}

// require_once("bible_inst_id.php");
// require_once("bible_mysql.php");
// connect();

require_once("bible_inst_functions.php");

?>
<style>
body{text-align:center;font-size:110%;}
a{text-decoration:none;}
a:hover{text-decoration:underline overline;}
</style>
<div style="text-align:center;color:darkred;font-size:140%; font-weight:bold;">Bible List Manager</div><br><br>

<center>

<?php

menu("bible_inst_list.php");

GlorifytheLORD();

$num1=intval($_GET["num1"]);
$num2=intval($_GET["num2"]);

if((!empty($num1))&&(!empty($num2))){swapBible($num1,$num2);}

$query="select * from `bible_versions` Order by `index`;";

$res=mysql_query($query);
$i=1;

echo("<br><b>This controls the order that installed Bibles are displayed in the Bible version menu</b>.<br><BR>

<table border=1><tr><td>Order</td><td>Short Name</td><td>Full name</td><td>Language</td><td>ISO 639-1<br>Language</td><td>Description</td><td>Position</td></tr>");

while($bible=mysql_fetch_array($res)){

echo("<tr><td>$i</td><td>".$bible["shortname"]."</td><td>".$bible["fullname"]."</td><td>".$bible["language"]."</td><td>".$bible["language_short"]."</td><td>".strip_tags(substr($bible["description"],0,250))."</td><td align=center> ");

updown($bible["index"]);

echo("</td></tr>");

$i++;

}//

echo("</table>");
