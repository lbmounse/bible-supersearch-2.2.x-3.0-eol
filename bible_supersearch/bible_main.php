<?php
// SECURE

// Bible SuperSearch Version 2.0.80
// The free, open source, PHP and MySQL web-based   
// Bible Reference retrevial and search utility
//
// Copyright (C) 2006, 2007, 2008 Luke Mounsey
// www.BibleSuperSearch.com
// www.PreservedWord.com
//
// bible_main.php
// Processes the search data and sends it to the lookup functions
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License included in the file 
// "license.txt" for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// File Version
$bible_main_version=1.6;

// code to extract submitted search and look up requests

require_once("bible_lookup.php");
require_once("bible_display.php");
require_once("bible_config.php");
require_once("bible_system.php");
//require("bible_mysql.php");

$lookup=str_replace(array(";","."),array(",",""),$lookup);
$lookup=syntax($lookup);
//$lookup=random_lookup($lookup);

if(($search!=null)&&(count($version_array)>1)){$parallel_search=true;}

//echo($parallel_search);

//echo($search);

//Proximity aliases



$PROX=array("~P","~","PROXIMITY");
$CHAP=array("~C","CHAPTER","CHAP");
$search=str_replace($CHAP, " PROX(200) ",$search);
$search=str_replace($PROX, " PROX ",$search);

//
if($searchtype=="chapter"){
$searchtype="proximity";
$proxrange=200;
}

$search=trim($search);

if((strpos($search," ")===false)&&($searchtype=="proximity")){
$searchtype="All Words";
}

// connect to mySQL server
connect();

// check for content
// if book and search are empty, the form is blank
if (empty($lookup)&&(empty($search))&&($submit==true)){
error("<center>You didn't enter anything. &nbsp;  Please enter your reference and/or search keywords before submitting your request.</center>");

}// end if
else{// LONG ELSE
//echo($error);
//echo("I'm here");
// check for proximity search with chapter:verse specification
if((strpos($lookup,":")!==false)&&($searchtype=="proximity")&&($search!="")){
echo("<center><b>Note: Entering a proximity search with a limitation specified to the verse (Such as Genesis 1:1-20) may produce unexpected results, results outside of the limitation, or no results.</b></center><br>");
}

if(searchWithin()){$searchtype="Boolean Search";}

$st=array("$searchtype");
if ($wholeword){$st[]="wholeword";}
else{$st[]="partial";}

$lookup=rtrim($lookup);
//$lookup=rtrim($lookup,";");
$lookup=shortcut($lookup);

//$ser_ver=getSearch("Ps 91; Rom;","terror",$version,$st);

$ser_ver=getSearch($lookup,$search,$version, $st);
//echo('lookup: ' . $lookup);
//print_r($ser_ver);
$lookupquery=explode(";",$lookup);
$set_of_verses=$ser_ver;


// Messages when no result is returned
//echo("|$submit|");

//ho($ser_ver[0][0][0]["text"]);


if ((($ser_ver==null)||(!is_array($ser_ver)||(count($ser_ver)==0)))&&($submit!=null)){

// This displays a message when a search with a verse range limitation produces no 
// results.  For some reason, these if statments do not detect a case where an 
// unlimited search produces no results.   A similar if statement is able to 
// detect this case in displayVerses

if ($search!=""){

if($lookup!=""){

if(commonword($search)){
error("<div class=center>You cannot search for the common word \"<span class='bss_errorsearch'>$search</span>\". &nbsp;This will return too many results.</div>");
}

error("<div class=center>Your request for <span class='bss_errorsearch'>$lookup</span> searched for \"<span class='bss_errorsearch'>$search</span>\" returned no results.&nbsp; Please edit your request and try again.</div>");
}//

else{
$ss=str_replace("\\","",$search);
error("<div class=center>Your search request for \"<span class='bss_errorsearch'>$ss</span>\" returned no results.&nbsp; Please edit your request and try again.</div>");
}

}// end if
else{error("<div class=center>Your request for \"<span class='bss_errorsearch'>$lookup</span>\" returned no results. &nbsp; Please edit your request and try again.</div>");}


}// end if

//$test=$set_of_verses[1][0];//[1][1]["text"];
//$test2=$test[0]["text"];

//echo("test $test2");
// BEGIN FUNCTIONIZED CODE

/*


// look up references and reference/search combos

// Convert search reference shortcuts to the acutal reference 
$lookup=rtrim($lookup,";");
$lookup=shortcut($lookup);

$set_of_verses=array();
$ser_ver=array();

// separate the references
$lookupquery=explode(";",$lookup);

// PROCESS LOOKUP/SEARCH COMBINATIONS
$in=0;
while($lookupquery[$in]!=null){

$ref=explodeRef($lookupquery[$in]);

// if request for whole book and whole book = false and no search
// display only first chapter.
if (($wholebook!="true")&&($ref["chapter"]=="")&&($ref["verse"]=="")&&($search=="")){
$ref["chapter"]=1;
}// end if

$verses=getVerses($ref["book"], $ref["chapter"], $ref["verse"], $search, $st, $version);
$set_of_verses[]=$verses;

if($search!=null){$ser_ver=array_merge($ser_ver,$verses);}

// display error message if no verses are found

if (($verses[0]==null) || ($verses==null)){

// This displays a message when a search with a verse range limitation produces no 
// results.  For some reason, these if statments do not detect a case where an 
// unlimited search produces no results.   A similar if statement is able to 
// detect this case in displayVerses

if ($search!=null){error("Your request for <span class='bss_errorsearch'>$lookupquery[$in]</span> searched for \"<span class='bss_errorsearch'>$search</span>\" returned no results.");}
else{error("Your request for \"<span class='bss_errorsearch'>$lookupquery[$in]</span>\" returned no results. &nbsp; Please edit your request and try again.");}
}// end if

$in+=1;	
}// end while
// END LOOKUP/SEARCH COMBINATIONS

// PROCESS SEARCHES

if (($search!=null)&&(empty($lookup))){


// searches only, with no passage reference

$set_of_verses[]=getVerses("","","",$search, $st, $version);

// if the search returns no results, display a message
// (this doesn't actually detect search errors, simular to as mentioned above)
if (empty($set_of_verses[0])){error("Your search for \"<span class='bss_errorsearch'>$search</span>\" returned no results. &nbsp; Please edit your search and try again.");}

else{$ser_ver=array_merge($ser_ver,$set_of_verses[0]);}// end else

}// end if (search only)

*/
// END FUNCTIONIZED

//echo("search|$search<br>");

if(($search!=null)){

//foreach ($set_of_verses as $v){
//$ser_ver=array_merge($ser_ver,$v);
//}// end foreach"

//echo("<BR>>page}$page");
// determine number of result verses
// lmiit to specified range
if(strpos($version,",")===false){

$num=max(count($ser_ver[0]),count($ser_ver[1]),count($ser_ver[2]),count($ser_ver[3]));
//$num=count($ser_ver);
$t1="";// text relating to parallel searches only
$t2="";
$range=pageRange($num,$page);

if(($page!=="all")&&($ser_ver!=null)){
$i=0;
//echo("I am here");

while($ser_ver[$i]!=null){
$ser_ver[$i]=array_slice($ser_ver[$i],$range["min"],$verses_per_page);
$i++;
}// end while
}


}// end if
else{
$num=max(count($ser_ver[0]),count($ser_ver[1]),count($ser_ver[2]),count($ser_ver[3]));
if($parallel_search){$num=count($ser_ver);}
$range=pageRange($num,$page);
$t1="a maximum of ";
$t2=" per Bible";

if(($page!=="all")&&($ser_ver!=null)){

//echo("|".$range['min']."|");

if($parallel_search){

//$i=0;
//while($ser_ver[$i]!=null){
$ser_ver=array_slice($ser_ver,$range["min"],$verses_per_page);
//$i++;
//}// end while

}
else{
$i=0;
while($ser_ver[$i]!=null){
$ser_ver[$i]=array_slice($ser_ver[$i],$range["min"],$verses_per_page);
$i++;
}// end while
}//end else


}// end if

}// end else


if($num!=1){$pl="s";}
else{$pl="";}

if ($num>0){

echo("<center><div class='center' style='margin-left:auto;margin-right:auto'>");

if($num>=$max_verses){echo("Your search was limited to $num results. &nbsp; Please refine your search if nessessary. &nbsp; ");}// end if

else{echo("<nobr>Your search produced $t1$num result$pl$t2.</NOBR> &nbsp; <nobr>");}// end else

if($page==="all"){echo(pageLink("one")."Paginate results</a>.<br><br></center>");}
else{
echo("Showing results ".($range["min"]+1)." to ".($range["max"]+1));

if ($num>$verses_per_page){echo(". &nbsp; ".pageLink("all")."Show all</a>.");}
echo("</nobr></div></center><br><BR>");

}// end if

}// end "results"

$index=paginationIndex($num,$page);

if(($page!=="all")&&($num>0)){echo("$index<br><BR>");}

//echo("I am here");

//printout($ser_ver);

if($parallel_search){

foreach($ser_ver as $ver){

$dis=displayVerses("",$ver,true);//, $range["min"], $range["max"]);

}//end foreach

}// end if
else{

$dis=displayVerses("",$ser_ver,true);//, $range["min"], $range["max"]);

}// end else

if(($page!=="all")&&($num>0)){echo("<br>$index<br><BR>");}

}// end if

else{

// lookup only

// check for "ranges of books"
$in=0;

while($lookupquery[$in]!=null){
$ref=explodeRef($lookupquery[$in]);
if(strpos($ref["book"],"-")>1){

error("<center>Please note that while you can search ranges of books (ex. Matt-John) you cannot look up ranges of books at a time. &nbsp; This helps prevent overloading my server. &nbsp;  If you need to look up multiple books, enter them into the look up box individually (ex. Matt; Mark; Luke; John).</center>");
break;
}// end if
$in++;
}// end while

// check for no results

$in=0;

while($lookupquery[$in]!=null){
$ref=$lookupquery[$in];

if(count($set_of_verses[$in][0])==0){
//if(strpos($ref["book"],"-")===false){


echo("<div class=center>Your request for \"<span class='bss_errorsearch'>".trim($lookupquery[$in])."</span>\" returned no results. &nbsp; Please edit your request and try again.</div>");//}
}

$in++;
}

// if there are verses, display them
$in=0;

while($lookupquery[$in]!=null){
//while($set_of_verses[$in]!=null){

//echo("in $in<br>");

$ref=explodeRef($lookupquery[$in]);
//echo($ref["book"]);

$verses=$set_of_verses[$in];
//echo(count($verses));

if (!is_array($verses[0])){

//if(strpos($ref["book"],"-")===false){echo("Your request for <span class='bss_errorsearch'>".$lookupquery[$in]."</span> was not found.<br><BR>");}

}
else{
$dis=displayVerses($ref, $verses, !empty($search),0,-1,$version);
}

$in++;
}// end while
}// end else

}// end long else

// display any error messages
//dispError();
// end main code

// syntax
// makes request reference syntax more flexible
// corrects incorrect reference request syntax
// @param - ref - entered reference
// @returns - ref - corrected reference

function syntax($ref){

//echo("old ref:$ref<br>");

$ref=stripExtraSpaces($ref);

// insert space between book name and chapter
$i=1;
while($c2=$ref[$i]){
$c1=$ref[$i-1];

if((ctype_alpha($c1))&&(ctype_digit($c2))){

$ref=str_replace("$c1$c2","$c1 $c2",$ref);
}

$i++;
}//end 




// OLD CODE BELOW
// DELETE ME??


// set all "distinguished commas" (commas between references that should be ";")to "~"
// so the reference can be easily splitten into separate parts

$com1=strposneg($ref,',');

// search for "distinguished commas"/
while($com1!=-1){

$char1=substr($ref,$com1+1,1);
$char2=substr($ref,$com1+2,1);
$char3=substr($ref,$com1+3,1);
$char4=substr($ref,$com1+4,1);

// replace comma
//echo("char1:$char1|char2:$char2|<br>");

if((ctype_alpha($char1))||(ctype_alpha($char2))||(ctype_digit($char2)&&(ctype_alpha($char4))&&(($char3==" ")||(ctype_alpha($char3))))){

//echo("dist at $coml<br>");//diagnostic
//echo("com1|$com1| col|$col| com2|$com2|<br>");// diagnostic

// this allows for functionality in php3, which does not have substr_replace
$ref=substr($ref, 0,$com1).";".substr($ref,$com1+1);
}// end if

//echo("<br>reff modified:|$ref|<br>");// diagnostic
//echo("I am looking for dist commas!<br>");

$com1=strposneg($ref,",",$com1+1);
//if(($com2>$max)||($col==-1)||($com2==-1)){$col = $max+5;}

}// end while

// reference by reference
$re=explode(";",$ref);
$stop=count($re);
$i=0;
$book="";
while($i<$stop){

$lastbook=$book;


$i+=1;
}// end while

$ref=implode(";", $re);

//echo("new ref:$ref<br>");

return $ref;
}// end syntax

function searchWithin(){

global $search,$old_search,$searchtype,$old_searchtype,$within,$lookup,$old_lookup;

if($within!="true"){return false;}

$search=booleanize($search,$searchtype);
$old_search=booleanize($old_search,$old_searchtype);



if($searchtype=="proximity"){
echo("<div class=center>Sorry.  You cannot search within a proximity search.</div><BR>");
return false;
}// end

$lookup=$old_lookup;
$search="($search) & ($old_search)";
return true;

}// end search within

// converts any search query into a boolean search

function booleanize($search,$searchtype){

if($searchtype=="All Words"){$search=str_replace(" "," & ",$search);}
if($searchtype=="Any Word"){$search=str_replace(" "," | ",$search);}
if($searchtype=="Exact Phrase"){$search="\"$search\"";}
if($searchtype=="proximity"){return $search;}// can't be booleanized

return $search;

}//

?>
<br><BR>