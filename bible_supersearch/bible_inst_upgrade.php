<?php 
//session_save_path("modules");
//session_start(); 
require_once("bible_inst_functions.php");


//if(!check_login_file()){return;}

require_once("bible_inst_auth.php");
if ($auth==false){return;}
?>

<div style="text-align:center;color:darkred;font-size:140%; font-weight:bold;">Bible Database Upgrade</div><br><br>

<center>

<?php

require_once("bible_inst_functions.php");
menu("bible_inst_upgrade.php");

GlorifytheLORD("YES");



require_once("bible_mysql.php");
require_once("bible_config.php");
require_once("bible_system.php");

connect();

// php 4 workaround equivalent for scandir
$dir=array();

$d = dir(".");
while (false !== ($entry = $d->read())) {
   $dir[]= $entry;
}
// end scandir workaround

$upgrades=array();

$action=$_POST["action"];

foreach($dir as $file){

if ((strpos($file,"bible_upgrade")!==false)&&(strpos($file,".sql")!==false)){
$upgrades[]=$file;
$upgrade=$file;
//echo("$file<br>");
}

}// end foreach

sort($upgrades);
//foreach($upgrades as $file){echo($file.'<br>');}

$upgrade=$upgrades[count($upgrades)-1];
$upversion=substr($upgrade,13,(strlen($upgrade)-17));

$comp=phpcompare($upversion,$installed_version);


if (($action!="upgrade")&&($comp==1)){
echo("<br>

Most recent upgrade file '$upgrade' found. &nbsp; <br>
<br>
<br>
Warning! &nbsp; This will updates your Bible SuperSearch tables corresponding to your latest upgrade of Bible SuperSearch itself. &nbsp; This does NOT upgrade Bible SuperSearch. &nbsp; This update is permenent; if you desire to reverse this operation, you will have to uninstall and reinstall Bible SuperSearch.<br><BR>

NOTE: In some instances, upgrading may require you to reinstall the Bible versions on your system.
<BR><br>

<table><tr><td>
<form action='' method=post><input type=submit value='Upgrade'><input type=hidden value=upgrade name=action></form></td><td>

<form action='bible_inst_manager.php'><input type=submit value='Cancel'><input type=hidden value=upgrade></form></td></tr></table>");
return;
}// end if

if($comp!=1){

echo("<br> 
<b>Your Bible SuperSearch SQL tables are up-to-date.</b>.<br><BR>

(No unapplied upgrade file was found.)<br><br>

If you are looking for upgrades for Bible SuperSearch, <br>see our website at <a href='http://www.biblesupersearch.com' target='_new'>www.BibleSuperSearch.com</a>, or see our <a href='http://sourceforge.net/project/showfiles.php?group_id=177945' target='_new'>SourceForge site</a> for more downloads.
<BR><br>


<form action='bible_inst_manager.php'><input type=submit value='Cancel'><input type=hidden value=upgrade></form>");

return;

}

runmysql($upgrade);

$file=implode('',file("bible_config.php"));
$file=str_replace("\$installed_version=\"$installed_version\";","\$installed_version=\"$upversion\";",$file);
write_file("bible_config.php",$file);

echo("<br><BR>Upgrade complete. 

<br><BR>
<form action='bible_inst_manager.php'><input type=submit value='Continue'><input type=hidden value=upgrade></form></td></tr></table>");

?>
