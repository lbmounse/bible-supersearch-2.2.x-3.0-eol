<?php

// SECURE
// Bible SuperSearch Version 1.0.00
// The free, open source, PHP and MySQL web-based   
// Bible Reference retrevial and search utility
//
// Copyright (C) 2006 Luke Mounsey
// www.BibleSuperSearch.com
// www.Alive-in-Christ.com
//
// bible_mysql.php
// Contains functions to connect to the MySQL database
// Links to bible_lookup.php, which contains the user's login data
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License included in the file 
// "license.txt" for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// file version
$bible_mysql_version=0.4;

if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
    preg_match('/\d*\.\d*\.\d*/', PHP_VERSION, $matches);
    $php_version_simple = $matches[0];

    ?>
        <div style='width: 800px; margin: 200px auto 0; font-family:sans-serif; font-size: 1.3em'>
            <b>Error</b>:  Bible SuperSearch Legacy is <u>not supported</u> on PHP 7.0 or higher. <br /><br />
            You are currently running PHP version <?php echo $php_version_simple ?>.<br /><br />
            Please visit <b><a href='https://www.biblesupersearch.com/downloads'>BibleSuperSearch.com Downloads</a></b> and download the latest version. <br /><br />
            Please see our <b><a href='https://www.biblesupersearch.com/migration-legacy/'>Legacy to Modern Migration Guide</a></b>.
        </div>
    <?php

    die();
}


// connect
// connects to MySQL database
// @ name connect
// @ param none
// @ return none
// logs into MySQL and 
// selects main database for use
require("bible_login.php");

function connect(){
require("bible_login.php");
// Connecting, selecting database

// ADVANCED USERS ONLY
// ALL OTHERS DO NOT CHANGE THIS SETTING!
// set this to false ONLY if you are using 
// an offline local server to develop/ debug
// other than the one you use to host your website.
// otherwise, 
$online=false;
error_reporting(0);
if ($online) {
	// mySQL database logon
	// set these values to your hostname, username and password
	
	$link = mysql_connect('localhost','user',"pass")
   	or die('Could not connect: Please check your hostname, username and password in the file mysql.php' . mysql_error());
	// set database name to the name of the database assigned.
	mysql_select_db('db') 
	or die('Database not found.   Please check your hostname, username, password, and database name in the file mysql.php');
}
else {
	// offline mysql login
	// see above
	// advanced users ONLY
	// set these values to your hostname, username and password
	//echo("mysql $host, $user, $password, $db");
		if(mysql_connect($host,$user,$password)==false){
	echo(mysql_error()."<br>");
	return false;

	}
	  // 	or die('Could not connect: Please check your hostname, username and password in the file mysql.php' . mysql_error());
		// set database name to the name of the database assigned.
		if (mysql_select_db($db)==false){
	//	$error=mysql_error();
	//	echo("$error<br>This means that you either do not have permission to access the 	database requested, you do not have permission to create databases if you tried to 		create a database, or your web hosting company has set a specific format for naming 		databases.   Please try a different database name, or contact your web hosting 		company for details.");
		return -1;
		}
	//	or die('Database not found.   Please check your hostname, username, password, and database name in the file mysql.php');
		return true;
}
error_reporting(E_ALL ^ E_NOTICE);
return true;
}// end connect
?> 
